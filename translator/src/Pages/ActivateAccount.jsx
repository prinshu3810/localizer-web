import React, { useState } from 'react';
import Notifications, { notify } from 'react-notify-toast';

import Axios from '../util/axiosconfig';
import Strings from '../constants/String';
import Header from '../components/Header';
import { PageRoutes } from '../constants/Constants';
import { Form, Input, Button } from 'antd';
import { UpdatePasswordStatus } from '../constants/Constants';
import './css/ActivateAccount.css';

function ActivateAccount(props) {

    const [isDirty, setIsDirty] = useState(false);
    const [loading, setLoading] = useState(false);
    const { getFieldDecorator } = props.form;

    const { match } = props;

    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
        },
    };

    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0,
            },
            sm: {
                span: 16,
                offset: 8,
            },
        },
    };

    // Mmask/Unmask password field.
    const handleConfirmBlur = e => {
        const { value } = e.target;
        setIsDirty(isDirty || !!value);
    };

    // Checks new password is same as confirm new password field.
    const compareToFirstPassword = (rule, value, callback) => {
        const { form } = props;
        if (value && value !== form.getFieldValue('newpassword')) {
            callback(Strings.AppMessage.ErrorMessage.InconsistentPassword);
        } else {
            callback();
        }
    };

    // Validate the confirm password field.
    const validateToNextPassword = (rule, value, callback) => {
        const { form } = props;
        if (value && isDirty) {
            form.validateFields(['confirm'], { force: true });
        }
        callback();
    };

    // Api call for account activation of user.
    const ActivateAccount = (data) => {
        Axios.put('User/Activate/' + match.params.invitationCode, data)
            .then(res => {
                if (res.data.statusCode === UpdatePasswordStatus.Success) {
                    props.form.resetFields();
                    setLoading(false);
                    notify.show(Strings.AppMessage.SuccessMessage.AccountActivated, "custom", 3000, { background: '#52c41a', text: "#FFFFFF" });
                    setTimeout(() => {
                        props.history.push('/');
                    }, 3000)
                } else if (res.data.statusCode === UpdatePasswordStatus.InvalidParameters) {
                    notify.show(Strings.AppMessage.ErrorMessage.InvalidAccount, "custom", 3000, { background: '#ee3647', text: "#FFFFFF" });
                    setLoading(false);
                }
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 3000, { background: '#ee3647', text: "#FFFFFF" });
                setLoading(false);
            });
    }

    // Validates the Activate account form.
    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                const data = {
                    NewPassword: values.newpassword
                }
                ActivateAccount(data);
            } else {
                setLoading(false);
            }
        });
    }

    return (
        <div className="a-account">
            <Header history={props.history} pageRoute={PageRoutes.Admin} />
            <h1>Activate Account</h1>
            <Form {...formItemLayout} onSubmit={handleSubmit} className="a-account__form" >
                <Form.Item label="New Password" hasFeedback labelAlign='left'>
                    {getFieldDecorator('newpassword', {
                        rules: [
                            {
                                required: true,
                                message: Strings.AppMessage.ErrorMessage.Required,
                            },
                            {
                                min: 8,
                                message: Strings.AppMessage.ErrorMessage.WeakPassword,
                            },
                            {
                                validator: validateToNextPassword,
                            },
                        ],
                    })(<Input.Password onBlur={handleConfirmBlur} />)}
                </Form.Item>
                <Form.Item label="Confirm New Password" hasFeedback labelAlign='left'>
                    {getFieldDecorator('confirm', {
                        rules: [
                            {
                                required: true,
                                message: Strings.AppMessage.ErrorMessage.ConfirmPassword,
                            },
                            {
                                validator: compareToFirstPassword,
                            },
                        ],
                    })(<Input.Password onBlur={handleConfirmBlur} />)}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit" className="login-form-button" disabled={loading}>
                        Activate
                    </Button>
                </Form.Item>
            </Form>
            <Notifications />
        </div>
    )
}

export default Form.create()(ActivateAccount);