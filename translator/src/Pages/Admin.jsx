import React, { useState, useEffect, useRef } from 'react';
import { Table, Modal, Spin, Checkbox, Button } from 'antd';
import Notifications, { notify } from 'react-notify-toast';
import _ from 'lodash';
import Axios from '../util/axiosconfig';
import ManageUserModal from '../components/Modals/MangeUserModal';
import Strings from '../constants/String';
import Header from '../components/Header';
import { PageRoutes, ListStates } from '../constants/Constants';
import InviteUserModal from '../components/Modals/InviteUserModal';
import UserStateList from '../components/UserStateList';
import './css/Admin.css';

const { confirm } = Modal;

function Admin(props) {

    const [users, setUsers] = useState([]);
    const [deletedUsers, setDeletedUsers] = useState([]);
    const [languages, setLanguages] = useState([]);
    const [roles, setRoles] = useState([]);
    const [userId, setUserId] = useState(0);
    const [manageModalLoading, setManageModalLoading] = useState(false);
    const [manageModalVisible, setManageModalVisible] = useState(false);
    const [inviteModalLoading, setInviteModalLoading] = useState(false);
    const [inviteModalVisible, setInviteModalVisible] = useState(false);
    const [showSpinner, setShowSpinner] = useState(true);
    const [saveButtonDisabled, setSaveButtonDisabled] = useState(true);
    const [inviteButtonDisabled, setInviteButtonDisabled] = useState(true);
    const [stateListValue, setStateListValue] = useState(ListStates.Active);
    const [tableDataSource, setTableDataSource] = useState([]);

    const AdminUsersTableColumns = [
        {
            title: "Name",
            dataIndex: "name",
            key: "name",
            width: "20%",
            render: (value, row, index) => {
                return <a href="#" onClick={() => showManageModal(row.id)} style={{ cursor: 'pointer' }}>{value}</a>;
            },
        },
        {
            title: "Email",
            dataIndex: "email",
            key: "email",
            width: "20%",
            render: (value, row, index) => {
                return <span>{value}</span>;
            },
        },
        {
            title: "Roles",
            dataIndex: "roles",
            key: "roles",
            width: "20%",
            render: (value, row, index) => {
                return <span>{value}</span>;
            },
        },
        {
            title: "Languages",
            dataIndex: "languages",
            key: "languages",
            width: "40%",
            render: (value, row, index) => {
                return <span>{value}</span>;
            },
        },
    ];

    const usersLoading = useRef(false);

    // Api calls for fetching roles, languages.  
    useEffect(() => {
        getRoles();
        getLanuages();
    }, []);

    // Api calls for fetching users
    useEffect(() => {
        if (usersLoading.current || roles.length === 0 || languages.length === 0) {
            return;
        }
        getUsers();
        setShowSpinner(false);
    }, [languages, roles]);

    useEffect(() => {
        let dataSource;
        if (stateListValue === ListStates.All) {
            dataSource = [...users, ...deletedUsers];
        } else if (stateListValue === ListStates.Active) {
            dataSource = [...users];
        } else {
            dataSource = [...deletedUsers]
        }
        setTableDataSource(dataSource);
    }, [stateListValue, users, deletedUsers]);

    // Api call to fetch users data and set users and deltedUsers state variables. 
    const getUsers = () => {
        usersLoading.current = true;
        return Axios.get('Admin/Users')
            .then(res => {
                if (res.data) {
                    let userData = res.data;
                    userData.forEach(user => {
                        user.languages = getNames(user.languageIds, languages);
                        user.roles = getNames(user.roleIds, roles);
                        user.name = user.name.includes('@') ? user.name.split('@')[0].split('.').join(' ') : user.name
                    })
                    setUsers(userData.filter(user => !user.disabled));
                    setDeletedUsers(userData.filter(user => user.disabled));
                    usersLoading.current = false;
                }
            }).catch(error => {
                usersLoading.current = false;
                notify.show(Strings.AppMessage.ErrorMessage.FetchDataError, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            });
    }

    // Api call to fetch languages data and set languages state variable. 
    const getLanuages = () => {
        Axios.get('Admin/Languages')
            .then(res => {
                if (res.data) {
                    setLanguages(res.data);
                }
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.FetchDataError, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            });
    }

    // Api call to fetch roles data and set roles state variable. 
    const getRoles = () => {
        Axios.get('Admin/Roles')
            .then(res => {
                if (res.data) {
                    setRoles(res.data);
                }
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.FetchDataError, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            });
    }

    const showManageModal = (id) => {
        setUserId(id);
        setManageModalVisible(true);
    };

    const handleManageModalSave = (formValues) => {
        setManageModalLoading(true);
        setShowSpinner(true);
        const data = { languageIds: formValues.languages, roleIds: formValues.roles };

        Axios.put('Admin/save/' + userId, data)
            .then(res => {
                let promise = new Promise(resolve => {
                    resolve();
                });

                if (res.data) {
                    notify.show(Strings.AppMessage.SuccessMessage.DetailsUpdated, "custom", 2500, { background: '#52c41a', text: "#FFFFFF" });
                    promise = getUsers();
                } else if (!res.data) {
                    notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                }
                setManageModalVisible(false);
                setManageModalLoading(false);
                setSaveButtonDisabled(true);
                promise.then(() => {
                    setShowSpinner(false);
                });
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                setManageModalVisible(false);
                setManageModalLoading(false);
                setSaveButtonDisabled(true);
                setShowSpinner(false);
            });
    }

    // To validate the invite user form and send an invite email to the user through an api call.
    const handleInvite = (formValues) => {
        setInviteModalLoading(true);

        const inviteEmail = formValues.email;
        const data = { name: formValues.name, languageIds: formValues.languages, roleIds: formValues.roles };
        Axios.post('Admin/Invite/' + inviteEmail, data)
            .then(res => {
                if (res.data) {
                    notify.show(Strings.AppMessage.SuccessMessage.InviteLinkSent, "custom", 2500, { background: '#52c41a', text: "#FFFFFF" });
                } else if (!res.data) {
                    notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                }
                setInviteModalVisible(false);
                setInviteModalLoading(false);
                setInviteButtonDisabled(true);
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.FetchDataError, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                setInviteModalVisible(false);
                setInviteModalLoading(false);
                setInviteButtonDisabled(true);
            });
    }

    // Close the modal
    const handleCancel = () => {
        setInviteModalLoading(false);
        setInviteModalVisible(false);
        setManageModalVisible(false);
        setManageModalLoading(false);
        setSaveButtonDisabled(true);
        setInviteButtonDisabled(true);
    };

    const isUserDisabledMessage = () => {
        let user = Array.isArray(users) && users.length && users.find(user => user.id === userId);
        return user ? Strings.AppMessage.SuccessMessage.UserDisabled : Strings.AppMessage.SuccessMessage.UserEnabled;
    }

    // Api call to enable or disable(toggle Disabled property of) a user.
    const toggleDisabled = (disabled) => {
        setShowSpinner(true);
        Axios.put('Admin/ToggleUserDisabled/' + userId, disabled)
            .then(res => {
                let promise = new Promise(resolve => {
                    resolve();
                });
                if (res.data) {
                    promise = getUsers();
                    notify.show(isUserDisabledMessage(), "custom", 2500, { background: '#52c41a', text: "#FFFFFF" });
                } else {
                    notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                }
                setManageModalLoading(false);
                setManageModalVisible(false);
                promise.then(() => {
                    setShowSpinner(false);
                });
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                setManageModalLoading(false);
                setManageModalVisible(false);
                setShowSpinner(false);
            });
    }

    // Asks confirmation for disabling a user. 
    const showConfirmDisable = () => {
        confirm({
            title: Strings.Texts.ConfirmDisable,
            okText: Strings.Texts.Yes,
            okType: Strings.Texts.Danger,
            cancelText: Strings.Texts.No,
            onOk() {
                toggleDisabled(true);
            },
            onCancel() {

            },
        });
    }

    const handleDisable = () => {
        setManageModalVisible(false);
        showConfirmDisable();
    }

    // Asks confirmation for enable a user.
    const showConfirmEnable = () => {
        confirm({
            title: Strings.Texts.ConfirmEnable,
            okText: Strings.Texts.Yes,
            okType: Strings.Texts.Danger,
            cancelText: Strings.Texts.No,
            onOk() {
                toggleDisabled(false);
            },
            onCancel() {
            },
        });
    }

    const handleEnable = (e) => {
        setManageModalVisible(false);
        showConfirmEnable();
    }

    // To return the name corresponding to the ids of roles and languages.
    const getNames = (ids, array) => {
        let names = [];
        if (Array.isArray(ids) && Array.isArray(array) && ids.length && array.length) {
            names = Array.isArray(ids) && ids.length && ids.reduce((acc, id) => {
                const match = array.find(element => element.id === id);

                if (match) {
                    acc.push(match.name);
                }

                return acc;
            }, []);
        }
        return names.length ? names.join(', ') : '';
    }

    // To return a single user data either from users or disabled users.
    const userData = () => {
        const user = users.filter(user => user.id === userId);
        return user.length ? user : deletedUsers.filter(deletedUser => deletedUser.id === userId);
    }

    // To return email ids of all the users.
    const emailIdsData = () => {
        return users.length || deletedUsers.length ? [...users.map(u => u.email), ...deletedUsers.map(du => du.email)] : [];
    }

    const handleStateListChange = (e) => {
        setStateListValue(e.target.value);
    }

    const allUsersCount = Array.isArray(users) && Array.isArray(deletedUsers) ? users.length + deletedUsers.length : 0;
    const activeUsersCount = Array.isArray(users) && users.length ? users.filter(user => !user.disabled).length : 0;

    return (
        <div className="ad-container">
            <Header history={props.history} pageRoute={PageRoutes.Admin} />
            <div className="ad-container__grid">
                <div className="ad-container__row">
                    <UserStateList allUsersCount={allUsersCount} activeUsersCount={activeUsersCount} value={stateListValue} handleStateListChange={handleStateListChange} />
                    <Button type="primary" ghost icon="plus" onClick={() => setInviteModalVisible(true)} className="ad-container__invite">Invite</Button>
                </div>
                {
                    !showSpinner && users.length && languages.length && roles.length && tableDataSource.length ?
                        <Table
                            dataSource={tableDataSource}
                            columns={AdminUsersTableColumns}
                            className="ad-container__table"
                            pagination={false}
                            rowClassName={(record, index) => (record.disabled && "ad-container__row-disabled")}
                        />
                        :
                        <div className="pdf-page-loader"><Spin size="large"></Spin></div>
                }
            </div>
            <ManageUserModal
                visible={manageModalVisible}
                loading={manageModalLoading}
                handleCancel={handleCancel}
                handleDisable={handleDisable}
                user={userData()}
                roles={roles}
                languages={languages}
                handleEnable={handleEnable}
                handleManageModalSave={handleManageModalSave}
                saveButtonDisabled={saveButtonDisabled}
                setSaveButtonDisabled={setSaveButtonDisabled}
            />
            <InviteUserModal
                visible={inviteModalVisible}
                loading={inviteModalLoading}
                handleCancel={handleCancel}
                roles={roles}
                languages={languages}
                emailIds={emailIdsData()}
                handleInvite={handleInvite}
                inviteButtonDisabled={inviteButtonDisabled}
                setInviteButtonDisabled={setInviteButtonDisabled}
            />
            <Notifications />
        </div>
    )
}

export default Admin;