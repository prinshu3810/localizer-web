import React, { useState, useEffect } from 'react';
import { Row, Col, Spin, Radio, Badge, Button } from 'antd';
import _ from 'lodash';
import Notifications, { notify } from 'react-notify-toast';
import Axios from '../util/axiosconfig';
import Header from '../components/Header';
import { PageRoutes } from '../constants/Constants';
import ManageGroups from '../components/ManageGroups';
import ArrangeGroups from '../components/ArrangeGroups';
import Strings from '../constants/String';
import { TranslationType } from '../constants/Constants';
import './css/Groups.css';

export default function Groups(props) {

    const [mode, setMode] = useState("groups");
    const [orphanTextsCount, setOrphanTextsCount] = useState(0);
    const [orphanPdfsCount, setOrphanPdfsCount] = useState(0);
    const [application, setApplication] = useState({});
    const [versions, setVersion] = useState([]);

    useEffect(() => {
        getApplicationDetails();
        getOrphanTextsCount();
        getOrphanPdfsCount();
        getVersionDetails();
    }, []);

    const getApplicationDetails = () => {
        const location = props.location.pathname;
        const applicationId = location.slice(location.lastIndexOf('/') + 1, location.length);

        Axios.get(`Localization/${applicationId}`)
            .then(res => {
                if (res.data) {
                    setApplication(res.data);
                }
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.FetchDataError, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            });
    }

    const getOrphanTextsCount = () => {
        const location = props.location.pathname;
        const applicationId = location.slice(location.lastIndexOf('/') + 1, location.length);

        Axios.get(`Group/OrphanTextsCount/${applicationId}`)
            .then(res => {
                if (res.data) {
                    setOrphanTextsCount(res.data);
                }
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.FetchDataError, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            });
    }

    const getOrphanPdfsCount = () => {
        const location = props.location.pathname;
        const applicationId = location.slice(location.lastIndexOf('/') + 1, location.length);

        Axios.get(`Group/OrphanPdfsCount/${applicationId}`)
            .then(res => {
                if (res.data) {
                    setOrphanPdfsCount(res.data);
                }
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.FetchDataError, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            });
    }

    const getVersionDetails = () => {    
        Axios.get(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/GetVersionDetails')
            .then(res => {
                setVersion(res.data);
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.FetchDataError, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            });
    }

    const handleModeChange = (e) => {
        setMode(e.target.value);
    }

    return (
        <div className="mg-container">
            <Header history={props.history} pageRoute={PageRoutes.ManageGroup} />

            <div className="mg-container__row">
                <div className="mg-container__title">{ mode && mode === "groups" ? <span>Manage Group</span> : <span>Arrange Group</span>} - {application && application.name}</div>
                <Radio.Group className="mg-tab-list" onChange={handleModeChange} defaultValue="groups">
                    <Radio.Button value="groups">Groups</Radio.Button>
                    <Radio.Button value="arrange">Arrange <Badge count={application && application.translationTypeId === TranslationType.Text ? orphanTextsCount : orphanPdfsCount} /></Radio.Button>
                </Radio.Group>
            </div>

            {mode && mode === "groups" ? <ManageGroups applicationId={application && application.id} /> : <ArrangeGroups applicationId={application && application.id} translationTypeId={application && application.translationTypeId} setOrphanTextsCount={setOrphanTextsCount} setOrphanPdfsCount={setOrphanPdfsCount} versions={versions}/>}
            <Notifications />
        </div>
    )
}
