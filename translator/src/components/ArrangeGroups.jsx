import React, { useState, useEffect, useRef } from 'react';
import { Spin, Row, Col, Select, Badge, Icon, Tooltip, Modal, Input, Button } from 'antd';
import Notifications, { notify } from 'react-notify-toast';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import _, { cloneDeep } from 'lodash';
import { ContextMenuTrigger, ContextMenu, MenuItem, SubMenu } from 'react-contextmenu';
import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';
import Axios from '../util/axiosconfig';
import Strings from '../constants/String';
import { TranslationType, Languages } from '../constants/Constants';

const { Option } = Select;
const MENU_TYPE = 'LTR';
let isDisabled = true;
let className = "";

export default function ArrangeGroups(props) {

    const [sections, setSections] = useState([]);
    const [groups, setGroups] = useState([]);
    const [showSpinner, setShowSpinner] = useState(true);
    const [selectedSection, setSelectedSection] = useState({});
    const [selectedGroup, setSelectedGroup] = useState({});
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [textValue, setTextValue] = useState("");
    const [textId, setTextId] = useState(-1);
    const [translation, setTranslation] = useState([]);
    const [editedReleaseId, setEditedReleaseId] = useState(-1);
    const [shouldRenderSave, setRenderSave] = useState(false);
    const { applicationId, translationTypeId, setOrphanTextsCount, setOrphanPdfsCount,versions } = props;

    useEffect(() => {
        setShowSpinner(true);
        getSectionsAndTranslations();
        getGroupsAndTranslations();
    }, []);

    // checks if the selected group is not empty then updates selected group state from groups state else set selected group as first group from groups.
    // This is done so that when component rerenders the selected group remains the same as before.
    useEffect(() => {
        if (!_.isEmpty(selectedGroup) && groups.length) {
            const selectedGroupIndex = groups.findIndex(group => group.id === selectedGroup.id)
            setSelectedGroup(groups[selectedGroupIndex]);
        }
        else if (_.isEmpty(selectedGroup) && groups.length) {
            setSelectedGroup(groups[0]);
        }
    }, [groups]);

    // checks if the selected section is not empty then updates selected section state from sections state else set selected section as first section from sections.
    // This is done so that when component rerenders the selected section remains the same as before.
    useEffect(() => {
        if (!_.isEmpty(selectedSection) && sections.length) {
            const selectedSectionIndex = sections.findIndex(section => section.id === selectedSection.id)
            setSelectedSection(sections[selectedSectionIndex]);
        }
        else if (_.isEmpty(selectedSection) && sections.length) {
            setSelectedSection(sections[0]);
        }
        if (translationTypeId === TranslationType.Text) {
            setOrphanTextsCount(getAllOrphanTextCount());
        } else {
            setOrphanPdfsCount(getAllOrphanPdfCount());
        }
    }, [sections]);

    const getAllOrphanTextCount = () => {

        const totalCount = sections.reduce((count, section) => {
            count += section.texts.filter(text => text.groupId === null).length;
            return count;
        }, 0);

        return totalCount;
    }

    const getAllOrphanPdfCount = () => {

        const totalCount = sections.reduce((count, section) => {
            count += section.pdfs.filter(pdf => pdf.groupId === null).length;
            return count;
        }, 0);

        return totalCount;
    }

    const getGroupsAndTranslations = () => {
        setShowSpinner(true);
        const isText = translationTypeId === TranslationType.Text ? true : false;
        Axios.get(`Group/GroupsAndTranslations/${applicationId}/${isText}`)
            .then(res => {
                setGroups(res.data);
                setShowSpinner(false);
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.FetchDataError, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            });
    }

    const getSectionsAndTranslations = () => {
        setShowSpinner(true);
        const isText = translationTypeId === TranslationType.Text ? true : false;
        Axios.get(`Group/SectionsAndTranslations/${applicationId}/${isText}`)
            .then(res => {
                setSections(res.data);
                setShowSpinner(false);
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.FetchDataError, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            });
    }

    const moveTextToAnotherGroup = (newGroupId, textId) => {
        Axios.put('Group/MoveTextToAnotherGroup', null, { params: { newGroupId, textId } })
            .then((res) => {
                if (res.data) {
                } else if (!res.data) {
                    notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                }
            })
            .catch((err) => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            })
    }

    const movePdfToAnotherGroup = (newGroupId, pdfId) => {
        Axios.put('Group/MovePdfToAnotherGroup', null, { params: { newGroupId, pdfId } })
            .then((res) => {
                if (res.data) {
                } else if (!res.data) {
                    notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                }
            })
            .catch((err) => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            })
    }

    const removeTextFromGroup = (textId) => {
        Axios.put(`Group/RemoveTextFromGroup/${textId}`)
            .then((res) => {
                if (res.data) {
                } else if (!res.data) {
                    notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                }
            }).catch((err) => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            })
    }

    const removePdfFromGroup = (pdfId) => {
        Axios.put(`Group/RemovePdfFromGroup/${pdfId}`)
            .then((res) => {
                if (res.data) {
                } else if (!res.data) {
                    notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                }
            }).catch((err) => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            })
    }

    // Api call to update the groupIds of the texts.
    const updateTexts = (texts) => {
        const data = texts;
        Axios.put('Group/ChangeTextsGroupId', data)
            .then((res) => {
                if (res.data) {
                } else if (!res.data) {
                    notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                }
            })
            .catch((err) => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            })
    }

    // Api call to update the groupIds of the pdfs.
    const updatePdfs = (pdfs) => {
        const data = pdfs;
        Axios.put('Group/ChangePdfsGroupId', data)
            .then((res) => {
                if (res.data) {
                } else if (!res.data) {
                    notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                }
            })
            .catch((err) => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            })
    }

    // To change the selected section when a new section is selected from select dropdown.
    const handleSectionChange = (value) => {
        if (sections.length) {
            const sectionIndex = sections.findIndex((section) => section.id === value);
            if (sectionIndex > -1) {
                setSelectedSection(sections[sectionIndex]);
            }
        }
    }

    // To change the selected group when a new group is selected from group dropdown.
    const handleGroupChange = (value) => {
        if (groups.length) {
            const groupIndex = groups.findIndex((group) => group.id === value);
            if (groupIndex > -1) {
                setSelectedGroup(groups[groupIndex]);
            }
        }
    }

    // checks if the given text with textId has an associated groupId.
    // returns true if the case is true else returns false. 
    const isTextAssociatedWithGroup = (textId) => {
        let text = {};
        if (selectedSection && selectedSection.texts) {
            text = selectedSection.texts.find((text) => text.id === textId);
        }
        return text && text.groupId !== null ? true : false;
    }

    const isPdfAssociatedWithGroup = (pdfId) => {
        let pdf = {};
        if (selectedSection && selectedSection.pdfs) {
            pdf = selectedSection.pdfs.find((pdf) => pdf.id === pdfId);
        }
        return pdf && pdf.groupId !== null ? true : false;
    }

    // handles the drag and drop from section droppable to groups droppable.
    const onDragEnd = (result) => {
        const { destination, source } = result;

        if (!destination) {
            return;
        }

        if (source.droppableId === "sections" && destination.droppableId === "groups") {

            // If there is selectedSection or selectedGroup then do nothing.
            if (_.isEmpty(selectedSection) || _.isEmpty(selectedGroup)) {
                return;
            }

            const copiedSection = _.cloneDeep(selectedSection);
            const copiedGroup = _.cloneDeep(selectedGroup);

            if (translationTypeId === TranslationType.Pdf) {
                const copiedSectionPdfs = [...selectedSection.pdfs];
                const copiedGroupPdfs = [...selectedGroup.pdf];
                const [removedSectionPdf] = copiedSectionPdfs.slice(source.index, source.index + 1);

                // Checks if this pdf already exists in another group.
                // Remove the old group id from the pdf and set the group Id to new group id.
                if (isPdfAssociatedWithGroup(removedSectionPdf.id) && removedSectionPdf.groupId !== null) {
                    let pdfId = removedSectionPdf.id;
                    let newGroupId = selectedGroup.id;
                    let oldGroupId = removedSectionPdf.groupId;

                    // if the user drags the text associated with a group to the same group then do nothing.
                    if (newGroupId === oldGroupId) {
                        return;
                    }

                    // get the pdf with this pdfId
                    let pdf = selectedSection.pdfs.filter(pdf => pdf.id === pdfId);

                    //if the pdf is not found then do nothing.
                    if (!pdf.length) {
                        return;
                    }
                    pdf[0].groupId = newGroupId;

                    // remove the pdf with this id from the previous group 
                    let oldGroupIndex = groups.findIndex(group => group.id === oldGroupId);
                    let oldGroupCopy = groups[oldGroupIndex];
                    let oldGroupPdfsCopy = [...oldGroupCopy.pdf];
                    oldGroupPdfsCopy = oldGroupPdfsCopy.filter(pdf => pdf.id !== pdfId);
                    oldGroupCopy.pdf = [...oldGroupPdfsCopy];

                    //add the pdf with this id to the new Group
                    let newGroupIndex = groups.findIndex(group => group.id === newGroupId);
                    let newGroup = groups[newGroupIndex];
                    newGroup.pdf.push(pdf[0])

                    //update group state
                    let groupsCopy = [...groups];
                    groupsCopy[newGroupIndex] = newGroup;

                    // find selected group index
                    groupsCopy[oldGroupIndex] = oldGroupCopy;

                    setGroups(groupsCopy);

                    movePdfToAnotherGroup(selectedGroup.id, removedSectionPdf.id);
                } else {
                    removedSectionPdf.groupId = selectedGroup.id;

                    copiedGroupPdfs.splice(destination.index, 0, removedSectionPdf);
                    copiedGroupPdfs[destination.index].groupId = selectedGroup.id;

                    copiedSection.pdfs = copiedSectionPdfs;
                    copiedGroup.pdf = copiedGroupPdfs;

                    // update selected group and selected section.
                    setSelectedSection(copiedSection);
                    setSelectedGroup(copiedGroup);

                    //update selected section.
                    let newSections = sections && sections.filter((section) => section.id !== selectedSection.id);
                    newSections = [...newSections, copiedSection];
                    setSections(newSections);

                    //update selected group.
                    let newGroups = groups && groups.filter((group) => group.id !== selectedGroup.id);
                    newGroups = [...newGroups, copiedGroup];
                    setGroups(newGroups);

                    //api call to update the Text group Id
                    updatePdfsGroupId(removedSectionPdf);
                }
            }
            else {
                const copiedSectionTexts = [...selectedSection.texts];
                const copiedGroupTexts = [...selectedGroup.text];
                const [removedSectionText] = copiedSectionTexts.slice(source.index, source.index + 1);

                // Checks if this text already exists in another group.
                // Remove the old group id from the text and set the group Id to new group id.
                if (isTextAssociatedWithGroup(removedSectionText.id) && removedSectionText.groupId !== null) {
                    let textId = removedSectionText.id;
                    let newGroupId = selectedGroup.id;
                    let oldGroupId = removedSectionText.groupId;

                    // if the user drags the text associated with a group to the same group then do nothing.
                    if (newGroupId === oldGroupId) {
                        return;
                    }

                    // get the text with this textId
                    let text = selectedSection.texts.filter(text => text.id === textId);

                    //if the text is not found then do nothing.
                    if (!text.length) {
                        return;
                    }
                    text[0].groupId = newGroupId;

                    // remove the text with this id from the previous group 
                    let oldGroupIndex = groups.findIndex(group => group.id === oldGroupId);
                    let oldGroupCopy = groups[oldGroupIndex];
                    let oldGroupTextsCopy = [...oldGroupCopy.text];
                    oldGroupTextsCopy = oldGroupTextsCopy.filter(text => text.id !== textId);
                    oldGroupCopy.text = [...oldGroupTextsCopy];

                    //add the text with this id to the new Group
                    let newGroupIndex = groups.findIndex(group => group.id === newGroupId);
                    let newGroup = groups[newGroupIndex];
                    newGroup.text.push(text[0])

                    //update group state
                    let groupsCopy = [...groups];
                    groupsCopy[newGroupIndex] = newGroup;

                    // find selected group index
                    groupsCopy[oldGroupIndex] = oldGroupCopy;

                    setGroups(groupsCopy);

                    moveTextToAnotherGroup(selectedGroup.id, removedSectionText.id);
                } else {
                    removedSectionText.groupId = selectedGroup.id;

                    copiedGroupTexts.splice(destination.index, 0, removedSectionText);
                    copiedGroupTexts[destination.index].groupId = selectedGroup.id;

                    copiedSection.texts = copiedSectionTexts;
                    copiedGroup.text = copiedGroupTexts;

                    // update selected group and selected section.
                    setSelectedSection(copiedSection);
                    setSelectedGroup(copiedGroup);

                    //update selected section.
                    let newSections = sections && sections.filter((section) => section.id !== selectedSection.id);
                    newSections = [...newSections, copiedSection];
                    setSections(newSections);

                    //update selected group.
                    let newGroups = groups && groups.filter((group) => group.id !== selectedGroup.id);
                    newGroups = [...newGroups, copiedGroup];
                    setGroups(newGroups);

                    //api call to update the Text group Id
                    updateTextGroupId(removedSectionText);
                }
            }
        }
    }

    const updateTextGroupId = (text) => {
        Axios.put('Group/ChangeTextGroupId', text)
            .then((res) => {
                if (!res.data) {
                    notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                }
            })
            .catch((err) => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            })
    }

    const updatePdfsGroupId = (pdf) => {
        Axios.put('Group/ChangePdfGroupId', pdf)
            .then((res) => {
                if (!res.data) {
                    notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                }
            })
            .catch((err) => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            })
    }

    // handles the case when a text is removed is clicked on the content-menu.
    const handleRemoveFromGroupClick = (event, data, element) => {
        const textId = parseInt(element.children[0].getAttribute('data-id'));

        let selectedGroupCopy = _.cloneDeep(selectedGroup);

        if (translationTypeId === TranslationType.Pdf) {
            let selectedGroupPdfsCopy = [...selectedGroupCopy.pdf];
            selectedGroupPdfsCopy = selectedGroupPdfsCopy.filter(pdf => pdf.id !== textId);
            selectedGroupCopy.pdf = [...selectedGroupPdfsCopy];
            setSelectedGroup(selectedGroupCopy);

            //make the groupId of the pdf with this id as null in section
            let selectedSectionCopy = _.cloneDeep(selectedSection);
            let selectedSectionPdfsCopy = [...selectedSectionCopy.pdfs];

            //check if the text with this id is in the selected section.
            const pdfIndex = selectedSectionPdfsCopy.findIndex(pdf => pdf.id === textId);
            if (pdfIndex > -1) {
                //update the selected section state.
                selectedSectionPdfsCopy[pdfIndex].groupId = null;
                selectedSectionCopy.pdfs = [...selectedSectionPdfsCopy];
                setSelectedSection(selectedSectionCopy);

                //update section state
                let sectionsCopy = [...sections];
                const selectedSectionIndex = sectionsCopy.findIndex(section => section.id === selectedSection.id);
                sectionsCopy[selectedSectionIndex] = selectedSectionCopy;
                setSections([...sectionsCopy]);
            } else {
                //find the pdf with this pdfId in sections, update its groupId to null and update sections state.
                let pdfIndex;
                let sectionsCopy = [...sections];
                sectionsCopy = sectionsCopy.map(section => {
                    pdfIndex = section.pdfs.findIndex(pdf => pdf.id === textId);
                    if (pdfIndex > -1) {
                        section.pdfs[pdfIndex].groupId = null;
                    }
                    return section;
                });

                setSections([...sectionsCopy]);
            }


            //update group state
            let groupsCopy = [...groups];
            const selectedGroupIndex = groupsCopy.findIndex(group => group.id === selectedGroup.id);
            groupsCopy[selectedGroupIndex] = selectedGroupCopy;
            setGroups([...groupsCopy]);

            //make an api call to make the group id of the text set as null.
            removePdfFromGroup(textId);
        } else {
            // remove the text with this id from the group
            let selectedGroupTextsCopy = [...selectedGroupCopy.text];
            selectedGroupTextsCopy = selectedGroupTextsCopy.filter(text => text.id !== textId);
            selectedGroupCopy.text = [...selectedGroupTextsCopy];
            setSelectedGroup(selectedGroupCopy);

            //make the groupId of the text with this id as null in section
            let selectedSectionCopy = _.cloneDeep(selectedSection);
            let selectedSectionTextsCopy = [...selectedSectionCopy.texts];

            //check if the text with this id is in the selected section.
            const textIndex = selectedSectionTextsCopy.findIndex(text => text.id === textId);
            if (textIndex > -1) {
                //update the selected section state.
                selectedSectionTextsCopy[textIndex].groupId = null;
                selectedSectionCopy.texts = [...selectedSectionTextsCopy];
                setSelectedSection(selectedSectionCopy);

                //update section state
                let sectionsCopy = [...sections];
                const selectedSectionIndex = sectionsCopy.findIndex(section => section.id === selectedSection.id);
                sectionsCopy[selectedSectionIndex] = selectedSectionCopy;
                setSections([...sectionsCopy]);
            } else {
                //find the text with this textId in sections, update its groupId to null and update sections state.
                let textIndex;
                let sectionsCopy = [...sections];
                sectionsCopy = sectionsCopy.map(section => {
                    textIndex = section.texts.findIndex(text => text.id === textId);
                    if (textIndex > -1) {
                        section.texts[textIndex].groupId = null;
                    }
                    return section;
                });

                setSections([...sectionsCopy]);
            }


            //update group state
            let groupsCopy = [...groups];
            const selectedGroupIndex = groupsCopy.findIndex(group => group.id === selectedGroup.id);
            groupsCopy[selectedGroupIndex] = selectedGroupCopy;
            setGroups([...groupsCopy]);

            //make an api call to make the group id of the text set as null.
            removeTextFromGroup(textId);
        }
    }

    // handles the case when a text has to be moved to new group when selected through the context-menu.
    const handleMoveToGroupClick = (e, data, element) => {
        const newGroupId = data.id;
        const textId = parseInt(element.children[0].getAttribute('data-id'));

        if (translationTypeId === TranslationType.Pdf) {
            //get the pdf with this textId
            let pdf = selectedGroup.pdf.filter(pdf => pdf.id === textId);

            //if the pdf is not found then do nothing.
            if (!pdf.length) {
                return;
            }
            pdf[0].groupId = newGroupId;

            // remove the text with this id from the slectedGroup
            let selectedGroupCopy = _.cloneDeep(selectedGroup);
            let selectedGroupPdfsCopy = [...selectedGroupCopy.pdf];
            selectedGroupPdfsCopy = selectedGroupPdfsCopy.filter(pdf => pdf.id !== textId);
            selectedGroupCopy.pdf = [...selectedGroupPdfsCopy];
            setSelectedGroup(selectedGroupCopy);

            //add the pdf with this id to the new Group
            let newGroupIndex = groups.findIndex(group => group.id === newGroupId);
            let newGroup = groups[newGroupIndex];
            newGroup.pdf.push(pdf[0]);

            //update group state
            let groupsCopy = [...groups];
            groupsCopy[newGroupIndex] = newGroup;

            //find selected group index
            let selectedGroupIndex = groups.findIndex(group => group.id === selectedGroup.id);
            groupsCopy[selectedGroupIndex] = selectedGroupCopy;

            setGroups(groupsCopy);

            //Api call to set the new GroupId for the textId
            movePdfToAnotherGroup(newGroupId, textId);
        } else {
            //get the text with this textId
            let text = selectedGroup.text.filter(text => text.id === textId);

            //if the text is not found then do nothing.
            if (!text.length) {
                return;
            }
            text[0].groupId = newGroupId;

            // remove the text with this id from the slectedGroup
            let selectedGroupCopy = _.cloneDeep(selectedGroup);
            let selectedGroupTextsCopy = [...selectedGroupCopy.text];
            selectedGroupTextsCopy = selectedGroupTextsCopy.filter(text => text.id !== textId);
            selectedGroupCopy.text = [...selectedGroupTextsCopy];
            setSelectedGroup(selectedGroupCopy);

            //add the text with this id to the new Group
            let newGroupIndex = groups.findIndex(group => group.id === newGroupId);
            let newGroup = groups[newGroupIndex];
            newGroup.text.push(text[0]);

            //update group state
            let groupsCopy = [...groups];
            groupsCopy[newGroupIndex] = newGroup;

            //find selected group index
            let selectedGroupIndex = groups.findIndex(group => group.id === selectedGroup.id);
            groupsCopy[selectedGroupIndex] = selectedGroupCopy;

            setGroups(groupsCopy);

            //Api call to set the new GroupId for the textId
            moveTextToAnotherGroup(newGroupId, textId);
        }
    }

    const renderContextMenu = () => {
        const groupsForSubMenuitems = groups.filter((group) => group.id !== selectedGroup.id)
        return (
            <ContextMenu id={MENU_TYPE} ltr>
                <MenuItem onClick={handleRemoveFromGroupClick} data={{ id: selectedGroup.id }}>Remove</MenuItem>
                {groupsForSubMenuitems.length ?
                    <SubMenu title='Move to Group' ltr>
                        <PerfectScrollbar
                            className="mg-arrange__submenu"
                            options={{ wheelPropagation: true, minScrollbarLength: 50 }}
                        >
                            {
                                groupsForSubMenuitems.map((group) => {
                                    return <MenuItem onClick={handleMoveToGroupClick} data={{ id: group.id }} key={group.id}>{group.name}</MenuItem>
                                })
                            }
                        </PerfectScrollbar>
                    </SubMenu>
                    :
                    null
                }
            </ContextMenu>
        )
    }

    const getOphanTextCountForSection = (sectionId) => {
        let orphanTextCount = 0;

        if (sections.length) {
            const section = sections.filter(section => section.id === sectionId);
            if (translationTypeId === TranslationType.Text) {
                if (section.length && section[0].texts && section[0].texts.length) {
                    orphanTextCount = section[0].texts.reduce((acc, text) => {
                        if (text.groupId === null) {
                            acc += 1;
                        }
                        return acc;
                    }, 0)
                }
            } else {
                if (section.length && section[0].pdfs && section[0].pdfs.length) {
                    orphanTextCount = section[0].pdfs.reduce((acc, pdf) => {
                        if (pdf.groupId === null) {
                            acc += 1;
                        }
                        return acc;
                    }, 0)
                }
            }
        }
        return orphanTextCount;
    }

    const renderSectionOptions = () => {
        if (sections.length) {
            return sections.map((section) => {
                return (
                <Option key={section.id} value={section.id}> 
                    <span>
                        <span className="mg-arrange__sectionname">{section.name}</span>
                        <span className="mg-arrange__sectionbadge"><Badge count={getOphanTextCountForSection(section.id)} /></span>
                    </span>
                </Option>
                );
            })
        } else {
            return null;
        }
    }

    const renderGroupOptions = () => {
        if (groups.length) {
            return groups.map((group) => {
                return (
                    <Option key={group.id} value={group.id}>
                        <span className="mg-arrange__groupname">{group.name}</span>
                    </Option>
                )
            });
        } else {
            return null;
        }
    }

    const getSectionName = () => {
        if (sections.length) {
            if (!_.isEmpty(selectedSection)) {
                return (
                    <span>
                        <span className="mg-arrange__sectionname">{selectedSection.name}</span>
                        <span className="mg-arrange__sectionbadge"><Badge count={getOphanTextCountForSection(selectedSection.id)} /></span>
                    </span>
                )
            } else {
                return (
                    <span>
                        <span className="mg-arrange__sectionname">{sections[0].name}</span>
                        <span className="mg-arrange__sectionbadge"><Badge count={getOphanTextCountForSection(sections[0].id)} /></span>
                    </span>
                )
            }
        } else {
            return <span>{Strings.Texts.NoData}</span>
        }
    }

    const getGroupName = () => {
        if (groups.length) {
            if (!_.isEmpty(selectedGroup)) {
                return <span className="mg-arrange__groupname">{selectedGroup.name}</span>
            } else {
                return <span className="mg-arrange__groupname">{groups[0].name}</span>
            }
        } else {
            return <span>{Strings.Texts.NoData}</span>
        }
    }

    const renderSelectedGroupPdfs = () => {
        return selectedGroup.pdf && selectedGroup.pdf.length ?
            selectedGroup.pdf.map((pdf, index) => {
                if (pdf.pdfTranslations.length) {
                    return (
                        <ContextMenuTrigger id={MENU_TYPE} holdToDisplay={1000} key={pdf.id}>
                            <Row key={pdf.id} data-id={pdf.id}>
                                <Col className="mg-arrange__text mg-arrange__green" span={12} >{pdf.key}</Col>
                                <Col className="mg-arrange__text mg-arrange__green" span={12} >{pdf.pdfTranslations[0].translation}</Col>
                            </Row>
                        </ContextMenuTrigger>
                    )
                } else {
                    return null;
                }
            })
            :
            null
    }

    const renderSelectedSectionPdfs = () => {
        let isAssociatedWithGroup = false;
        if (!selectedSection.pdfs || !selectedSection.pdfs.length) {
            return null;
        } else {
            return selectedSection.pdfs.map((pdf, index) => {
                if (pdf.pdfTranslations.length) {
                    return (
                        <Draggable key={pdf.id} draggableId={pdf.id.toString()} index={index}>
                            {(provided, snapshot) => (
                                <div
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    style={{
                                        backgroundColor: snapshot.isDraggingOver ? 'blue' : '#fff',
                                        ...provided.draggableProps.style
                                    }}
                                >
                                    {isAssociatedWithGroup = isPdfAssociatedWithGroup(pdf.id)}
                                    <Row key={pdf.id}>
                                        <Col span={12} className={isAssociatedWithGroup ? 'mg-arrange__text mg-arrange__green' : 'mg-arrange__text mg-arrange__red'} >{pdf.key}</Col>
                                        <Col span={12} className={isAssociatedWithGroup ? 'mg-arrange__text mg-arrange__green' : 'mg-arrange__text mg-arrange__red'} >{pdf.pdfTranslations[0].translation}</Col>
                                    </Row>
                                </div>
                            )}
                        </Draggable>
                    )
                } else {
                    return null;
                }
            })
        }
    }

     var obj = {
        TextId: textId,
        SectionId: selectedSection.id,
        Translation: textValue,
        LanguageId: Languages.english,
        ReleaseId: editedReleaseId !== -1 ? editedReleaseId: translation.releaseId,
    }

    const renderReleaseOptions = () => {
        return (
            props.versions.map(ver => {
                return (
                    <Option value={ver.id} key={ver.id + ver.versionName} > { ver.versionName}</Option >
                );
            })
        );
    }
    const showModal = (value, id) => {
        setIsModalVisible(true);
        setTextValue(value);
        setTextId(id);
        const text = _.cloneDeep(selectedSection.texts.find(t => t.id === id));
        setTranslation(text);
        className = isTextAssociatedWithGroup(id) ? 'mg-arrange__text mg-arrange__green' : 'mg-arrange__text mg-arrange__red';
   };
    
    const handleOk = () => {
        setShowSpinner(true);
        saveTranslationChanges(obj);
        setIsModalVisible(false);
        setRenderSave(false);
    };
   
    const handleCancel = () => {
        setRenderSave(false);
        setIsModalVisible(false);
    };
    
    const handleTextChange = (event) => {
        const text = event.target.value;
        if (text != textValue) {
             setRenderSave(true);
        }
        setTextValue(text);
    }

    const handleReleaseChange = (value) => {
        setRenderSave(translation.releaseId!=value);
        setEditedReleaseId(value);
    }

    const renderSectionTranslations = (isAssociatedWithGroup,text) => {
        return (
            <Row key={text.id}>
                <Col span={12} className={isAssociatedWithGroup ? 'mg-arrange__text mg-arrange__green' : 'mg-arrange__text mg-arrange__red'}  >{text.key}</Col>
                <Col 
                    span={12} className={isAssociatedWithGroup ? 'mg-arrange__text mg-arrange__green' : 'mg-arrange__text mg-arrange__red'}
                    onDoubleClick={()=>showModal(text.textTranslations[0].translation,text.id)} >{text.textTranslations[0].translation}
                </Col>
            </Row>
        );
    }

    const saveTranslationChanges = (obj) => {
       return Axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/SaveTranslationGroup', obj)
            .then(() => {
                getSectionsAndTranslations();
                getGroupsAndTranslations();
                notify.show(Strings.Texts.SavedSuccess,"custom",5000,{background:'#008000',text:"#FFFFFF"})
                return true; // on success;
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.FetchDataError, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
           });
    }
    const renderSelectedGroupTexts = () => {
        return selectedGroup.text && selectedGroup.text.length ?
            selectedGroup.text.map((text, index) => {
                if (text.textTranslations.length) {
                    return (
                        <ContextMenuTrigger id={MENU_TYPE} holdToDisplay={1000} key={text.id}>
                            <Row key={text.id} data-id={text.id}>
                                <Col className="mg-arrange__text mg-arrange__green" span={12} >{text.key}</Col>
                                <Col className="mg-arrange__text mg-arrange__green" span={12} >{text.textTranslations[0].translation}</Col>
                            </Row>
                        </ContextMenuTrigger>
                    )
                } else {
                    return null;
                }
            })
            :
            null
    }

    const renderSelectedSectionTexts = () => {
        let isAssociatedWithGroup = false;
        if (!selectedSection.texts || !selectedSection.texts.length) {
            return null;
        } else {
            return selectedSection.texts.map((text, index) => {
                if (text.textTranslations.length) {
                    return (
                        <Draggable key={text.id} draggableId={text.id.toString()} index={index}>
                            {(provided, snapshot) => (
                                <div
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    style={{
                                        backgroundColor: snapshot.isDraggingOver ? 'blue' : '#fff',
                                        ...provided.draggableProps.style
                                    }}
                                >
                                    {isAssociatedWithGroup = isTextAssociatedWithGroup(text.id)}
                                   {renderSectionTranslations(isAssociatedWithGroup,text)}
                                </div>
                            )}
                        </Draggable>
                    )
                } else {
                    return null;
                }
            })
        }
    }

    // handle the case when arrow button is clicked to move all the text with null groupId to current selected group.
    const handleMultiTextMove = () => {
        // case for multiple pdfs move
        if (translationTypeId === TranslationType.Pdf) {
            // Check if there is any pdf in the selected section for which group Id is null
            if (selectedSection.pdfs.some(pdf => pdf.groupId === null)) {
                // for all the pdfs in the current section whose groupId is null change that to selectedGroup id.
                // push all the pdfs to the selected group
                let selectedSectionCopy = _.cloneDeep(selectedSection);
                let selectedSectionPdfsCopy = [...selectedSectionCopy.pdfs];

                // Update the selected group so that pdf from sections are rendered in the group.
                let selectedGroupCopy = _.cloneDeep(selectedGroup);

                // pdfs for which groupIds need to be changed in the database.
                let pdfsToUpdate = [];

                selectedSectionPdfsCopy = selectedSectionPdfsCopy.map(pdf => {
                    if (pdf.groupId === null) {
                        selectedGroupCopy.pdf.push(pdf);
                        pdf.groupId = selectedGroup.id;
                        pdfsToUpdate.push(pdf);
                    }
                    return pdf;
                });
                selectedSectionCopy.pdfs = [...selectedSectionPdfsCopy];

                //update the selected section state.
                setSelectedSection(selectedSectionCopy);

                //update the selected group state
                setSelectedGroup(selectedGroupCopy);

                //update the sections state.
                const selectedSectionIndex = sections.findIndex(section => section.id === selectedSection.id);
                let sectionsCopy = [...sections];
                sectionsCopy[selectedSectionIndex] = selectedSectionCopy;
                setSections([...sectionsCopy]);

                //update the groups state.
                const selectedGroupIndex = groups.findIndex(group => group.id === selectedGroup.id);
                let groupsCopy = [...groups];
                groupsCopy[selectedGroupIndex] = selectedGroupCopy;
                setGroups([...groupsCopy]);

                //api call to update group id for texts
                updatePdfs(pdfsToUpdate);

                notify.show("All Pdfs moved.", "custom", 2500, { background: '#52c41a', text: "#FFFFFF" });
            } else {
                notify.show("No Pdf to move from current section.", "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            }
        } else {
            // Check if there is any text in the selected section for which group Id is null
            if (selectedSection.texts.some(text => text.groupId === null)) {
                // for all the text in the current section whose groupId is null change that to selectedGroup id.
                // push all the texts to the selected group
                let selectedSectionCopy = _.cloneDeep(selectedSection);
                let selectedSectionTextsCopy = [...selectedSectionCopy.texts];

                // Update the selected group so that text from sections are rendered in the group.
                let selectedGroupCopy = _.cloneDeep(selectedGroup);

                // texts for which groupIds need to be changed in the database.
                let textsToUpdate = [];

                selectedSectionTextsCopy = selectedSectionTextsCopy.map(text => {
                    if (text.groupId === null) {
                        selectedGroupCopy.text.push(text);
                        text.groupId = selectedGroup.id;
                        textsToUpdate.push(text);
                    }
                    return text;
                });
                selectedSectionCopy.texts = [...selectedSectionTextsCopy];

                //update the selected section state.
                setSelectedSection(selectedSectionCopy);

                //update the selected group state
                setSelectedGroup(selectedGroupCopy);

                //update the sections state.
                const selectedSectionIndex = sections.findIndex(section => section.id === selectedSection.id);
                let sectionsCopy = [...sections];
                sectionsCopy[selectedSectionIndex] = selectedSectionCopy;
                setSections([...sectionsCopy]);

                //update the groups state.
                const selectedGroupIndex = groups.findIndex(group => group.id === selectedGroup.id);
                let groupsCopy = [...groups];
                groupsCopy[selectedGroupIndex] = selectedGroupCopy;
                setGroups([...groupsCopy]);

                //api call to update group id for texts
                updateTexts(textsToUpdate);

                notify.show("All Texts moved.", "custom", 2500, { background: '#52c41a', text: "#FFFFFF" });
            } else {
                notify.show("No Text to move from current section.", "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            }
        }
    }

    const renderMoveAllTextArrow = () => {
        if (!_.isEmpty(selectedGroup) && !_.isEmpty(selectedSection) && (selectedSection.texts.length || selectedSection.pdfs.length)) {
            return <Col span={2} style={{ textAlign: 'center', height: '500px' }}>
                <Tooltip title="Move all texts">
                    <Icon type="arrow-right" onClick={() => handleMultiTextMove()} style={{ fontSize: '32px', marginTop: '250px', marginLeft: '50px' }} />
                </Tooltip>
            </Col>
        } else {
            return <Col span={2}></Col>
        }
    }

    const renderModal = () => {
        return (
            <Modal title="Translation"
                visible={isModalVisible}
                onOk={handleOk}
                footer={[
                    <Button key="cancel" type="danger" onClick={handleCancel} >Cancel</Button>, <Button
                        key="submit" type="primary" onClick={handleOk}
                        disabled={!(shouldRenderSave)}>Save</Button>
                ]}
                destroyOnClose={true}
                onCancel={handleCancel}
            >
                <div id="modal-translation">
                    <Select
                        style={{ marginBottom: "20px", width: "50%" }}
                        defaultValue={translation.releaseId}
                        autosize={{ minRows: 3 }}
                        onChange={handleReleaseChange}   
                    >
                    {renderReleaseOptions()}
                    </Select>
                    <Input value={translation.key} readOnly className={className} style={{ marginBottom: "20px" }} />
                    <Input.TextArea
                        autosize={{ minRows: 3 }}
                        autoFocus
                        onFocus={(e) => e.target.setSelectionRange(textValue.length, textValue.length)}
                        className={className}
                        onChange={handleTextChange}
                        data-name="translation"
                        placeholder="Translation"
                        value={textValue}
                    />
                </div>
            </Modal>
        );
    }

    return (
        <div className="mg-arrange">
            {showSpinner ? <div className="pdf-page-loader"><Spin size="large"></Spin></div> : ''}
            <DragDropContext onDragEnd={onDragEnd}>
                <Row className="mg-arrage__selectList">
                    <Col span={10} offset={1}>
                        <Row>
                            Section &nbsp;
                                        <Select  
                                        value={getSectionName()}
                                        onChange={handleSectionChange}
                                        className="mg-arrange__sectionselect"
                                        dropdownClassName="mg-arrange__sectiondropdown"
                                        style={{ width: 200 }} 
                                        >
                                            {renderSectionOptions()}
                                        </Select>
                        </Row>
                    </Col>
                    <Col span={10} offset={3}>
                        Group &nbsp;
                                    <Select  
                                        value={getGroupName()}
                                        style={{ width: 200 }} 
                                        onChange={handleGroupChange}
                                        className="mg-arrange__groupselect"
                                        dropdownClassName="mg-arrange__groupdropdown"
                                    >
                                        {renderGroupOptions()}
                                    </Select>
                    </Col>
                </Row>
                <Row>
                    <Col span={10} offset={1} className="mg-arrage__section">
                        <PerfectScrollbar
                            className="mg-arrage__scrollbar"
                            options={{ wheelPropagation: true, minScrollbarLength: 50 }}
                        >
                            <Droppable droppableId="sections">
                                {(provided, snapshot) => (
                                    <div
                                        ref={provided.innerRef}
                                        className="mg-arrange__sectiondroppable"
                                        {...provided.droppableProps}
                                    >
                                        <Row className="mg-arrange__sections-row">
                                            <Col span={23} style={{ margin: '0 14px' }}>
                                                {
                                                    translationTypeId === TranslationType.Text ? renderSelectedSectionTexts() : renderSelectedSectionPdfs()
                                                }
                                            </Col>
                                        </Row>
                                        {provided.placeholder}
                                    </div>
                                )}
                            </Droppable>
                        </PerfectScrollbar>
                    </Col>
                    {renderMoveAllTextArrow()}
                    <Col span={10} offset={1} className="mg-arrage__section">
                        <Droppable droppableId="groups">
                            {(provided, snapshot) => (
                                <div
                                    ref={provided.innerRef}
                                    className="mg-arrange__groupdroppable"
                                    {...provided.droppableProps}
                                >
                                    <PerfectScrollbar
                                        className="mg-arrage__scrollbar"
                                        options={{ wheelPropagation: true, minScrollbarLength: 50 }}
                                    >
                                        <Row className="mg-arrange__groups-row">
                                            <Col span={23} style={{ margin: '0 14px' }}>
                                                {
                                                    translationTypeId === TranslationType.Text ? renderSelectedGroupTexts() : renderSelectedGroupPdfs()
                                                }
                                            </Col>
                                        </Row>
                                    </PerfectScrollbar>
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </Col>
                </Row>
            </DragDropContext>
            {renderContextMenu()}
            <Notifications />
            {renderModal()}          
        </div>
    )
}
