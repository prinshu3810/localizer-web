import React, { Component } from 'react';
import { Row, Col, Input, Button, Form, Card } from 'antd';
import axios from 'axios';
import Notifications, { notify } from 'react-notify-toast';
import Strings from '../constants/String';
import Header from './Header';
import { UpdatePasswordStatus, PageRoutes } from '../constants/Constants';
import Roles from '../constants/Constants';

class ChangePassword extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            loading: false,
            confirmDirty: false
        };
    }

    userInfo = localStorage.getItem("gsuser") ? JSON.parse(localStorage.getItem("gsuser")) : null;
    isPasswordFirstReset = false;

    componentWillMount() {
        if (!this.userInfo) {
            this.props.history.push('/');
        }
    }

    handleUpdatePassword = (e) => {
        let that = this;
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {

            if (!err) {
                this.setState({ loading: true });
                var obj = {
                    'CurrentPassword': values.currentPassword,
                    'NewPassword': values.password,
                    'Username': this.userInfo.username
                };
                let headers = {
                    authorization: `Bearer ${JSON.parse(localStorage.getItem("gsuser")).token}`
                };
                axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'Login/ChangePassword', obj, { headers: headers })
                    .then(res => {
                        this.setState({ loading: false });
                        if (res.data.statusCode == UpdatePasswordStatus.Success) { // successfully updated
                            that.props.form.resetFields();
                            that.showToast(Strings.Texts.Success, "The password was updated successfully");
                            that.userInfo.token = res.data.token;
                            // check if password updated with OTP
                            that.isPasswordFirstReset = !that.userInfo.isPasswordVerified;
                            that.userInfo.isPasswordVerified = true;
                            //reset token for updated password
                            localStorage.setItem("gsuser", JSON.stringify(that.userInfo));
                        }
                        else if (res.data.statusCode == UpdatePasswordStatus.CurrentPasswordWrong) {// wrong current password
                            that.showToast(Strings.Texts.Danger, "It seems you have entered the password incorrectly");
                        }
                        else {
                            that.showToast(Strings.Texts.Danger, "Some error occured!");
                        }
                    })
                    .catch(error => {
                        this.showToast(Strings.Texts.Danger, "It seems some error occured!");
                        console.log(error);
                        this.setState({ loading: false });
                    });
            }
        });
    }

    showToast = (type, message) => {
        let color = "";
        if (type == Strings.Texts.Danger) {
            color = { background: '#ee3647', text: "#FFFFFF" };
            notify.show(message, "custom", 2500, color);
        }
        else if (type == Strings.Texts.Info) {
            color = { background: '#0E1717', text: "#FFFFFF" };
            notify.show(message, "custom", 2000, color);
        }
        else if (type == Strings.Texts.Success) {
            color = { background: '#008000', text: "#FFFFFF" };
            notify.show(message, "custom", 5000, color);
        }
    }

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            this.handleUpdatePassword(event);
        }
    }

    handleCancelButton = () => {
        if (this.isPasswordFirstReset) {
            this.props.history.push("/Translations");
        }
        else if (this.userInfo.isPasswordVerified) {
            let previousPage = localStorage.getItem("previousPage") ? localStorage.getItem("previousPage") : "Translations";
            this.props.history.push(previousPage);
        } else {
            this.props.history.push("/");
        }
    }

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }
    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('New Password and Confirm Password mismatched!');
        } else {
            callback();
        }
    }

    passwordRangeValidation = (rule, value, callback) => {
        const form = this.props.form;
        if (value && (value.length < 7 || value.length > 30)) {
            callback('Password length should be between 7 and 30');
        } else {
            callback();
        }
    }

    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true });
        }
        callback();
    }

    hasErrors() {
        const { getFieldsError, isFieldTouched } = this.props.form;
        let fieldsError = getFieldsError();
        let isTouched = isFieldTouched('currentPassword') && isFieldTouched('password') && isFieldTouched('confirm');

        return !isTouched || Object.keys(fieldsError).some(field => fieldsError[field]);
    }

    isOnlyTranslator = (userRoles) => {
        return userRoles.length === 1 && userRoles[0] === Roles.Translator;
    }

    render() {
        const FormItem = Form.Item;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 5 },
            wrapperCol: { span: 12 },
        };

        // if the user is a translator and path is not equal to 'profile' then redirect to '/profile'
        const userRoles = JSON.parse(localStorage.getItem('gsuser')).roles;
        if(this.isOnlyTranslator(userRoles) && this.props.match && this.props.match.params.path !== '/profile'){
            this.props.history.push("/profile");
        }

        return (
            <Row xs={20} >
            {
                this.props.match && this.props.match.path === '/ChangePassword' && 
                <Header history={this.props.history} pageRoute={PageRoutes.ChangePassword} />
            }
                <div className={this.isOnlyTranslator(userRoles) ? "change-password-translator" : "change-password"}>
                    <Card title="Change Password" className="profile-card">
                        <Row className="login">
                            <Col lg={18}>
                                <Form onSubmit={this.handleUpdatePassword} className="login-form">
                                    <Row md={20} lg={18}>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Current Password">
                                        {getFieldDecorator('currentPassword', {
                                            initialValue: '',
                                            rules: [{ required: true, message: 'Required!' }],
                                        })(
                                            <Input type="password" placeholder="Current Password" name="currentPassword" onKeyPress={this.handleKeyPress} />
                                        )}
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="New Password">
                                        {getFieldDecorator('password', {
                                            initialValue: '',
                                            rules: [{ required: true, message: 'Required!' },
                                                , {
                                                validator: this.passwordRangeValidation,
                                            },
                                            {
                                                validator: this.validateToNextPassword,
                                            }],
                                        })(
                                            <Input type="password" placeholder="New Password" name="newPassword" onKeyPress={this.handleKeyPress} />
                                        )}
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Confirm Password">
                                        {getFieldDecorator('confirm', {
                                            initialValue: '',
                                            rules: [{
                                                required: true, message: 'Required!',
                                            }, {
                                                validator: this.compareToFirstPassword,
                                            }],
                                        })(
                                            <Input type="password" name="confirmPassword" placeholder="Confirm Password" onKeyPress={this.handleKeyPress}
                                                onBlur={this.handleConfirmBlur} />

                                        )}
                                    </FormItem>
                                    </Row>
                                    <Row>
                                        <Col  sm={17} className="change-password-align-end">
                                            <Button disabled={this.hasErrors()} type="primary" htmlType="submit" loading={this.state.loading} >Update</Button>
                                            &nbsp;<Button onClick={this.handleCancelButton}>Cancel</Button>
                                        </Col>
                                    </Row>
                                </Form>
                            </Col>
                        </Row>
                    </Card>
                    <Notifications />
                </div>

            </Row >
        );
    }
}
export default Form.create()(ChangePassword);