import React, { Component } from 'react';
import { Row, Col, Input, Button, Icon, Drawer, Form, Checkbox, Modal, Radio, Spin, Select } from 'antd';
import axios from 'axios';
import _ from 'lodash';
import { PageRoutes } from '../constants/Constants';
import { checkForDuplicates } from '../constants/Common'
import Notifications, { notify } from 'react-notify-toast';
import Strings from '../constants/String';
import UploadForBulkImport from './Upload';
import RadioGroup from 'antd/lib/radio/group';
import Header from './Header';
import { ImportModes, TranslationType } from '../constants/Constants';
import { splitJsonFile } from './Upload';

const RESET_COMMANDS = {
    RESET_NEW_EDIT_SECTIONS: 'RESET_NEW_EDIT_SECTIONS',
    RESET_NEW_SECTIONS: 'RESET_NEW_SECTIONS',
    RESET_LOADING: 'RESET_LOADING',
    RESET_TRANSLATION_TYPE: 'RESET_TRANSLATION_TYPE',
    RESET_EDIT_DRAWER_VISIBILITY: 'RESET_EDIT_DRAWER_VISIBILITY'
}

const SET_COMMANDS = {
    SET_LOADING: 'SET_LOADING',
}

const { Option } = Select;

const validateSectionName = (sections, message) => {
    if (sections.length > 0 && sections.find(x => x.name.trim() === '')) {
        message = 'Section name is required';
    }

    return message;
}

const validateAppNameLength = (name, message) => {
    if (!name) {
        message = 'App name is required.';
    }

    return message;
}

const validateSectionCount = (newSections, existingSections, sectionsToDelete, sectionsToUpload) => {
    /* At least one section should be present in an application. So, 
       (No. of new sections + existing sections - sections marked to delete + sections present in upload) > 0
    */
    return newSections.length +
        (existingSections.length - sectionsToDelete.length)
        + sectionsToUpload.length > 0;
}

class Developer extends Component {
    counter = 1;
    editAppCounter = 1;
    headers = {
        authorization: localStorage.getItem("gsuser") ? `Bearer ${JSON.parse(localStorage.getItem("gsuser")).token}` : null
    };
    hideImportAppOption = false;
    userRole = 0;
    user = null;
    userinfo = {};
    applicationToEdit = {};
    importedFileContent = {};
    fileListWithLanguage = [];
    applicationEditInfo = {
        id: 0,
        name: null,
        sectionsToEdit: [],
        sectionsToDelete: [],
        newSections: [],
        sectionsToUpload: [],
        translationTypeId: 0
    };
    appUngroupedKeys = [];

    constructor() {
        super();
        this.state = {
            applications: [],
            showAddOption: false,
            showImportOption: false,
            newApp: '',
            newSections: [],
            allLanguages: [],
            uploadModalVisible: false,
            importedFileList: [],
            isEditDrawerVisible: false,
            newEditSections: [],
            selectedTranslationType: null,
            loading: false,
            removeEnglishForOtherLang: true,
            version: [],
            newAppRelease: 0,

        }
        localStorage.setItem("previousPage", "Admin");
    }

    addSections = () => {
        const temp = {
            Id: this.counter++,
            name: '',
            displayName: '',
            isVisible: true
        };

        const sections = this.state.newSections;
        sections.push(temp);
        this.setState({ newSections: sections });
    }

    validateAddApp() {
        let message = "";
        let index = this.state.applications.findIndex(x => x.Name.trim().toLowerCase() === this.state.newApp.trim().toLowerCase());

        message = validateSectionName(this.state.newSections, message);

        if (this.state.newSections.length === 0) {
            message = "Please add at least one section.";
        }

        if (index > -1) {
            message = "App name already exists.";
        }

        message = validateAppNameLength(this.state.newApp, message);

        if (!this.state.selectedTranslationType) {
            message = "Please select translation type(Text | Pdf)."
        }

        // INH-69: Duplicate section names are not allowed in an application.
        const isDuplicateFound = checkForDuplicates(this.state.newSections.map(item => item.name));

        if (isDuplicateFound) {
            message = 'Section names cannot be duplicate';
        }

        if (message != "") {
            Modal.warning({
                title: 'Warning',
                content: (
                    <div>
                        <p>{message}</p>
                    </div>
                )
            });
            return false;
        }
        return true;
    }

    checkForDuplicateSection(source) {
        let isDuplicateFound = false;

        for (let i = 0; i < source.length; i++) {
            const baseElement = source[i];
            const sourceOtherThanBase = source.filter(item => item.Id !== baseElement.Id);

            isDuplicateFound = sourceOtherThanBase
                .map(item => item.name)
                .filter(item => item === baseElement.name).length > 0;

            if (!isDuplicateFound && baseElement.displayName) {
                isDuplicateFound = sourceOtherThanBase
                    .filter(item => item.displayName)
                    .map(item => item.displayName)
                    .filter(item => item === baseElement.displayName).length > 0;
            }

            if (isDuplicateFound) {
                break;
            }
        }

        return isDuplicateFound;
    }

    validateEditApp = () => {
        if (!this.applicationEditInfo) {
            return true;
        }

        // concat existing edited section with new section
        let sectionToCheck = this.applicationEditInfo.sectionsToEdit.reduce((acc, section) => {
            acc.push({
                Id: section.Id,
                applicationId: section.ApplicationId,
                displayName: section.DisplayName,
                isvisible: section.IsVisible,
                name: section.Name
            });
            return acc;
        }, []).concat(this.applicationEditInfo.newSections);

        let message = validateSectionName(sectionToCheck, message);
        message = validateAppNameLength(this.applicationEditInfo.name, message);

        if (!message && this.applicationEditInfo.newSections && this.applicationToEdit.Section) {
            const isDuplicateFound = this.checkForDuplicateSection(this.applicationToEdit.Section.map(item => {
                let itemData = item;
                if (this.applicationEditInfo.sectionsToEdit.length) {
                    const editSection = this.applicationEditInfo.sectionsToEdit.find(s => s.Id === item.Id);

                    if (editSection) {
                        itemData = editSection;
                    }
                }

                return {
                    Id: itemData.Id,
                    displayName: itemData.DisplayName.trim(),
                    name: itemData.Name.trim()
                }
            }).concat(this.applicationEditInfo.newSections.map(item => { return { Id: item.Id, displayName: item.displayName, name: item.name } })));

            if (isDuplicateFound) {
                message = 'Section names and display names cannot be duplicate';
            }
        }

        // A section marked to delete should not be edited.
        if (!message && this.applicationEditInfo.sectionsToDelete.length && this.applicationEditInfo.sectionsToEdit.length) {
            const index = this.applicationEditInfo.sectionsToEdit.findIndex(item => this.applicationEditInfo.sectionsToDelete.indexOf(item.Id) >= 0);
            message = index >= 0 ? 'Section marked to delete cannot be edited' : message;
        }

        if (message) {
            Modal.warning({
                title: 'Warning',
                content: (
                    <div>
                        <p>{message}</p>
                    </div>
                )
            });
            return false;
        }
        return true;
    }

    addApp = () => {
        if (!this.validateAddApp()) {
            return;
        }

        this.setDrawerState([SET_COMMANDS.SET_LOADING]);

        const obj = {
            name: this.state.newApp,
            isVisible: this.state.selectedSection,
            sections: this.state.newSections.map(section => {
                section.Id = 0;
                return section;
            }),
            translationTypeId: this.state.selectedTranslationType,
        };

        axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'Admin/AddApplicationAndSections', obj, { headers: this.headers })
            .then(res => {
                this.resetDrawerState([RESET_COMMANDS.RESET_LOADING]);
                this.getAppsAndSections();
            }).catch(error => {
                this.errorHandler(error);
            });
    }

    deleteApp = (event) => {
        const target = event.currentTarget;
        const id = target.getAttribute("data-id");
        const self = this;

        Modal.confirm({
            title: Strings.Texts.DeleteAppConfirmation,
            content: '',
            okText: Strings.Texts.Yes,
            cancelText: Strings.Texts.No,
            onOk() {
                self.setDrawerState([SET_COMMANDS.SET_LOADING]);
                axios.get(Strings.URL.GcLocalizerServiceBaseUrl + 'Admin/DeleteApp?Id=' + id, { headers: self.headers })
                    .then(() => {
                        self.getAppsAndSections()
                            .then(() => {
                                self.resetDrawerState([RESET_COMMANDS.RESET_LOADING]);
                                self.showToast(Strings.Texts.Success, Strings.Texts.AppDeleteSucess);
                            })
                            .catch(() => {
                                self.resetDrawerState([RESET_COMMANDS.RESET_LOADING]);
                            });
                    }).catch(error => {
                        self.errorHandler(error);
                    });
            },
            onCancel() {

            }
        });
    }

    showAddAppOption = () => {
        this.setState({
            showAddOption: true
        });
    }

    hideAddAppOption = () => {
        this.setState({
            newApp: "",
            newSections: [],
            selectedTranslationType: null,
            showAddOption: false
        });
    }

    showImportAppOption = () => {
        this.setState({
            showImportOption: true
        });
    }

    hideImportAppOption = () => {
        this.setState({
            newApp: "",
            newAppRelease: 0,
            showImportOption: false
        });
    }

    handleChange = (event) => {
        const target = event.target,
            value = target.value;
        this.setState({ [target.getAttribute("data-name")]: value });
    }

    componentWillMount() {
        this.setDrawerState([SET_COMMANDS.SET_LOADING]);
        this.user = JSON.parse(localStorage.getItem("gsuser"));
        if (!this.user) {
            this.props.history.push('/');
            return;
        }
        else if (!this.user.isPasswordVerified) {
            this.props.history.push('/ChangePassword');
            return;
        }
        this.getAppsAndSections();
        this.getLanguages();
        this.getVersions();
        this.handleChange = this.handleChange;
    }

    getLanguages = () => {
        axios.get(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/GetAllLanguages', { headers: this.headers })
            .then(res => {
                this.setState({ allLanguages: res.data.languages })

            }).catch(error => {
                this.errorHandler(error);
            });

    }

    errorHandler = (error) => {
        if (error.response && error.response.status == 401) {
            localStorage.removeItem("gsuser");
            this.props.history.push('/');
        }
        else {
            console.log(error);
        }

        if (error && error.response && error.response.data) {
            let message;
            let title;

            switch (error.response.data.error) {
                case "ApplicationIsLockedException":
                    message = 'Application is locked.';
                    title = 'Locked!';
                    break;

                default:
                    message = 'Something went wrong';
                    title = 'Error!';
                    break;
            }

            Modal.error({
                title: title,
                content: (
                    <div>
                        <p>{message}</p>
                    </div>
                ),
                onOk() { }
            });
        }

        this.setState({
            loading: false
        });
    }

    getVersions = () => {
        axios.get(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/GetVersionDetails', { headers: this.headers })
            .then(res => {
                this.setState({ version: res.data })
            }).catch(error => {
                this.errorHandler(error);
            });
    }

    selectedRelease = (value) => {
        this.setState({ newAppRelease: value })
    }

    renderReleaseVersion = () => {
        return this.state.version.map((ver) => {
            return (
                <Option value={ver.id}>{ver.versionName}</Option>
            );
        });
    }

    getAppsAndSections = () => {
        const userInfo = axios.get(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/GetUserInfo?id=' + this.user.id, { headers: this.headers });
        const ungroupedKeysCount = axios.get(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/AppUngroupedKeysCount', { headers: this.headers });

        axios.all([userInfo, ungroupedKeysCount])
            .then(axios.spread((...responses) => {
                const userInfoResponse = responses[0]
                const ungroupedKeysCountResponse = responses[1]

                this.userRole = userInfoResponse.data.userinfo.role;
                this.userinfo = userInfoResponse.data.userinfo;
                this.applications = JSON.parse(userInfoResponse.data.application);
                this.appUngroupedKeys = ungroupedKeysCountResponse.data.textAppUngroupedKeys.concat(ungroupedKeysCountResponse.data.pdfAppUngroupedKeys);

                if (this.applications && this.applications[0]) {
                    this.sections = this.applications[0].sections;
                }

                if (this.sections && this.sections[0]) {
                    this.selectedsection = this.sections[0].Id;
                }

                this.setState({
                    applications: this.applications,
                    sections: this.sections,
                    showAddOption: false,
                    newSections: [],
                    selectedTranslationType: null,
                    loading: false
                });
            }))
            .catch(error => {
                this.resetDrawerState([RESET_COMMANDS.RESET_LOADING]);
                this.errorHandler(error);
            });
    }

    handleChange = (event) => {
        const target = event.target,
            value = target.value;
        this.setState({ [target.getAttribute("data-name")]: value });
    }

    handleSectionChange = (event) => {
        const target = event.target,
            value = target.value,
            id = parseInt(target.getAttribute("data-id")),
            name = target.getAttribute("data-name"),
            temp = this.state.newSections;

        for (let i = 0; i < temp.length; i++) {
            if (temp[i].Id === id) {
                temp[i][name] = value;
                break;
            }
        }

        this.setState({ translations: temp });
    }

    showToast = (type, message) => {
        let color = "";
        if (type == "danger") {
            color = { background: '#ee3647', text: "#FFFFFF" };
            notify.show(message, "custom", 2500, color);
        }
        else if (type == "info") {
            color = { background: '#0E1717', text: "#FFFFFF" };
            notify.show(message, "custom", 2000, color);
        }
        else if (type == "success") {
            color = { background: '#008000', text: "#FFFFFF" };
            notify.show(message, "custom", 5000, color);
        }
    }

    deleteSectionClicked(event, sectionId) {
        let newSections = this.state.newSections.filter(x => x.Id !== sectionId);
        this.setState({ newSections: newSections });
    }

    onNewEditSectionsDeleteClick = (event, sectionId) => {
        this.setState(prevState => {
            prevState.newEditSections.splice(prevState.newEditSections.findIndex(item => item.Id === sectionId), 1);
            return prevState;
        });
    }

    hideEditApplication = () => {
        this.setState({
            isEditDrawerVisible: false
        });

        this.resetDrawerState([RESET_COMMANDS.RESET_LOADING, RESET_COMMANDS.RESET_NEW_EDIT_SECTIONS]);
        this.applicationEditInfo.newSections = [];
        this.applicationEditInfo.sectionsToDelete = [];
        this.editAppCounter = 0;
    }

    editApplication = (event) => {
        const id = event.currentTarget.getAttribute('data-id');
        this.applicationToEdit = _.cloneDeep(this.state.applications.find(application => application.Id == id)); // deep clone to avoid direct mutation of state.
        this.applicationEditInfo.name = this.applicationToEdit.Name;
        this.applicationEditInfo.id = this.applicationToEdit.Id;
        this.applicationEditInfo.translationTypeId = this.applicationToEdit.TranslationTypeId;

        this.editAppCounter = this.applicationToEdit.Section.length;
        this.setState({
            isEditDrawerVisible: true
        });
    }

    handleAppNameChange = (event) => {
        this.applicationEditInfo.name = event.target.value;
    }

    markSectionsToDelete = (event) => {
        const id = event.target['data-id'];

        if (id < 0) {
            return;
        }

        if (event.target.checked) {
            this.applicationEditInfo.sectionsToDelete.push(id);
        } else {
            const index = this.applicationEditInfo.sectionsToDelete.findIndex(appId => appId == id);

            if (index >= 0) {
                this.applicationEditInfo.sectionsToDelete.splice(index, 1);
            }
        }
    }

    editAddSection = (event) => {
        const temp = {
            Id: ++this.editAppCounter,
            name: '',
            displayName: '',
            isvisible: true,
            applicationId: this.applicationEditInfo.id
        };

        this.applicationEditInfo.newSections.push(temp);
        this.setState({ newEditSections: this.applicationEditInfo.newSections });
    }

    editNewAppVisibleToggle = (event) => {
        const id = event.target['data-id'];
        const section = this.applicationEditInfo.newSections.find(section => section.Id == id);
        section.IsVisible = event.target.checked;
        this.setState({ newEditSections: this.applicationEditInfo.newSections });
    }

    editNewSectionNameChange = (event) => {
        const target = event.target,
            value = target.value,
            id = parseInt(target.getAttribute("data-id")),
            name = target.getAttribute("data-name"),
            temp = this.applicationEditInfo.newSections;

        for (let i = 0; i < temp.length; i++) {
            if (temp[i].Id === id) {
                temp[i][name] = value;
                break;
            }
        }

        this.setState({ newEditSections: this.applicationEditInfo.newSections });
    }

    editAppSubmit = (event) => {
        if (!this.validateEditApp()) {
            return;
        }

        this.setState({
            loading: true
        });

        if (this.applicationEditInfo.newSections && this.applicationEditInfo.newSections.length) {
            this.applicationEditInfo.newSections.forEach(section => { section.Id = 0 });
        }

        if (this.fileListWithLanguage && this.fileListWithLanguage.length > 0) {
            const fileListWithLanguage = this.fileListWithLanguage;

            fileListWithLanguage.forEach(fileDetails => {
                let reader = new FileReader();
                let self = this;
                reader.onload = function (e) {
                    try {
                        let languageName = fileDetails.languageName.toLowerCase();
                        var jsonList = JSON.parse(e.target.result);

                        if (languageName == "english") {
                            self.importedFileContent[languageName] = splitJsonFile(jsonList);
                        }
                        else {
                            self.importedFileContent[languageName] = splitJsonFile(jsonList);
                        }
                        if (Object.keys(self.importedFileContent).length === fileListWithLanguage.length) {
                            self.editCurrentApplication(self);
                        }
                    } catch (ex) {
                        console.log('error when trying to parse json = ' + ex, e, fileDetails);

                        Modal.error({
                            title: 'Invalid Json',
                            content: (<div>
                                <p>{fileDetails.fileName} is invalid.</p>
                            </div>)
                        });

                        self.resetDrawerState([RESET_COMMANDS.RESET_LOADING]);
                    }
                }
                reader.readAsText(fileDetails.file);

            });
        } else {
            this.editCurrentApplication(this.applicationEditInfo);
        }
    }

    editCurrentApplication = (self) => {
        let applicationEditInfo;
        let applicationToEdit;
        let resetDrawerState;

        if (self.importedFileContent) {
            const sectionsToUpload = [];
            Object.entries(self.importedFileContent).forEach(([language, sections]) => {
                const languageId = self.state.allLanguages.find(lang => lang.name.toLowerCase() === language.toLowerCase()).id;

                Object.entries(sections).forEach(([name, texts]) => {
                    let section = sectionsToUpload.find(s => s.name === name);

                    if (!section) {
                        section = {
                            id: 0,
                            name: name.trim(),
                            isVisible: true,
                            texts: []
                        };

                        sectionsToUpload.push(section);
                    }

                    Object.entries(texts).forEach(([key, value]) => {
                        let text = section.texts.find(t => t.key === key);

                        if (!text) {
                            text = {
                                sectionId: 0,
                                key: key.trim(),
                                textTranslations: [{
                                    translationId: 0,
                                    textId: 0,
                                    languageId: languageId,
                                    translation: value.trim(),
                                    statusId: 3
                                }]
                            };

                            section.texts.push(text);
                        } else {
                            text.textTranslations.push({
                                translationId: 0,
                                textId: 0,
                                languageId: languageId,
                                translation: value.trim(),
                                statusId: 3
                            });
                        }
                    });
                });
            });
            self.applicationEditInfo.sectionsToUpload = sectionsToUpload;
            applicationEditInfo = self.applicationEditInfo;
            applicationToEdit = self.applicationToEdit;
            resetDrawerState = self.resetDrawerState;
        } else {
            applicationEditInfo = self;
            applicationToEdit = this.applicationToEdit;
            resetDrawerState = this.resetDrawerState;
        }

        if (!validateSectionCount(
            applicationEditInfo.newSections,
            applicationToEdit.Section,
            applicationEditInfo.sectionsToDelete,
            applicationEditInfo.sectionsToUpload)) {

            Modal.warning({
                title: 'Warning',
                content: (
                    <div>
                        <p>Application should have at least one section</p>
                    </div>
                )
            });

            resetDrawerState([RESET_COMMANDS.RESET_LOADING]);
            return;
        }

        applicationEditInfo.removeEnglishForOtherLang = this.state.removeEnglishForOtherLang;

        axios.post(`${Strings.URL.GcLocalizerServiceBaseUrl}Admin/EditApplication`, applicationEditInfo, { headers: this.headers })
            .then(res => {
                this.resetDrawerState([RESET_COMMANDS.RESET_LOADING, RESET_COMMANDS.RESET_EDIT_DRAWER_VISIBILITY]);

                Modal.success({
                    title: 'Success',
                    content: (
                        <div>
                            <p>Application edited successfully.</p>
                        </div>
                    ),
                    onOk() { window.location.reload(); },
                });
            })
            .catch(reason => {
                this.resetDrawerState([RESET_COMMANDS.RESET_LOADING]);
                this.errorHandler(reason);
            })
    }

    resetDrawerState = (resetCommands) => {
        const partialState = {};

        if (resetCommands.indexOf(RESET_COMMANDS.RESET_LOADING) >= 0) {
            partialState.loading = false;
        }

        if (resetCommands.indexOf(RESET_COMMANDS.RESET_NEW_EDIT_SECTIONS) >= 0) {
            partialState.newEditSections = [];
        }

        if (resetCommands.indexOf(RESET_COMMANDS.RESET_NEW_SECTIONS) >= 0) {
            partialState.newSections = [];
        }

        if (resetCommands.indexOf(RESET_COMMANDS.RESET_TRANSLATION_TYPE) >= 0) {
            partialState.selectedTranslationType = null;
        }

        if (resetCommands.indexOf(RESET_COMMANDS.RESET_EDIT_DRAWER_VISIBILITY) >= 0) {
            partialState.isEditDrawerVisible = false;
        }

        this.setState(partialState);
    }

    setDrawerState = (setCommands) => {
        const partialState = {};

        if (setCommands.indexOf(SET_COMMANDS.SET_LOADING) >= 0) {
            partialState.loading = true;
        }

        this.setState(partialState);
    }

    editSection = (event) => {
        const id = event.target.getAttribute('data-id');
        const name = event.target.getAttribute('data-name');
        const sectionToEdit = this.applicationToEdit.Section.find(section => section.Id == id);

        if (this.applicationEditInfo
            && this.applicationEditInfo.sectionsToEdit
            && sectionToEdit[name] !== event.target.value) {
            const index = this.applicationEditInfo.sectionsToEdit.findIndex(section => section.Id == id);

            if (index >= 0) {

                if (sectionToEdit[name].trim() === event.target.value.trim()) {
                    this.applicationEditInfo.sectionsToEdit.splice(index, 1);
                } else {
                    this.applicationEditInfo.sectionsToEdit[index][name] = event.target.value;
                }
            } else {
                this.applicationEditInfo.sectionsToEdit.push({
                    ...sectionToEdit,
                    [name]: event.target.value
                });
            }
        }
    }

    updateImportFileList = (fileListWithLanguage) => {
        this.fileListWithLanguage = fileListWithLanguage;
    }

    renderEditApplicationDrawer = () => {
        if (this.state.isEditDrawerVisible) {
            return (<Drawer
                title="Edit Application"
                width={420}
                placement="right"
                onClose={this.hideEditApplication}
                maskClosable={false}
                visible={this.state.isEditDrawerVisible}
                style={{
                    height: 'calc(100% - 55px)',
                    overflow: 'auto',
                    paddingBottom: 53,
                }}
                destroyOnClose={true}
            >
                <Form layout="vertical" hideRequiredMark>
                    <Row gutter={6}>
                        <Row gutter={6} className="field">
                            <Row span={24} className='field'>
                                <Col span={16}>
                                    <Input size="default" placeholder="App name" data-name="newApp" onChange={this.handleAppNameChange} defaultValue={this.applicationToEdit.Name.trim()} />
                                </Col>
                            </Row>
                            <Row span={24} className='field'>
                                <Col span={24}>
                                    <Checkbox
                                        checked={this.state.removeEnglishForOtherLang}
                                        onChange={this.onRemoveEnglishCheckboxChange}>
                                        Remove English Translation For Non-English Languages
                                </Checkbox>
                                </Col>
                            </Row>
                            {
                                this.applicationToEdit && this.applicationToEdit.TranslationTypeId === 1 ?
                                    <Row>
                                        <h3>Import Json Files</h3>
                                        <UploadForBulkImport mode={ImportModes.Edit} updateImportFileList={this.updateImportFileList} newAppName={this.applicationToEdit.Name} allLanguages={this.state.allLanguages} />
                                    </Row>
                                    : ''
                            }
                        </Row>
                        <Row className="field">
                            <Col span={4}><Icon title="Check Sections to delete" className="size-font-16 margin-top-5" type="delete"></Icon></Col>
                            <Col span={10}><h3>Name</h3></Col>
                            <Col span={10}><h3>Display Name</h3></Col>
                        </Row>
                        <Row className="field">
                            {
                                this.applicationToEdit.Section.map(section => {
                                    return (
                                        <Row className="app-section" key={section.Id}>
                                            <Col span={4}>
                                                <Checkbox defaultChecked={false} data-id={section.Id} onChange={this.markSectionsToDelete}></Checkbox>
                                            </Col>
                                            <Col span={10}>
                                                <Input
                                                    placeholder="Name"
                                                    type="text"
                                                    defaultValue={section.Name.trim()}
                                                    data-id={section.Id}
                                                    data-name="Name"
                                                    onChange={this.editSection}></Input>
                                            </Col>
                                            <Col span={10}>
                                                <Input
                                                    placeholder="Display Name"
                                                    type="text"
                                                    defaultValue={section.DisplayName.trim()}
                                                    data-id={section.Id}
                                                    data-name="DisplayName"
                                                    onChange={this.editSection}></Input>
                                            </Col>
                                        </Row>
                                    );
                                })
                            }
                        </Row>
                        <Row span={24} className='field'>
                            <Col span={16}>
                                <h3>Add Sections</h3>
                            </Col>
                            <Col span={8}>
                                <Icon className="add-section" type="plus-circle" onClick={this.editAddSection} theme="twoTone" />
                            </Col>
                        </Row>
                        <Row className="field">
                            {
                                this.state.newEditSections.map(section => {
                                    return (
                                        <Row className="app-section" key={section.Id}>
                                            <Col span={4}>
                                                <Button onClick={e => this.onNewEditSectionsDeleteClick(e, section.Id)} >
                                                    <Icon type="delete" theme="outlined" />
                                                </Button>
                                            </Col>
                                            <Col span={10}>
                                                <Input maxLength="45" onChange={this.editNewSectionNameChange} data-id={section.Id} data-name="name" value={section.name} placeholder="Name" />
                                            </Col>
                                            <Col span={10}>
                                                <Input maxLength="45" onChange={this.editNewSectionNameChange} data-id={section.Id} data-name="displayName" value={section.displayName} placeholder="Display Name" />
                                            </Col>
                                        </Row >
                                    );
                                })
                            }
                        </Row>
                    </Row>
                </Form>
                <div className="drawer-gutter">
                    <Button className="right-8" onClick={this.hideEditApplication}>Cancel</Button>
                    <Button type="primary" htmlType="submit" onClick={this.editAppSubmit} loading={this.state.loading}>Save</Button>
                </div>
            </Drawer>);
        }

        return <React.Fragment></React.Fragment>;
    }

    onAllowPublishChange = (e, applicationId) => {
        Modal.confirm({
            title: Strings.Texts.AllowPublishConfirmation,
            onOk: () => this.changeAllowPublishChange(e, applicationId),
            okText: Strings.Texts.Yes,
            cancelText: Strings.Texts.No
        });
    }

    changeAllowPublishChange = (e, applicationId) => {
        const isChecked = e.target.checked;
        const appsOrig = JSON.parse(JSON.stringify(this.state.applications));
        const apps = this.updateAllowPublish(isChecked, applicationId, appsOrig);
        this.setDrawerState([SET_COMMANDS.SET_LOADING]);

        axios.post(Strings.URL.GcLocalizerServiceBaseUrl + `Admin/${applicationId}/ChangeAllowPublish?allowPublish=${isChecked}`, {}, { headers: this.headers })
            .then(res => {
                this.setState({ applications: apps }, () => this.setDrawerState([RESET_COMMANDS.RESET_LOADING]));
                this.getAppsAndSections();
            }).catch(error => {
                const apps = this.updateAllowPublish(!isChecked, applicationId, appsOrig);
                this.setState({ applications: apps }, () => this.setDrawerState([RESET_COMMANDS.RESET_LOADING]));
                this.errorHandler(error);
            });
    }

    updateAllowPublish = (checked, applicationId, appsOrig) => {
        const apps = appsOrig.map((a) => {
            if (a.id === applicationId) {
                a.AllowPublish = checked;
            }
            return a;
        });
        return apps;
    }

    handleMangeGroup = (event) =>{
        const applicationId = event.currentTarget.getAttribute('data-id');
        this.props.history.push(`/Groups/${applicationId}`);
    }

    renderApplications = () => {
        return this.state.applications.map((application) => {
            const checkboxDisabled = application.TranslationTypeId !== TranslationType.Text;
            const checked = application.AllowPublish && !checkboxDisabled;
            const text = application.IsVisible ? 'Yes' : 'No';
            const ungroupedKeys = this.appUngroupedKeys ? this.appUngroupedKeys.find(k => k.appId === application.Id).ungroupedKeys : '';

            return (
                <Row className="row">
                    <Col span={6}>
                        <div className="cell">
                            {application.Name}
                        </div>
                    </Col>
                    <Col span={3}>
                        <div className="cell">
                            <Icon type="delete" theme="outlined" onClick={this.deleteApp} data-id={application.Id} className="icons" />
                        </div>
                    </Col>
                    <Col span={3}>
                        <div className="cell">
                            <Icon type="edit" theme="outlined" onClick={this.editApplication} data-id={application.Id} className="icons" />
                        </div>
                    </Col>
                    <Col span={4}>
                        <div className="cell">
                            <Checkbox checked={checked} onChange={(e) => this.onAllowPublishChange(e, application.Id)} disabled={checkboxDisabled} />
                        </div>
                    </Col>
                    <Col span={4}>
                        <div className="cell">
                            <Icon type="edit" theme="outlined" onClick={this.handleMangeGroup} data-id={application.Id} className="icons" />
                        </div>
                    </Col>
                    <Col span={4}>
                        <div className="cell">
                            {ungroupedKeys}
                        </div>
                    </Col>
                </Row>
            );
        });
    }

    fileChangedHandler = (event) => {
        this.importedFileContent = null;
        const file = event.target.files[0];
        var reader = new FileReader();
        reader.onload = (function (theFile) {
            return function (e) {
                try {
                    this.importedFileContent = JSON.parse(e.target.result);
                } catch (ex) {
                    console.log('error when trying to parse json = ' + ex);
                }
            }
        })(file);
        reader.readAsText(file);
    }

    showImportDrawer = () => {
        return (<Drawer
            title="Import App From Json"
            width={400}
            placement="right"
            onClose={this.hideImportAppOption}
            maskClosable={false}
            visible={this.state.showImportOption}
            style={{
                height: 'calc(100% - 55px)',
                overflow: 'auto',
                paddingBottom: 53,
            }}
            destroyOnClose={true}
        >
            <Form layout="vertical" hideRequiredMark>
                <Row gutter={6}>
                    <Row span={24} className='field'>
                        <Col span={16}>
                            <Input size="default" placeholder="App name" data-name="newApp" onChange={this.handleChange} />
                        </Col>
                    </Row>
                    <Row span={24} className="feild">
                        <Col span={16} style={{ marginBottom: "20px" }}>
                            <Select
                                placeholder="Release"
                                onChange={this.selectedRelease}
                            >
                                {this.renderReleaseVersion()}
                            </Select>
                        </Col>
                    </Row>

                    <UploadForBulkImport mode={ImportModes.New} newAppName={this.state.newApp} allLanguages={this.state.allLanguages} releaseId={this.state.newAppRelease} />
                </Row>
            </Form>
        </Drawer>)
    }

    openImportModal = () => {
        this.setState({
            uploadModalVisible: true,
        });
    }

    handleImportModalOk = (fileList, selectedLanguage) => {
        this.setState({
            uploadModalVisible: false,
        });
        let importedFileList = this.state.importedFileList;
        importedFileList.push({
            file: fileList[0],
            selectedLanguageId: selectedLanguage.id,
            selectedLanguageName: selectedLanguage.name
        })
        this.setState({ importedFileList })
    }

    handleImportModalCancel = () => {
        this.setState({
            uploadModalVisible: false,
        });
    }

    handleLogoClick = () => {
        this.props.history.push('/Translations')
    }

    handleTranslationTypeChange = (e) => {
        this.setState({
            selectedTranslationType: e.target.value
        });
    }

    onRemoveEnglishCheckboxChange = (e) => {
        this.setState({ removeEnglishForOtherLang: e.target.checked });
    }

    render() {

        return (
            <div className="height-100">
                <Header history={this.props.history} pageRoute={PageRoutes.Developer} />
                <div className="container app-pad-50">
                    <h2 style={{ marginLeft: '150px', marginTop: '50px' }}>Apps & Sections</h2>
                    {this.state.loading ? <div className="pdf-page-loader"><Spin size="large"></Spin></div> : ''}
                    <Row>
                        <Col span={16} offset={2}>
                            <Row className="table-header">
                                <Col span={6} className="cell">
                                    Name
                                </Col>
                                <Col span={3} className="cell">
                                    Delete
                                </Col>
                                <Col span={3} className="cell">
                                    Edit
                                </Col>
                                <Col span={4} className="cell">
                                    Allow Publish
                                </Col>
                                <Col span={4} className="cell">
                                    Manage Group
                                </Col>
                                <Col span={4} className="cell">
                                    Ungrouped Strings
                                </Col>
                            </Row>
                            {this.renderApplications()}
                        </Col>
                        <Col span={6}>
                            <div className="buttons-block text-center">
                                <Row className="buttons">
                                    <Button className="links" onClick={this.showAddAppOption} type="primary" icon="plus" size="large">
                                        Add
                                    </Button>
                                </Row>
                                <Row>
                                    <Button className="links" onClick={this.showImportAppOption} type="primary" size="large">
                                        Import New App
                                    </Button>
                                </Row>
                            </div>
                        </Col>
                    </Row>
                </div>
                {this.showImportDrawer()}
                <Drawer
                    title="Add New App"
                    width={420}
                    placement="right"
                    onClose={this.hideAddAppOption}
                    maskClosable={false}
                    visible={this.state.showAddOption}
                    style={{
                        height: 'calc(100% - 55px)',
                        overflow: 'auto',
                        paddingBottom: 53,
                    }}
                    destroyOnClose={true}
                >
                    <Form layout="vertical" hideRequiredMark>
                        <Row gutter={6}>
                            <Row span={24} className='field'>
                                <Col span={16}>
                                    <Input size="default" placeholder="App name" data-name="newApp" onChange={this.handleChange} />
                                </Col>
                            </Row>
                            <Row gutter={6}>
                                <Row span={24} className='field'>
                                    <Col span={8} >
                                        <RadioGroup placeholder="Select Translation Type" style={{ 'width': '235px' }} onChange={(e) => this.handleTranslationTypeChange(e)}>
                                            <Radio value="1" key={1}>Text</Radio>
                                            <Radio value="2" key={2}>File</Radio>
                                        </RadioGroup>
                                    </Col>
                                </Row>
                            </Row>
                            <Row span={24} className='field'>
                                <Col span={16}>
                                    <h3>Sections</h3>
                                </Col>
                                <Col span={8}>
                                    <Icon className="add-section" type="plus-circle" onClick={this.addSections} theme="twoTone" />
                                </Col>
                            </Row>
                            <Row className="field">
                                {
                                    this.state.newSections.map(section => {
                                        return (
                                            <Row className="app-section">
                                                <Col span={4}>
                                                    <Button onClick={(e) => this.deleteSectionClicked(e, section.Id)} >
                                                        <Icon type="delete" theme="outlined" />
                                                    </Button>
                                                </Col>
                                                <Col span={10}>
                                                    <Input maxLength="45" onChange={this.handleSectionChange} data-id={section.Id} data-name="name" value={section.name} placeholder="Name" />
                                                </Col>
                                                <Col span={10}>
                                                    <Input maxLength="45" onChange={this.handleSectionChange} data-id={section.Id} data-name="displayName" value={section.displayName} placeholder="Display Name" />
                                                </Col>
                                            </Row>
                                        )
                                    })
                                }
                            </Row>
                        </Row>
                    </Form>
                    <div className="drawer-gutter">
                        <Button className="right-8" onClick={this.hideAddAppOption}>Cancel</Button>
                        <Button onClick={this.addApp} type="primary">Add</Button>
                    </div>
                </Drawer>
                {this.renderEditApplicationDrawer()}
                <Notifications />
            </div >
        )
    }
}

export default Developer;
