import React, { Component } from "react";
import { Row, Col, Input, Button, Form } from "antd";
import axios from "axios";
import Notifications, { notify } from "react-notify-toast";
import Strings from "../constants/String";
import { ForgotPasswordStatus } from "../constants/Constants";

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: false };
  }

  showToast = (type, message) => {
    let color = "";

    if (type == Strings.Texts.Danger) {
      color = { background: "#ee3647", text: "#FFFFFF" };
      notify.show(message, "custom", 2500, color);
    } else if (type == Strings.Texts.Info) {
      color = { background: "#0E1717", text: "#FFFFFF" };
      notify.show(message, "custom", 2000, color);
    } else if (type == Strings.Texts.Success) {
      color = { background: "#008000", text: "#FFFFFF" };
      notify.show(message, "custom", 5000, color);
    }
  };

  forgotPassword = (e) => {
    let that = this;

    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({ loading: true });
        axios
          .get(
            Strings.URL.GcLocalizerServiceBaseUrl +
              "Login/ForgotPassword?username=" +
              values.username
          )
          .then((res) => {
            if (res.data) {
              if (res.data.statusCode === ForgotPasswordStatus.EmailNotExist) {
                that.showToast(
                  Strings.Texts.Danger,
                  "The email id which you have entered doesn't exist in the system"
                );
              } else if (
                res.data.statusCode === ForgotPasswordStatus.DisabledAccount
              ) {
                that.showToast(
                  Strings.Texts.Danger,
                  Strings.Texts.DisabledAccount
                );
              } else if (res.data.statusCode === ForgotPasswordStatus.Success) {
                that.props.form.resetFields();
                this.showToast(
                  Strings.Texts.Success,
                  Strings.Texts.PasswordResetMessage
                );
              } else {
                that.showToast(Strings.Texts.Danger, "Some error occured");
              }
              this.setState({ loading: false });
            }
          })
          .catch((error) => {
            this.showToast(Strings.Texts.Danger, "It seems some error occured");
            console.log(error);
            this.setState({ loading: false });
          });
      }
    });
  };

  handleKeyPress = (event) => {
    if (event.key === "Enter") {
      this.forgotPassword();
    }
  };
  handleCancel() {
    this.props.history.push("/");
  }

  render() {
    const FormItem = Form.Item;
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="login-screen">
        <Form className="login-form">
          <Row className="login">
            <div className="login-box">
              <div className="header">Localizer</div>
              <h2 className="forgot-heading">Forgot Password</h2>
              <FormItem className={"forgot-username"}>
                {getFieldDecorator("username", {
                  initialValue: "",
                  rules: [
                    { required: true, message: "Required!" },
                    {
                      type: "email",
                      message: "Invalid Email!",
                    },
                  ],
                })(
                  <Input
                    placeholder="Enter email"
                    name="username"
                    onKeyPress={this.handleKeyPress}
                  />
                )}
              </FormItem>
              <FormItem>
                <Button
                  loading={this.state.loading}
                  type="primary"
                  onClick={this.forgotPassword}
                >
                  Reset Password
                </Button>
                &nbsp;
                <Button onClick={this.handleCancel.bind(this)}>Cancel</Button>
              </FormItem>
            </div>
          </Row>
        </Form>
        <Notifications />
      </div>
    );
  }
}

export default Form.create()(ForgotPassword);
