import React, { Component } from "react";
import { Button, Icon, Menu, Dropdown, Spin } from "antd";
import Roles, { PageRoutes, PageRouteTexts } from "../constants/Constants";

class Header extends Component {
  constructor() {
    super();
  }
  user = localStorage.getItem("gsuser") ? JSON.parse(localStorage.getItem("gsuser")) : null;
  name = this.user && this.user.name ? this.user.name : this.user && this.user.username ? this.user.username.split("@")[0].split(".").join(" ") : "";

  handleMenuClick = (event) => {
    switch (event.key) {
      case PageRouteTexts.Logout:
        localStorage.removeItem("gsuser");
        this.props.history.push(`${PageRoutes.Login}`);
        break;
      case PageRouteTexts.Admin:
        this.props.history.push(`/${PageRoutes.Admin}`);
        break;
      case PageRouteTexts.Developer:
        this.props.history.push(`/${PageRoutes.Developer}`);
        break;
      case PageRouteTexts.Translations:
        this.props.history.push(`/${PageRoutes.Translations}`);
        break;
      case PageRouteTexts.ChangePassword:
        this.props.history.push(`/${PageRoutes.ChangePassword}`);
        break;
      case PageRouteTexts.Profile:
        this.props.history.push(`/${PageRoutes.Profile}`);
        break;
      case PageRouteTexts.Login:
        this.props.history.push(`${PageRoutes.Login}`);
        break;
      default:
        break;
    }
  };

  handleLogoClick = () => {
    this.props.history.push("/Translations");
  };

  render() {
    let menu;
    if (this.user && this.user.roles && this.user.roles.includes(Roles.Admin)) {
      const page = this.props.pageRoute;
      menu = (
        <Menu onClick={this.handleMenuClick}>
          {page != PageRoutes.Admin && (
            <Menu.Item key="admin">Manage Users</Menu.Item>
          )}
          {page != PageRoutes.Developer && (
            <Menu.Item key="developer">Applications</Menu.Item>
          )}
          {page != PageRoutes.Translations && (
            <Menu.Item key="translations">Translations</Menu.Item>
          )}
          {page != PageRoutes.ChangePassword && (
            <Menu.Item key="changePassword">Change Password</Menu.Item>
          )}
          <Menu.Item key="logout">Logout</Menu.Item>
        </Menu>
      );
    } else if (this.user && this.user.roles && this.user.roles.includes(Roles.Developer)) {
      const page = this.props.pageRoute;
      menu = (
        <Menu onClick={this.handleMenuClick}>
          {page != PageRoutes.Developer && (
            <Menu.Item key="developer">Applications</Menu.Item>
          )}
          {page != PageRoutes.Translations && (
            <Menu.Item key="translations">Translations</Menu.Item>
          )}
          {page != PageRoutes.ChangePassword && (
            <Menu.Item key="changePassword">Change Password</Menu.Item>
          )}
          <Menu.Item key="logout">Logout</Menu.Item>
        </Menu>
      );
    } else if (this.user && this.user.roles && this.user.roles.includes(Roles.Translator)) {
      const page = this.props.pageRoute;
      menu = (
        <Menu onClick={this.handleMenuClick}>
          {page != PageRoutes.Translations && (
            <Menu.Item key="translations">Translations</Menu.Item>
          )}
          {page != PageRoutes.Profile && (
            <Menu.Item key="profile">Profile</Menu.Item>
          )}
          <Menu.Item key="logout">Logout</Menu.Item>
        </Menu>
      );
    } else {
      menu = (
        <Menu onClick={this.handleMenuClick}>
          <Menu.Item key="login">Login</Menu.Item>
        </Menu>
      );
    }

    return (
      <div className="navbar">
        <div className="logo" onClick={this.handleLogoClick}>
          Localizer
        </div>
        {this.user ?
          <div className="logout-link">
            <Dropdown overlay={menu}>
              <Button>
                Hi, {this.name} <Icon type="down" />
              </Button>
            </Dropdown>
          </div> : ""
        }

      </div>
    );
  }
}
export default Header;
