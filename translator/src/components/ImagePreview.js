import React, { Component } from 'react';

class ImagePreview extends Component {
    imagePreviewUrl =  "";
    constructor() {
        super();
        this.setPreviewUrl();
    }
    
    setPreviewUrl() {
        let indexOfParam = window.location.href.indexOf("prurl=");
        let searchQuery = window.location.href.substr(indexOfParam);
        this.imagePreviewUrl = searchQuery.substr(searchQuery.indexOf("=")+1);
    }
    render() {
        return <img src={this.imagePreviewUrl}></img>
    }
}

export default ImagePreview;
