import React, { Component } from "react";
import { Row, Col, Input, Button } from "antd";
import axios from "axios";
import Notifications, { notify } from "react-notify-toast";
import Strings from "../constants/String";

class Login extends Component {
  headers = {};

  constructor(props) {
    super(props);
    this.state = { username: "", password: "", loading: false };
  }

  componentDidMount() {
    this.user = JSON.parse(localStorage.getItem("gsuser"));
    if (this.user != null) {
      //this.props.history.push('/Translations');
    }
  }

  loginUser = () => {
    if (
      this.state.username.trim().length != 0 &&
      this.state.password.trim().length != 0
    ) {
      this.setState({ loading: true });
      var obj = {
        Username: this.state.username,
        Password: this.state.password,
      };
      axios
        .post(Strings.URL.GcLocalizerServiceBaseUrl + "Login", obj)
        .then((res) => {
          if (res.data) {
            if (res.data.disabled) {
              const color = { background: "#ee3647", text: "#FFFFFF" };
              notify.show("Your account is disabled", "custom", 2500, color);
              this.setState({ loading: false });
            } else {
              localStorage.setItem("gsuser", JSON.stringify(res.data));
              let redirection =
                res.data && res.data.isPasswordVerified
                  ? "/Translations"
                  : "/ChangePassword";
              this.props.history.push(redirection);
              this.headers = {
                authorization: `Bearer ${
                  JSON.parse(localStorage.getItem("gsuser")).token
                }`,
              };
            }
          }
        })
        .catch((error) => {
          const color = { background: "#ee3647", text: "#FFFFFF" };
          notify.show(
            "It seems you have entered an incorrect password",
            "custom",
            2500,
            color
          );
          console.log(error);
          this.setState({ loading: false });
        });
    } else {
      const color = { background: "#ee3647", text: "#FFFFFF" };
      notify.show(
        "Please enter both username and password",
        "custom",
        2500,
        color
      );
    }
  };

  handleInputChange = (event) => {
    const target = event.target,
      value = target.value,
      name = target.name;

    this.setState({
      [name]: value,
    });
  };

  handleKeyPress = (event) => {
    if (event.key === "Enter") {
      this.loginUser();
    }
  };

  forgotPassword() {
    this.props.history.push("/ForgotPassword");
  }

  render() {
    let disabled = this.state.loading == true ? "disabled" : "";
    return (
      <div className="login-screen">
        <Row className="login">
          <Col span={24}>
            <div className="login-box">
              <div className="header">Localizer</div>
              <div className="login-field">
                <Input
                  placeholder="Username"
                  name="username"
                  value={this.state.username}
                  disabled={disabled}
                  onChange={this.handleInputChange}
                  onKeyPress={this.handleKeyPress}
                />
              </div>
              <div className="login-field">
                <Input
                  type="password"
                  name="password"
                  placeholder="Password"
                  value={this.state.password}
                  disabled={disabled}
                  onChange={this.handleInputChange}
                  onKeyPress={this.handleKeyPress}
                />
              </div>
              <Row type="flex" justify="space-between">
                <Col>
                  <Button
                    type="primary"
                    loading={this.state.loading}
                    onClick={this.loginUser}
                  >
                    Login
                  </Button>
                </Col>
                <Col>
                  <a href="#" onClick={this.forgotPassword.bind(this)}>
                    Forgot Password?
                  </a>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
        <Notifications />
      </div>
    );
  }
}

export default Login;
