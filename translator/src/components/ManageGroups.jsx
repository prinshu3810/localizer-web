import React, { useState, useEffect } from 'react'
import { Icon, Table, Modal, Row, Col, Button, Spin } from 'antd';
import Notifications, { notify } from 'react-notify-toast';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import Axios from '../util/axiosconfig';
import Strings from '../constants/String';
import GroupNameChangeModal from './Modals/GroupNameChangeModal';
import DeleteGroupModal from './Modals/DeleteGroupModal';
import AddGroupModal from '../components/Modals/AddGroupModal';

const { confirm } = Modal;

export default function ManageGroups(props) {

    const [groups, setGroups] = useState([]);
    const [selectedGroup, setSelectedGroup] = useState([]);
    const [nameChangeModalVisible, setNameChangeModalVisible] = useState(false);
    const [nameChangeModalLoading, setNameChangeModalLoading] = useState(false);
    const [deleteModalVisible, setDeleteModalVisible] = useState(false);
    const [deleteModalModalLoading, setDeleteModalModalLoading] = useState(false);
    const [moveAndDelete, setMoveAndDelete] = useState(false);
    const [saveButtonDisabled, setSaveButtonDisabled] = useState(true);
    const [addGroupModalVisible, setAddGroupModalVisible] = useState(false);
    const [addGroupModalLoading, setAddGroupModalLoading] = useState(false);
    const [showSpinner, setShowSpinner] = useState(true);

    const { applicationId } = props;

    // Get Group Details for the current Application 
    useEffect(() => {
        if (applicationId) {
            getGroupDetails();
        }
    }, [applicationId]);

    const getGroupDetails = () => {
        Axios.get(`Group/Groups/${applicationId}`)
            .then(res => {
                setGroups(res.data);
                setShowSpinner(false);
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.FetchDataError, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                setShowSpinner(false);
            });
    }

    const updateGroupOrder = () => {
        const GroupsOrder = groups.reduce((acc, group) => {
            acc.push({ GroupId: group.id, Sno: group.sno });
            return acc;
        }, []);

        const data = { ApplicationId: applicationId, GroupsOrder: GroupsOrder }

        Axios.put(`Group/ChangeGroupOrder`, data)
            .then(res => {

            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
            });
    }

    const handleDeleteGroup = (event) => {
        const groupId = parseInt(event.currentTarget.getAttribute('data-id'));
        const group = groups.length ? groups.filter(group => group.id === groupId) : [];
        setSelectedGroup(group)
        setDeleteModalVisible(true);
    }

    const handleDeleteGroupModalSave = (values) => {
        if (values.newGroupId != 0) {
            setMoveAndDelete(true);
        } else {
            setDeleteModalModalLoading(true);
        }
        deleteGroup(selectedGroup[0].id, values.newGroupId);
    }

    //call to delete the group
    const deleteGroup = (currentGroupId, newGroupId) => {
        Axios.delete(`Group/DeleteGroup`, { params: { currentGroupId, newGroupId } })
            .then(res => {
                if (res.data) {
                    getGroupDetails();
                    setDeleteModalModalLoading(false);
                    setDeleteModalVisible(false);
                    setMoveAndDelete(false);
                    notify.show("Group Deleted", "custom", 2500, { background: '#52c41a', text: "#FFFFFF" });
                } else if (!res.data) {
                    notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                    setDeleteModalModalLoading(false);
                    setDeleteModalVisible(false);
                    setMoveAndDelete(false);
                }
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                setDeleteModalModalLoading(false);
                setDeleteModalVisible(false);
                setMoveAndDelete(false);
            });
    }

    const showNameChangeModal = (id) => {
        const group = groups.length ? groups.filter(group => group.id === id) : [];
        setSelectedGroup(group);
        setNameChangeModalVisible(true);
    }

    const handleCancel = () => {
        setNameChangeModalVisible(false);
        setNameChangeModalLoading(false);
        setDeleteModalModalLoading(false);
        setDeleteModalVisible(false);
        setAddGroupModalVisible(false);
        setAddGroupModalLoading(false);
        setSaveButtonDisabled(true);
        setMoveAndDelete(false);
    }

    const handleNameChangeModalSave = (values) => {
        setNameChangeModalLoading(true);
        saveGroupName(values)
    }

    const saveGroupName = (values) => {
        const data = { id: selectedGroup[0].id, name: values.name };
        Axios.put('Group/ChangeGroupName', data)
            .then(res => {
                if (res.data) {
                    notify.show("Group Name Changed", "custom", 2500, { background: '#52c41a', text: "#FFFFFF" });
                    getGroupDetails();
                    setNameChangeModalLoading(false);
                    setNameChangeModalVisible(false);
                }
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                setNameChangeModalLoading(false);
                setNameChangeModalVisible(false);
            });
    }

    const reorderGroups = (groupItems, sourceIndex, destinationIndex) => {
        const copiedGroupItems = [...groupItems];
        const [removedItem] = copiedGroupItems.splice(sourceIndex, 1);
        copiedGroupItems.splice(destinationIndex, 0, removedItem);
        return copiedGroupItems.reduce((acc, group, index) => {
            group.sno = index + 1;
            return acc;
        }, copiedGroupItems);
    }

    const onDragEnd = (result) => {
        const { destination, source } = result;
        if (!destination) {
            return;
        }
        if (destination.draggableId === source.draggableId && destination.index === source.index) {
            return;
        }

        setGroups(reorderGroups(groups, source.index, destination.index));
        updateGroupOrder();
    }

    const handleAddGroupModalSave = (groupDetails) => {
        setAddGroupModalLoading(true);
        const groupData = { applicationId: applicationId, name: groupDetails.name, isVisible: 1, sno: getSno(), note: null }
        Axios.post(`Group/Group`, groupData)
            .then(res => {
                if (res.data) {
                    getGroupDetails();
                    setAddGroupModalVisible(false);
                    setAddGroupModalLoading(false);
                    notify.show("Group Added", "custom", 2500, { background: '#52c41a', text: "#FFFFFF" });
                }
            }).catch(error => {
                notify.show(Strings.AppMessage.ErrorMessage.OperationFailed, "custom", 2500, { background: '#ee3647', text: "#FFFFFF" });
                setAddGroupModalVisible(false);
                setAddGroupModalLoading(false);
            });
    }

    // To get the Sno for the group.
    const getSno = () => {
        return groups.length ? groups[groups.length - 1].sno + 1 : 0;
    }

    const renderGroups = (groupId, groupName) => {
        return (<Row className="row">
            <Col span={12}>
                <div className="cell">
                    <a href="#" onClick={() => showNameChangeModal(groupId)} style={{ cursor: 'pointer' }}>{groupName}</a>
                </div>
            </Col>
            <Col span={12}>
                <div className="cell">
                    <Icon type="delete" theme="outlined" onClick={handleDeleteGroup} data-id={groupId} className="icons" />
                </div>
            </Col>
        </Row>)
    }

    return (
        <div>
            {showSpinner ? <div className="pdf-page-loader"><Spin size="large"></Spin></div> : ''}
            <DragDropContext onDragEnd={onDragEnd}>
                <div className="mg-container__add">
                    <Button type="primary" ghost icon="plus" onClick={() => setAddGroupModalVisible(true)}  >Add</Button>
                </div>
                <div className="mg-container__grid">
                    <Row>
                        <Col span={24}>
                            <Row className="table-header">
                                <Col span={12} className="cell">
                                    Name
                                        </Col>
                                <Col span={12} className="cell">
                                    Delete
                                        </Col>
                            </Row>
                            <div>
                                <Droppable droppableId="groups">
                                    {(provided, snapshot) => (
                                        <div>
                                            <div
                                                ref={provided.innerRef}
                                                style={{ backgroundColor: snapshot.isDraggingOver ? 'lightblue' : 'lightgrey' }}
                                                {...provided.droppableProps}
                                            >
                                                {groups.length && !showSpinner ?
                                                    groups.map((group, index) => {
                                                        return (
                                                            <Draggable key={group.id} draggableId={group.id.toString()} index={index}>
                                                                {(provided, snapshot) => (
                                                                    <div
                                                                        ref={provided.innerRef}
                                                                        {...provided.draggableProps}
                                                                        {...provided.dragHandleProps}
                                                                        style={{
                                                                            backgroundColor: snapshot.isDraggingOver ? 'blue' : '#fff',
                                                                            ...provided.draggableProps.style
                                                                        }}
                                                                    >
                                                                        {renderGroups(group.id, group.name)}
                                                                    </div>
                                                                )}
                                                            </Draggable>
                                                        )
                                                    })
                                                    :
                                                    <Row className="row" style={{ backgroundColor: "#fff", textAlign: "center" }}>
                                                        <Col span={24}>
                                                            <div className="cell">
                                                                {Strings.Texts.NoData}
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                }
                                            </div>
                                            {provided.placeholder}
                                        </ div>
                                    )}
                                </Droppable>
                            </div>
                        </Col>
                    </Row>
                </div>
            </DragDropContext>
            <GroupNameChangeModal
                selectedGroup={selectedGroup}
                loading={nameChangeModalLoading}
                visible={nameChangeModalVisible}
                saveButtonDisabled={saveButtonDisabled}
                handleCancel={handleCancel}
                handleNameChangeModalSave={handleNameChangeModalSave}
                setSaveButtonDisabled={setSaveButtonDisabled}
            />
            <DeleteGroupModal
                groups={groups}
                selectedGroup={selectedGroup}
                loading={deleteModalModalLoading}
                moveAndDelete={moveAndDelete}
                visible={deleteModalVisible}
                saveButtonDisabled={saveButtonDisabled}
                handleCancel={handleCancel}
                handleDeleteGroupModalSave={handleDeleteGroupModalSave}
                setSaveButtonDisabled={setSaveButtonDisabled}
            />
            <AddGroupModal
                loading={addGroupModalLoading}
                visible={addGroupModalVisible}
                saveButtonDisabled={saveButtonDisabled}
                handleCancel={handleCancel}
                handleAddGroupModalSave={handleAddGroupModalSave}
                setSaveButtonDisabled={setSaveButtonDisabled}
            />
            <Notifications />
        </div>
    )
}
