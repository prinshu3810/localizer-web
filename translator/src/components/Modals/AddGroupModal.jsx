import React, { useState } from 'react';
import { Button, Modal, Form, Input, Select } from 'antd';
import Strings from '../../constants/String';

function AddGroupModal(props) {

    const {
        loading,
        visible,
        saveButtonDisabled,
        handleCancel,
        handleAddGroupModalSave,
        setSaveButtonDisabled
    } = props;

    const [groupName, setGroupName] = useState('');

    const { getFieldDecorator } = props.form;

    const formItemLayout = {
        labelCol: {
            span: 7,
            offset: 0
        },
        wrapperCol: {
            span: 16,
            offset: 0
        }
    }

    const validateGroupName = (rule, value, callback) => {
        if (!value.length) {
            setSaveButtonDisabled(true);
            callback();
        } else if (value.length > 50) {
            callback("Use less than 50 characters.");
            setSaveButtonDisabled(true);
        } else {
            setSaveButtonDisabled(false);
            callback();
        }
    }

    const validateFields = (e) => {
        e.preventDefault();
        props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                handleAddGroupModalSave(values);
            } else if (err) {
                setSaveButtonDisabled(true);
            }
        });
    }

    return (
        <Modal
            visible={visible}
            title="Add Group"
            onCancel={handleCancel}
            footer={[
                <Button key="back" onClick={handleCancel}>
                    Cancel
                </Button>,
                <Button key="submit" type="primary" loading={loading}
                    onClick={(e) => validateFields(e)} disabled={saveButtonDisabled}
                >
                    Save
                </Button>,
            ]}
            destroyOnClose={true}
        >
            <Form
                {...formItemLayout}
                layout="horizontal"
                name="form_in_modal"
                className="addgroupform"
            >
                <Form.Item name="name" label="Name">
                    {getFieldDecorator('name', {
                        initialValue: '',
                        setFieldsValue: { groupName },
                        rules: [
                            {
                                required: true,
                                message: Strings.AppMessage.ErrorMessage.Required
                            },
                            { validator: validateGroupName, }
                        ],
                    })(
                        <Input type='text' placeholder="Enter Group Name" onChange={() => setGroupName(groupName)} />
                    )}
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default Form.create()(AddGroupModal);
