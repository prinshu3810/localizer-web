import React from 'react';
import { Button, Modal, Form, Input, Select } from 'antd';
import Strings from '../../constants/String';
import { useEffect } from 'react';

const { Option } = Select;

function DeleteGroupModal(props) {

    const {
        selectedGroup,
        loading,
        visible,
        saveButtonDisabled,
        handleCancel,
        handleDeleteGroupModalSave,
        moveAndDelete,
        setSaveButtonDisabled
    } = props;

    let { groups } = props;

    const { getFieldDecorator } = props.form;

    useEffect(() => {
        groups = groups.filter(group => group.id != selectedGroup.id);
    }, []);

    const formItemLayout = {
        labelCol: {
            span: 7,
            offset: 0
        },
        wrapperCol: {
            span: 16,
            offset: 0
        }
    }

    const validateGroupField = (rule, value, callback) => {
        setSaveButtonDisabled(false);
        callback();
    }

    const validateFields = (e) => {
        e.preventDefault();
        props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                handleDeleteGroupModalSave({ newGroupId: values.groups });
            } else if (err) {
                setSaveButtonDisabled(true);
            }
        });
    }

    // Sets the newGroupId to 0 since the user does not want to move current Group's text  and images to a new Group.
    const skip = () =>{
        handleDeleteGroupModalSave({ newGroupId: 0});
    }

    const getCurrentGroupName = () => {
        return Array.isArray(selectedGroup) && selectedGroup.length ? selectedGroup[0].name : ''
    }

    // Returns all the groups excluding the selected group to be rendered in select options.
    const getGroups = () => {
        const selectedgroupId = Array.isArray(selectedGroup) && selectedGroup.length ? selectedGroup[0].id : 0;
        return Array.isArray(groups) && groups.length ? groups.filter(x => x.id !== selectedgroupId) : [];
    }

    return (
        <Modal
            title="Delete Group"
            visible={visible}
            onCancel={handleCancel}
            destroyOnClose={true}
            footer={[
                <Button key="back" onClick={handleCancel}>
                    Cancel
                </Button>,
                <Button type="danger" onClick={skip} loading={loading}>Delete</Button>,
                <Button key="submit" type="primary" loading={moveAndDelete}
                    onClick={(e) => validateFields(e)} disabled={saveButtonDisabled}
                >
                    Move and Delete
                </Button>,
            ]}
        >
            <p>Please Select a group to move the screenshots and Texts of {getCurrentGroupName()} to a new Group.</p>
            <Form
                {...formItemLayout}
                layout="horizontal"
                name="form_in_modal"
                className="mg-manage__deletegroupform"
            >
                <Form.Item name="groups" label="Select Group">
                    {getFieldDecorator('groups', {
                        initialValue: [],
                        rules: [
                            { validator: validateGroupField, }
                        ],
                    })(
                        <Select
                            style={{ width: '100%' }}
                            placeholder="Select Group"
                        >
                            {getGroups().map(group => <Option key={group.name} value={group.id}>{group.name}</Option>)}
                        </Select>
                    )}
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default Form.create()(DeleteGroupModal);