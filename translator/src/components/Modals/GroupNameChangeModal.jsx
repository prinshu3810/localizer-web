import React, { useState } from 'react';
import { Button, Modal, Form, Input, Select } from 'antd';
import Strings from '../../constants/String';

function GroupNameChangeModal(props) {

    const {
        selectedGroup,
        loading,
        visible,
        handleCancel,
        saveButtonDisabled,
        setSaveButtonDisabled,
        handleNameChangeModalSave
    } = props;

    const { getFieldDecorator } = props.form;

    const [groupName, setGroupName] = useState('');

    const formItemLayout = {
        labelCol: {
            span: 7,
            offset: 0
        },
        wrapperCol: {
            span: 16,
            offset: 0
        }
    }

    const validateGroupName = (rule, value, callback) => {
        if (!value.length) {
            setSaveButtonDisabled(true);
            callback();
        }else if(value.length > 50){
            callback("Use less than 50 characters.");
            setSaveButtonDisabled(true);
        }else{
            setSaveButtonDisabled(false);
            callback();
        }
    }

    const validateFields = (e) => {
        e.preventDefault();
        props.form.validateFields((err, values) => {
            if (!err) {
                handleNameChangeModalSave(values);
            }else if (err) {
                setSaveButtonDisabled(true);
            }
        });
    }

    return (
        <Modal
            title="Change Group Name"
            visible={visible}
            onCancel={handleCancel}
            destroyOnClose={true}
            footer={[
                <Button key="back" onClick={handleCancel}>
                    Cancel
                </Button>,
                <Button key="submit" type="primary" loading={loading}
                    onClick={(e) => validateFields(e)} disabled={saveButtonDisabled}
                >
                    Save
                </Button>,
            ]}
        >
            <Form
                {...formItemLayout}
                layout="horizontal"
                name="form_in_modal"
                className="groupnamechangeform"
            >
                <Form.Item name="name" label="Name" className="groupName__label">
                    {getFieldDecorator('name', {
                        initialValue: Array.isArray(selectedGroup) && selectedGroup.length ? selectedGroup[0].name : '',
                        setFieldsValue: { groupName },
                        rules: [
                            {
                                required: true,
                                message: Strings.AppMessage.ErrorMessage.Required
                            },
                            { validator: validateGroupName, }
                        ],
                    })(
                        <Input type='text' placeholder="Enter Group Name" onChange={() => setGroupName(groupName)} />
                    )}
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default Form.create()(GroupNameChangeModal);
