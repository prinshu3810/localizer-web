import React, { useState } from 'react';
import { Button, Modal, Form, Input, Select } from 'antd';
import Strings from '../../constants/String';

const { Option } = Select;

function InviteUserModal(props) {

    const [email, setEmail] = useState('');
    const [name, setName] = useState('');

    const {
        visible,
        loading,
        handleCancel,
        roles,
        languages,
        emailIds,
        handleInvite,
        inviteButtonDisabled,
        setInviteButtonDisabled
    } = props;

    const { getFieldDecorator } = props.form;

    const formItemLayout = {
        labelCol: {
            span: 7,
            offset: 0
        },
        wrapperCol: {
            span: 16,
            offset: 0
        }
    }

    // To check if a user account already exists entered in the email field.
    const validateEmailAccount = (rule, value, callback) => {
        const ids = emailIds.map(id => id.toLowerCase());
        if (!value.length) {
            setInviteButtonDisabled(true);
            callback();
        }
        else if (ids.includes(value.toLowerCase())) {
            setInviteButtonDisabled(true);
            callback(Strings.AppMessage.ErrorMessage.AccountExists);
        } else {
            setInviteButtonDisabled(false);
            callback();
        }
    }

    const validateLanguagesField = (rule, value, callback) => {
        if (!value.length) {
            setInviteButtonDisabled(true);
            callback();
        } else {
            setInviteButtonDisabled(false);
            callback();
        }
    }

    const validateRolesField = (rule, value, callback) => {
        if (!value.length) {
            setInviteButtonDisabled(true);
            callback();
        } else {
            setInviteButtonDisabled(false);
            callback();
        }
    }

    const validateFields = (e) => {
        e.preventDefault();
        props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                handleInvite(values);
            } else if (err) {
                setInviteButtonDisabled(true);
            }
        });
    }

    return (
        <Modal
            visible={visible}
            title="Invite"
            onCancel={handleCancel}
            footer={[
                <Button key="back" onClick={handleCancel}>
                    Cancel
                </Button>,
                <Button key="submit" type="primary" loading={loading}
                    onClick={(e) => validateFields(e)} disabled={inviteButtonDisabled}
                >
                    Invite
                </Button>,
            ]}
            destroyOnClose={true}
        >
            <Form
                {...formItemLayout}
                layout="horizontal"
                name="form_in_modal"
                className="inviteuserform"
            >
                <Form.Item name="name" label="Name" labelAlign='left'>
                    {getFieldDecorator('name', {
                        initialValue: '',
                        setFieldsValue: { name },
                    })(
                        <Input type='text' placeholder="Enter name" onChange={() => setName(name)} />
                    )}
                </Form.Item>
                <Form.Item name="email" label="Email" labelAlign='left'>
                    {getFieldDecorator('email', {
                        initialValue: '',
                        setFieldsValue: { email },
                        rules: [
                            {
                                type: 'email',
                                required: true,
                                message: Strings.AppMessage.ErrorMessage.Required
                            },
                            {
                                validator: validateEmailAccount,
                            },
                        ],
                    })(
                        <Input type='email' placeholder="Enter email" onChange={() => setEmail(email)} />
                    )}
                </Form.Item>
                <Form.Item name="languages" label="Select Languages" labelAlign='left'>
                    {getFieldDecorator('languages', {
                        initialValue: [],
                        rules: [
                            { required: true, message: Strings.AppMessage.ErrorMessage.Required },
                            { validator: validateLanguagesField, }
                        ],
                    })(
                        <Select
                            mode="multiple"
                            style={{ width: '100%' }}
                            placeholder="Please select"
                        >
                            {languages.map(language => <Option key={language.name} value={language.id}>{language.name}</Option>)}
                        </Select>
                    )}
                </Form.Item>
                <Form.Item name="roles" label="Select Roles" labelAlign='left'>
                    {getFieldDecorator('roles', {
                        initialValue: [],
                        rules: [
                            { required: true, message: Strings.AppMessage.ErrorMessage.Required },
                            { validator: validateRolesField, }
                        ],
                    })(
                        <Select
                            mode="multiple"
                            style={{ width: '100%' }}
                            placeholder="Please select"
                        >
                            {roles.map(role => <Option key={role.name} value={role.id}>{role.name}</Option>)}
                        </Select>
                    )}
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default Form.create()(InviteUserModal);