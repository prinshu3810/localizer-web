import React from 'react';
import { Button, Modal, Form, Input, Select } from 'antd';
import _ from 'lodash';
import Strings from '../../constants/String';
import Roles from '../../constants/Constants';

const { Option } = Select;

function MangeUserModal(props) {

    const {
        user,
        visible,
        loading,
        handleCancel,
        handleDisable,
        roles,
        languages,
        handleEnable,
        handleManageModalSave,
        saveButtonDisabled,
        setSaveButtonDisabled
    } = props;

    const adminId = localStorage.getItem("gsuser") ? JSON.parse(localStorage.getItem("gsuser")).id : null
    const isDeletedUser = Array.isArray(user) && user.length && user[0].disabled;
    const { getFieldDecorator } = props.form;

    const formItemLayout = {
        labelCol: {
            span: 7,
            offset: 0
        },
        wrapperCol: {
            span: 16,
            offset: 0
        }
    }

    const isAdmin = () => {
        return Array.isArray(user) && user.length && user[0].id === adminId
    }

    /* 
       To check if the roles field is empty and admin user does not remove admin role from his/her roles
    */
    const validateRolesField = (rule, value, callback) => {
        if (!value.length) {
            setSaveButtonDisabled(true);
            callback();
        } else if (Array.isArray(props.form.getFieldValue('languages')) && !props.form.getFieldValue('languages').length) {
            setSaveButtonDisabled(true);
            callback();
        }
        else if (isAdmin()) {
            if (!value.includes(Roles.Admin)) {
                setSaveButtonDisabled(true);
                callback(Strings.AppMessage.ErrorMessage.AdminRoleRequired);
            }
            else {
                setSaveButtonDisabled(false);
                callback();
            }
        } else {
            /* 
                Purpose: to enable the save button if the values are different from previous assigned roles to user.    
                First condition checks if the value selected by user is different from previous values in roles field.
                Second condition checks if the values in languages field are different from previous values in languages field.
                If only the first condition is checked then in a certain condition when all the roles are removed and different languages
                are selected but the same role is selected as assigned to that user before, the save button is still disabled.
            */
            if (!_.isEqual(value, user[0].roleIds) || !_.isEqual(props.form.getFieldValue('languages'), user[0].languageIds)) {
                setSaveButtonDisabled(false);
                callback();
            }
            else {
                callback();
            }
        }
    }

    const validateLanguagesField = (rule, value, callback) => {
        if (!value.length) {
            setSaveButtonDisabled(true);
            callback();
        }
        else if (Array.isArray(props.form.getFieldValue('roles')) && !props.form.getFieldValue('roles').length) {
            setSaveButtonDisabled(true);
            callback();
        } else if (!_.isEqual(value, user[0].languageIds) || !_.isEqual(props.form.getFieldValue('roles'), user[0].roleIds)) {
            /* 
               Purpose: to enable the save button if the values are different from previous assigned languages to user.    
               First condition checks if the value selected by user is different from previous values in languages field.
               Second condition checks if the values in roles field are different from previous values in roles field.
               If only the first condition is checked then in a certain condition when all the languages are removed and different roles
               are selected but the same language is selected as assigned to that user before, the save button is still disabled.
           */
            setSaveButtonDisabled(false);
            callback();
        }
        else {
            callback();
        }
    }

    const validateFields = (e) => {
        e.preventDefault();
        props.form.validateFields((err, values) => {
            if (!err) {
                handleManageModalSave(values);
            }
        });
    }

    return (
        <Modal
            visible={visible}
            title="Manage"
            onCancel={handleCancel}
            footer={[
                isDeletedUser ?
                    <Button key="enable" type="primary" onClick={handleEnable} style={{ float: "left" }}>
                        Enable
                    </Button> :
                    <Button key="disable" type="danger" ghost onClick={handleDisable} style={{ float: "left" }}
                        disabled={isAdmin()}
                    >
                        Disable
                    </Button>
                ,
                <Button key="back" onClick={handleCancel}>
                    Cancel
                </Button>,
                <Button key="submit" type="primary" loading={loading}
                    onClick={(e) => validateFields(e)} disabled={saveButtonDisabled}
                >
                    Save
                </Button>,
            ]}
            destroyOnClose={true}
        >
            <Form
                {...formItemLayout}
                layout="horizontal"
                name="form_in_modal"
                className="manageuserform"
            >
                <Form.Item name="email" label="Email" className="manageuserform__label">
                    <Input type='email' value={Array.isArray(user) && user.length ? user[0].email : ''} disabled />
                </Form.Item>
                <Form.Item name="languages" label="Select Languages" className="manageuserform__label">
                    {getFieldDecorator('languages', {
                        initialValue: Array.isArray(user) && user.length ? user[0].languageIds : [],
                        rules: [
                            { required: true, message: Strings.AppMessage.ErrorMessage.Required },
                            { validator: validateLanguagesField, }
                        ],
                    })(
                        <Select
                            mode="multiple"
                            style={{ width: '100%' }}
                            placeholder="Please select"
                        >
                            {languages.map(language => <Option key={language.name} value={language.id}>{language.name}</Option>)}
                        </Select>
                    )}
                </Form.Item>
                <Form.Item name="roles" label="Select Roles" className="manageuserform__label">
                    {getFieldDecorator('roles', {
                        initialValue: Array.isArray(user) && user.length ? user[0].roleIds : [],
                        rules: [
                            { required: true, message: Strings.AppMessage.ErrorMessage.Required },
                            { validator: validateRolesField, }
                        ],
                    })(
                        <Select
                            mode="multiple"
                            style={{ width: '100%' }}
                            placeholder="Please select"
                        >
                            {roles.map(role => <Option key={role.name} value={role.id}>{role.name}</Option>)}
                        </Select>
                    )}
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default Form.create()(MangeUserModal);