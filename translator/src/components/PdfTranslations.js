import React, { Component } from 'react';
import { Row, Col, Input, Button, Icon, Spin, Drawer, Form, Modal, Upload, Tooltip, Select } from 'antd';
import axios from 'axios';
import { notify } from 'react-notify-toast';
import Roles, { TranslationStatus, Languages, STATUS_CODES, AppStates } from '../constants/Constants';
import _ from 'lodash';
import Strings from '../constants/String';
import * as JSzip from 'jszip';
import * as JSZipUtils from 'jszip-utils';
import * as FileSaver from 'file-saver';

const RESET_COMMANDS = {
    RESET_LOADED: 'RESET_LOADED',
}

const SET_COMMANDS = {
    SET_LOADED: 'SET_LOADED',
}

const { Option } = Select;

export default class PdfTranslations extends Component {

    headers = {
        authorization: `Bearer ${JSON.parse(localStorage.getItem("gsuser")).token}`
    };
    userRole = 0;
    user = {};
    userLangList = [];
    allLanguages = [];
    newTranslationCounter = -1;
    userLanguages = []
    constructor() {
        super();
        this.state = {
            size: 'large',
            loaded: false,
            translations: [],
            addTranslateOptionVisible: false,
            newKey: '',
            isUnique: AppStates.None,
            sectionFileListMap: null, // contains fileList per sectionId
            imageUrl: '',
            applicationNotification: [],
            sectionNotification: [],
            languageNotification: [],
            showTranslationModal: false,
            englishFileList: [],
            translatedFileList: [],
            selectedFile: null,
            newPdfRelease: 0,
            isModalVisible: false,
            editedReleaseId: 0

        };
        this.onTranslationModalCancel = this.onTranslationModalCancel.bind(this);
        this.handleSaveClick = this.handleSaveClick.bind(this);
    }

    componentDidMount() {
        if (this.props.selectedLanguage && (this.props.selectedSection || this.props.selectedGroup) && this.props.selectedApplicationId && this.props.userInfo) {
            this.getPdfTranslations();
        }
    }
    componentWillReceiveProps(props) {

        if (props.userInfo !== this.props.userInfo
            || props.selectedLanguage !== this.props.selectedLanguage
            || props.selectedApplicationId !== this.props.selectedApplicationId
            || props.selectedSection != this.props.selectedSection
            || props.selectedGroup != this.props.selectedGroup
            || props.isGroupView != this.props.isGroupView) {
            if (props.selectedLanguage && (props.selectedSection || props.selectedGroup) && props.selectedApplicationId && props.userInfo) {
                this.getPdfTranslations(props);
            }
        }

        if (props.selectedApplicationId == this.props.selectedApplicationId) {
            const recievedApplication = props.applications.find(app => app.Id == props.selectedApplicationId);
            const currentApplication = this.props.applications.find(app => app.Id == this.props.selectedApplicationId);

            if (currentApplication.IsLocked !== recievedApplication.IsLocked) {
                currentApplication.IsLocked = recievedApplication.IsLocked;
                this.setState({});
            }
        }

        if (props.isVisible !== this.props.isVisible) {
            this.setState(prevState => {
                if (props.isAllIgnored) {
                    prevState.translations.forEach(item => {
                        item.isVisible = props.isVisible;
                    });
                }

                return prevState;
            }, () => {
                this.props.getNotifications();
            })
        }
    }

    shouldComponentUpdate(nextProps) {
        if (!this.state.loaded) {
            return true;
        }

        if (this.props.isGroupView) {
            const nextSelectedGroup = nextProps.selectedApp
                && nextProps.selectedApp.Group
                && nextProps.selectedApp.Group.find(group => group.Id === nextProps.selectedGroup);

            const currSelectedGroup = this.props.selectedApp
                && this.props.selectedApp.Group
                && this.props.selectedApp.Group.find(group => group.Id === this.props.selectedGroup);

            if (nextSelectedGroup && currSelectedGroup && this.props.notesModalVisible) {
                return nextSelectedGroup.Note !== currSelectedGroup.Note;
            } else {
                return true;
            }
        } else {
            const nextSelectedSection = nextProps.selectedApp
                && nextProps.selectedApp.Section
                && nextProps.selectedApp.Section.find(section => section.Id == nextProps.selectedSection);

            const currSelectedSection = this.props.selectedApp
                && this.props.selectedApp.Section
                && this.props.selectedApp.Section.find(section => section.Id == this.props.selectedSection);

            if (nextSelectedSection && currSelectedSection && this.props.notesModalVisible) {
                return nextSelectedSection.Note !== currSelectedSection.Note;
            } else {
                return true;
            }
        }
    }

    errorHandler = (error) => {
        if (error.response && error.response.status) {
            if (error.response.status == 401) {
                localStorage.removeItem("gsuser");
                this.props.history.push('/');
            }
        }
        else {
            this.showToast(Strings.Texts.Danger, error.message);
        }
    }

    handleTranslationInputChange = (event) => {
        const name = event.target.getAttribute('data-name');
        const id = parseInt(event.target.getAttribute('data-id'));
        const translations = _.cloneDeep(this.state.translations); // deep clone to avoid direct mutation of current state.
        const translation = translations.find(translation => translation.textId === id);

        /* For a translator, save icon should be rendered if: 
           
           1. It is not equal to its previous value. 
           2. Its english translation is changed by the developer. */

        if (name == "englishTranslation") {
            translation.shouldRenderSaveIcon = translation.originalEnglishTranslation !== event.target.value;
            translation.englishTranslation = event.target.value;


            if (this.selectedTranslation) {
                this.selectedTranslation.englishTranslation = event.target.value;
                this.selectedTranslation.shouldRenderSaveIcon = translation.shouldRenderSaveIcon;
            }
        } else {
            translation.shouldRenderSaveIcon = translation.originalTranslation !== event.target.value
            translation[name] = event.target.value;

            if (this.selectedTranslation) {
                this.selectedTranslation[name] = translation[name];
                this.selectedTranslation.shouldRenderSaveIcon = translation.shouldRenderSaveIcon;
            }
        }

        this.setState({ translations });
    }

    handleKeyChange = (event) => {
        var target = event.target,
            value = target.value;
        this.setState({ [target.getAttribute("data-name")]: value });
    }

    publishTranslations = () => {
        var temp = this.props.versionId === 0 ? this.state.translations : this.state.translations.filter(trans => trans.releaseId === this.props.versionId), ids = [];
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].statusId = TranslationStatus.Updated) {
                ids.push(temp[i].id);
            }
        }

        let groups = this.props.applications.find(x => x.Id == this.props.selectedApplicationId).Group;
        let groupIds = groups.map(x => x.Id).join(',');

        this.setState({ loaded: false });
        axios.get(Strings.URL.GcLocalizerServiceBaseUrl + `PdfTranslation/PublishPdfTranslations?languageId=${this.props.selectedLanguage.id}&applicationId=${this.props.selectedApplicationId}&releaseId=${this.props.versionId}`, { headers: this.headers })
            .then(res => {
                Modal.success({
                    title: 'Success',
                    content: (
                        <div>
                            <p>Translations published successfully.</p>
                        </div>
                    ),
                    onOk() { },
                });
                this.getPdfTranslations(null, true);
            }).catch(error => {
                this.props.errorHandler(error);
            });
    }

    showAddTranslationOption = () => {
        if (this.props.userInfo && this.props.userInfo.role === Roles.Developer && !this.props.isGroupView && !this.props.selectedApp.IsLocked) {
            return (
                <Row className="buttons">
                    <Button className="links" onClick={this.showAddTranslation} type="primary" icon="plus" size="large">
                        Add
                    </Button>
                </Row>
            )
        }
    }

    onPdfStatusChange = (info, translation, isTranslated = false, isReplace = false, isAddMultiple = false) => {
        if (info.file.status !== "done" && info.file.status !== "error") {
            if (!isTranslated && translation.englishPdfPath.find(x => x.split("/").pop() === info.file.name)) {
                Modal.info({
                    title: "File with same name already exists.",
                    content: ''

                });
                return false;
            }

            const formData = new FormData();

            if (this.props.isGroupView) {
                const pdfTranslationObj = {
                    PdfTranslationId: isTranslated ? translation.translatedPdfTranslationId : translation.englishPdfTranslationId[0],
                    LanguageId: isTranslated ? this.props.selectedLanguage.id : Languages.English,
                    Language: isTranslated ? this.props.selectedLanguage.name : "English",
                    Key: translation.key,
                    GroupId: this.props.selectedGroup,
                    StatusId: 2,
                    Path: "",
                    PdfId: translation.id,
                    isAddMultiple: isAddMultiple,
                    IsVisible: translation.isVisible
                };

                if (isTranslated) {
                    formData.append(info.file.name, info.file);
                }
                else {
                    formData.append(info.file.name, info.file.originFileObj);
                }
                formData.append("group", this.props.applications
                    .find(x => x.Id == this.props.selectedApplicationId)
                    .Group.find(x => x.Id == this.props.selectedGroup).Name.trim());
                formData.append("application", this.props.applications.find(x => x.Id == this.props.selectedApplicationId).Name.trim());
                formData.append("groupId", this.props.selectedGroup);
                formData.append("languageId", this.props.selectedLanguage.id);
                formData.append("pdfTranslation", JSON.stringify(pdfTranslationObj));
                this.setState({ loaded: false });
                axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'PdfTranslation/AddPdfTranslationForGroup', formData, { headers: this.headers })
                    .then(() => {
                        this.getPdfTranslations(null, true);
                    })
                    .catch(err => {
                        this.props.errorHandler(err);
                        this.setState({ loaded: true });
                    });

            } else {
                const pdfTranslationObj = {
                    PdfTranslationId: isTranslated ? translation.translatedPdfTranslationId : translation.englishPdfTranslationId[0],
                    LanguageId: isTranslated ? this.props.selectedLanguage.id : Languages.English,
                    Language: isTranslated ? this.props.selectedLanguage.name : "English",
                    Key: translation.key,
                    SectionId: this.props.selectedSection,
                    StatusId: 2,
                    Path: "",
                    PdfId: translation.id,
                    isAddMultiple: isAddMultiple,
                    IsVisible: translation.isVisible
                };

                if (isTranslated) {
                    formData.append(info.file.name, info.file);
                }
                else {
                    formData.append(info.file.name, info.file.originFileObj);
                }
                formData.append("section", this.props.applications
                    .find(x => x.Id == this.props.selectedApplicationId)
                    .Section.find(x => x.Id == this.props.selectedSection).Name.trim());
                formData.append("application", this.props.applications.find(x => x.Id == this.props.selectedApplicationId).Name.trim());
                formData.append("sectionId", this.props.selectedSection);
                formData.append("languageId", this.props.selectedLanguage.id);
                formData.append("pdfTranslation", JSON.stringify(pdfTranslationObj));
                this.setState({ loaded: false });
                axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'PdfTranslation/AddPdfTranslation', formData, { headers: this.headers })
                    .then(() => {
                        this.getPdfTranslations(null, true);
                    })
                    .catch(err => {
                        this.props.errorHandler(err);
                        this.setState({ loaded: true });
                    });
            }
        }
    }

    onNewPdfStatusChange = (info, key) => {
        const fileList = info.fileList.reduce((acc, file) => {
            const { thumbUrl, ...rest } = file;
            if (info.file.status === 'done') {
                rest.thumbUrl = info.file.response.accessUrl;
                rest.url = info.file.response.accessUrl;
            }
            if (info.file.uid == file.uid) {
                acc.push(rest);
            }
            return acc;
        }, []);
        this.setState(prevState => ({ sectionFileListMap: { [0]: fileList } }));
    }

    onImageRemove = (file) => {
        this.setState({ sectionFileListMap: null });
    }

    onPdfRemove = (pdfTranslationId, path, isTranslated = false, isMultiple = false) => {
        this.setState({ loaded: false });
        axios
            .delete(`${Strings.URL.GcLocalizerServiceBaseUrl}PdfTranslation/DeletePdf?pdfTranslationId=${pdfTranslationId}&path=${path}&isMultiple=${isMultiple}`, { headers: this.headers })
            .then(res => {
                let translations = _.cloneDeep(this.state.translations);
                let index = -1;
                if (isTranslated) {
                    index = translations.findIndex(x => x.translatedPdfTranslationId == pdfTranslationId)
                    translations[index].translatedPdfPath = null;
                    translations[index].statusId = 1;

                }
                else {
                    index = translations.findIndex(x => x.englishPdfTranslationId.find(id => id == pdfTranslationId));
                    if (isMultiple) {
                        translations[index].englishPdfPath = translations[index].englishPdfPath.filter(x => x != path);
                    }
                    else {
                        translations[index].englishPdfPath = [];
                        translations[index].statusId = 1;
                    }

                }
                this.setState({ translations: translations, loaded: true }, () => {
                    this.props.getNotifications();
                    this.getPdfTranslations();
                });
            })
            .catch(reason => {
                this.props.errorHandler(reason);
            });
    }

    showPublishOption = () => {
        if (this.props.userInfo && this.props.userInfo.role === Roles.Developer && !this.props.isGroupView) {
            return (
                <Row className="buttons">
                    <Button className="links" type="primary" size="large" onClick={this.publishTranslations}>Publish</Button>
                </Row>
            );
        }
    }
    showAddNotesOption = (note) => {
        if (this.props.userInfo && this.props.userInfo.role === Roles.Developer && this.props.isGroupView && !this.props.selectedApp.IsLocked) {
            return (
                <Row className="buttons">
                    {!note && <Button className="links" onClick={this.props.addNote} type="primary" icon="plus" size="large">
                        Add Note
                    </Button>}
                </Row>
            )
        }
    }

    deletePdfConfirm = (pdfTranslationId, path, isTranslated = false, isMultiple) => {
        Modal.confirm({
            title: "Confirm file delete",
            content: '',
            okText: Strings.Texts.Yes,
            cancelText: Strings.Texts.No,
            onOk: () => {
                this.onPdfRemove(pdfTranslationId, path, isTranslated, isMultiple);
            },
            onCancel() {

            }
        });

    }
    deleteTranslation = (event) => {
        const target = event.currentTarget;
        const id = parseInt(target.getAttribute("data-id"));

        Modal.confirm({
            title: Strings.Texts.TranslationDeleteConfirmation,
            content: '',
            okText: Strings.Texts.Yes,
            cancelText: Strings.Texts.No,
            onOk: () => {
                this.setState({ loaded: false });
                axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}PdfTranslation/DeletePdfTranslation?pdfId=${id}`, { headers: this.headers })
                    .then(res => {
                        this.showToast(Strings.Texts.Success, Strings.Texts.TranslationDeleteSucess);
                        this.getPdfTranslations(null, true);
                    }).catch(error => {
                        this.props.errorHandler(error);
                    });
            },
            onCancel() {

            }
        });
    }

    showToast = (type, message) => {
        let color = "";
        if (type == "danger") {
            color = { background: '#ee3647', text: "#FFFFFF" };
            notify.show(message, "custom", 2500, color);
        }
        else if (type == "info") {
            color = { background: '#0E1717', text: "#FFFFFF" };
            notify.show(message, "custom", 2000, color);
        }
        else if (type == "success") {
            color = { background: '#008000', text: "#FFFFFF" };
            notify.show(message, "custom", 5000, color);
        }
    }

    addTranslation = () => {

        let message = null;
        if (this.state.translations.findIndex(x => x.key.trim() == this.state.newKey.trim()) > -1) {
            Modal.warning({
                title: 'Warning',
                content: (
                    <div>
                        <p>Key already exists in this page.</p>
                    </div>
                ),
                onOk() { },
            });
            return;
        }

        if (!this.state.newKey.trim()) {
            message = "Please provide value for key"
        }
        else if (this.state.newPdfRelease === 0) {
            message = "please select a release"
        }
        else if (!this.state.sectionFileListMap) {
            message = "Please Upload Pdf."
        }
        if (message) {
            Modal.warning({
                title: 'Warning',
                content: (
                    <div>
                        <p>{message}</p>
                    </div>
                ),
                onOk() { },
            });
            return;
        }

        const pdfTranslationObj = {
            PdfTranslationId: -1,
            LanguageId: Languages.English,
            Language: this.props.selectedLanguage.name,
            Key: this.state.newKey.trim(),
            SectionId: this.props.selectedSection,
            StatusId: 2,
            Path: "",
            TextId: 0,
            IsVisible: true,
            ReleaseId: this.state.newPdfRelease,
        };

        const application = this.props.applications.find(x => x.Id == this.props.selectedApplicationId);
        const section = application.Section.find(x => x.Id == this.props.selectedSection);

        const formData = new FormData();
        formData.append(this.state.sectionFileListMap[0][0].name, this.state.sectionFileListMap[0][0].originFileObj);
        formData.append("section", section.Name.trim());
        formData.append("application", application.Name.trim());
        formData.append("sectionId", this.props.selectedSection);
        formData.append("pdfTranslation", JSON.stringify(pdfTranslationObj));
        formData.append("languageId", this.props.selectedLanguage.id)

        if (this.state.newKey.trim().length != 0) {
            this.setState({ loaded: false });
            axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'PdfTranslation/Upload', formData, { headers: this.headers })
                .then(() => {
                    this.showToast(Strings.Texts.Success, Strings.Texts.TranslationAddSucess);
                    this.getPdfTranslations(null, true);
                    this.hideAddTranslation();
                }).catch(error => {
                    this.props.errorHandler(error);
                });
        }
        else {
            let message = this.state.newKey.trim().length == 0 ?
                Strings.Texts.EmptyNewKey : Strings.Texts.EmptyNewTranslation;
            this.showToast(Strings.Texts.Danger, message);
        }
    }

    showAddTranslation = () => {
        this.setState({
            addTranslateOptionVisible: true
        });
    }

    hideAddTranslation = () => {
        this.setState({
            addTranslateOptionVisible: false,
            newKey: "",
            sectionFileListMap: "",
            newPdfRelease: 0,
        });
    }

    toggleTranslationModal = (id) => {
        const textId = id;

        if (textId) {
            this.selectedTranslation = _.cloneDeep(this.state.translations.find(translation => translation.id == id)); // deep clone to avoid mutation of state.
        } else {
            this.selectedTranslation = null;
        }
        this.setState(prevState => ({ isModalVisible: !prevState.isModalVisible }));
    }

    renderTranslationModal = () => {
        if (!this.selectedTranslation) {
            return;
        }

        return this.selectedTranslation.renderModal();
    }

    handleSaveClick(event, modalTranslation) {
        let target = event.currentTarget;
        let id = parseInt(target.getAttribute("data-id"));
        let temp = _.cloneDeep(this.state.translations);
        const translation = temp.find(o => o.id === id);

        var obj = {
            Id: translation.id,
            ReleaseId: translation.releaseId,
        }
        if (this.state.isModalVisible) {
            this.setState({ isModalVisible: false });
        }
        axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/SaveVersion', obj, { headers: this.headers })
            .then(() => {
                this.getPdfTranslations(null, true);
                this.showToast(Strings.Texts.Success, Strings.Texts.SavedSuccess);
                return true; // on success;
            }).catch(error => {
                this.props.errorHandler(error);
                return false; // on failure;
            });
    }

    getPdfTranslations = (propsParam = null, shouldUpdateNotifications = false) => {
        let props = propsParam ? propsParam : this.props
        let langId = props.selectedLanguage.id;
        let sectionId = props.selectedSection;
        let groupId = props.selectedGroup;
        let userInfo = props.userInfo;
        this.setState({
            loaded: false
        });
        if (props.isGroupView) {
            axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}PdfTranslation/GetGroupPdfTranslations?groupId=${groupId}&languageId=${langId}&userId=${userInfo.id}`, { headers: this.headers })
                .then(res => {
                    let translations;
                    if (res.status === 204) { // if there are no groups then no translation are returned
                        translations = [];
                    } else {
                        const list = res.data.pdfTranslations;
                        translations = (list.length > 0) ? this.createPdfTranslationList(list, langId) : [];

                        if (res.data.application) {
                            if (props.selectedApp) {
                                props.selectedApp.IsLocked = res.data.application.isLocked;
                                this.props.selectedApp.IsLocked = props.selectedApp.IsLocked;
                            }
                        }
                    }

                    this.setState({
                        loaded: true,
                        translations: translations,
                        addTranslateOptionVisible: false,
                        newKey: ''
                    }, () => {
                        if (shouldUpdateNotifications) {
                            this.props.getNotifications();
                        }
                    });
                }).catch(error => {
                    this.props.errorHandler(error);
                });
        } else {
            axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}PdfTranslation/GetPdfTranslations?sectionId=${sectionId}&languageId=${langId}&userId=${userInfo.id}`, { headers: this.headers })
                .then(res => {
                    const list = res.data.pdfTranslations;
                    let translations = (list.length > 0) ? this.createPdfTranslationList(list, langId) : [];

                    if (res.data.application) {

                        if (props.selectedApp) {
                            props.selectedApp.IsLocked = res.data.application.lockedCode === STATUS_CODES.ApplicationLocked;
                            this.props.selectedApp.IsLocked = props.selectedApp.IsLocked;
                        }
                    }

                    this.setState({
                        loaded: true,
                        translations: translations,
                        addTranslateOptionVisible: false,
                        newKey: ''
                    }, () => {
                        if (shouldUpdateNotifications) {
                            this.props.getNotifications();
                        }
                    });
                }).catch(error => {
                    this.props.errorHandler(error);
                });
        }
    }

    createPdfTranslationList(list, langId) {
        let pdfTranslations = [];
        for (let i = 0; i < list.length; i++) {
            const pdfTranslation = {
                id: list[i].pdfId,
                key: list[i].key,
                statusId: list[i].pdfTranslation[0].statusId,
                englishPdfPath: list[i].pdfTranslation.filter(x => x.languageId == 1).map(item => item.path),
                englishPdfTranslationId: list[i].pdfTranslation.filter(x => x.languageId == 1).map(item => item.pdfTranslationId),
                translatedPdfPath: '',
                translatedPdfTranslationId: null,
                releaseId: list[i].releaseId,
                originalRelease: list[i].releaseId
            };

            const obj = _.find(list[i].pdfTranslation, function (o) { return o.languageId === langId; });

            if (obj) {
                pdfTranslation.isVisible = obj.isVisible;
                if (this.props.selectedLanguage.id != 1) {
                    pdfTranslation.statusId = obj.statusId;
                }
                pdfTranslation.translatedPdfPath = obj.path;
                pdfTranslation.translatedPdfTranslationId = obj.pdfTranslationId;
            }
            else {
                pdfTranslation.isVisible = true;
                pdfTranslation.statusId = 1;
            }

            pdfTranslations.push(pdfTranslation);
        }
        return pdfTranslations;
    }

    handleIgnoreClick = (translation) => {
        this.setTranslationState([SET_COMMANDS.SET_LOADED]);
        translation.isVisible = !translation.isVisible;
        translation.pdfId = translation.id;

        const transferObject = {
            languageId: this.props.selectedLanguage.id,
            pdfTranslations: [translation]
        };

        axios.post(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/ToggleIgnore`, transferObject, { headers: this.headers })
            .then(() => {
                this.setState(prevState => {
                    const prevTranslation = prevState.translations.find(t => t.id === translation.pdfId);
                    prevTranslation.isVisible = translation.isVisible;

                    if (prevTranslation.isVisible) {
                        this.props.updateApplicationIgnore({ type: 'translation' });
                    }

                    prevState.loaded = true;
                    return prevState;
                }, () => {
                    this.props.getNotifications();
                    this.getPdfTranslations();
                });
            })
            .catch(reason => {
                this.resetTranslationState([RESET_COMMANDS.RESET_LOADED])
                this.props.errorHandler(reason);
            });
    }
    showModal = () => {
        this.setState({ isModalVisible: true });
    }

    handleReleaseChange = (id, value) => {
        const translations = _.cloneDeep(this.state.translations); // deep clone to avoid direct mutation of current state.
        const translation = translations.find(translation => translation.id === id);
        translation.releaseId = value;
        this.selectedTranslation.shouldRenderSaveIcon = translation.releaseId !== translation.originalRelease;
        this.setState({ translations });
    }

    renderReleaseOPtions = () => {
        return (
            this.props.version.map(ver => {
                return (
                    <Option value={ver.id} key={ver.id + ver.versionName} > {ver.versionName}</Option >
                );
            })
        );

    }
    onTranslationModalCancel(translation) {
        const translations = _.cloneDeep(this.state.translations); // deep clone to avoid mutation of state.
        const currentTranslation = translations.find(t => t.id === translation.id);
        currentTranslation.releaseId = currentTranslation.originalRelease;

        this.setState(prevState => ({
            isModalVisible: !prevState.isModalVisible,
            translations: translations
        }));
    }


    renderTranslations = () => {
        const keyDisabled = this.props.userInfo && this.props.userInfo.role === Roles.Developer ? 'disabled' : 'disabled';
        const translations = this.props.userInfo && this.props.userInfo.role === Roles.Translator && !this.props.includeIgnored ? this.state.translations.filter(translation => translation.isVisible) : this.state.translations;
        const versionTranslations = this.props.versionId === 0 ? translations : translations.filter(trans => trans.releaseId === this.props.versionId);

        if (versionTranslations.length === 0) {
            return <div className="no-result-section" ><Icon type="warning" />There are no strings to translate for now!</div>
        }

        return versionTranslations.map((t, index) => {
            let statusClass = '';
            let uploadBtnBackgroundColor = '';

            switch (t.statusId) {
                case 1:
                    statusClass = 'new-pdf-translation';
                    break;

                case 2:
                    statusClass = 'updated-pdf-translation';
                    break;

                case 3:
                    statusClass = 'published-pdf-translation';
                    break;

                default:
                    break;
            }

            if (!t.isVisible) {
                statusClass = 'ignored-pdf-translation';
                uploadBtnBackgroundColor = '#d7d7d7';
            }

            const deleteIcon = this.props.userInfo.role === Roles.Developer && !this.props.selectedApp.IsLocked ? (
                <Col span={1} title="Delete translation">
                    <Icon type="delete" theme="twoTone" onClick={this.deleteTranslation} data-id={t.id} className="translation-row-icon" />
                </Col>
            ) : '';
            let checkEnglishRole = (this.props.selectedLanguage.id == Languages.English && this.props.userInfo.userLanguage.findIndex(x => x.languageId == Languages.English) > -1)
            let checkTranslatorRole = this.props.userInfo.role == Roles.Developer || this.props.userInfo.userLanguage.findIndex(x => x.languageId == this.props.selectedLanguage.id) > -1
            let translatedPdfName = t.translatedPdfPath ? t.translatedPdfPath.split("/").pop() : '';
            let trimTranslatedPdfName = translatedPdfName.length > 25 ? translatedPdfName.slice(0, 25) + "..." : translatedPdfName;
            const addIcon = (this.props.selectedLanguage.id == Languages.English && this.props.userInfo.role === Roles.Developer || checkEnglishRole) && !this.props.selectedApp.IsLocked ? (
                <Upload
                    fileList={[]}
                    listType='text'
                    className='ant-upload-list-item'
                    onChange={(e) => this.onPdfStatusChange(e, t, false, false, true)}>
                    {/* <Col span={1} title="Add File"> */}
                    <Icon type="plus-circle" theme="twoTone" className="translation-row-icon" />
                    {/* </Col> */}
                </Upload>
            ) : '';

            const editIcon = this.props.userInfo.role == Roles.Developer ?
                <Col span={1} title="Edit Translation">
                    <Icon type="edit" theme="twoTone" onClick={() => this.toggleTranslationModal(t.id)} className="translation-row-icon" />
                </Col> : "";


            let multipleEnglishFiles = t.englishPdfPath && t.englishPdfPath.filter(item => item).map((ePdfPath, index) => {
                let englishPdfName = ePdfPath.split("/").pop();
                let trimEnglishPdfName = englishPdfName.length > 25 ? englishPdfName.slice(0, 25) + "..." : englishPdfName;
                return (<React.Fragment>

                    <div className="uploaded-pdf">
                        <div className={statusClass}>
                            <div className="ant-upload-list ant-upload-list-text">
                                <div className="ant-upload-list-item ant-upload-list-item-undefined">
                                    <div className="ant-upload-list-item-info">
                                        <Icon type="paper-clip" theme="outlined" />
                                        <i className="anticon anticon-paper-clip"></i>
                                        <a rel="noopener noreferrer"
                                            className="ant-upload-list-item-name" title={englishPdfName}
                                            onClick={() => this.onPdfDownload(ePdfPath)}
                                            href="javascript:void(0);" >{trimEnglishPdfName}</a>
                                    </div>
                                    {/* {this.props.userInfo.role == 1 && checkEnglishRole && editEnglishPdfIcon} */}
                                    {(this.props.userInfo.role == Roles.Developer && this.props.selectedLanguage.id == Languages.English || checkEnglishRole) && <Icon type="close" theme="outlined" onClick={() => this.deletePdfConfirm(t.englishPdfTranslationId[index], ePdfPath, false, t.englishPdfPath.length > 1)} />}
                                </div>
                            </div>
                        </div>
                    </div>

                </React.Fragment >)
            });

            const englishPdfUploadIcon = t.englishPdfPath && t.englishPdfPath[0] ? <Col span={7} type="flex" className="wrapper-upload-multiple-pdf"> {multipleEnglishFiles}</Col> :
                <Col span={7} className="uploaded-pdf">
                    <div className={statusClass}>
                        {this.props.userInfo.role == 1 || checkEnglishRole ?
                            <Upload
                                fileList={[]}
                                className='upload-list-inline'
                                listType='text'
                                onChange={(e) => this.onPdfStatusChange(e, t)}
                            >
                                <Button type="file"><Icon type="upload" />Upload</Button>
                            </Upload> : <div style={{ 'line-height': '30px' }}>No File Exists</div>}</div></Col>

            const translatedPdfUploadIcon = t.translatedPdfPath ?
                <Col span={7} className="uploaded-pdf">
                    <div className={statusClass}>
                        <div className="ant-upload-list ant-upload-list-text">
                            <div className="ant-upload-list-item ant-upload-list-item-undefined">
                                <div className="ant-upload-list-item-info">
                                    <span>
                                        <Icon type="paper-clip" theme="outlined" />
                                        <a rel="noopener noreferrer"
                                            className="ant-upload-list-item-name" title={translatedPdfName}
                                            onClick={() => this.onPdfDownload(t.translatedPdfPath)}
                                            href="javascript:void(0);" >{trimTranslatedPdfName}</a>
                                    </span>
                                </div>
                                {checkTranslatorRole && <Icon type="close" theme="outlined" onClick={() => this.deletePdfConfirm(t.translatedPdfTranslationId, t.translatedPdfPath, true)} />}
                            </div>
                        </div>
                    </div>
                </Col>
                :
                <Col span={7} className="uploaded-pdf">
                    <div className={statusClass}>
                        {checkTranslatorRole ?
                            <div>
                                <Upload
                                    beforeUpload={() => false}
                                    fileList={[]}
                                    className='upload-list-inline'
                                    listType='text'
                                    onChange={(e) => this.onPdfStatusChange(e, t, true)}
                                >
                                    <Button style={{ backgroundColor: uploadBtnBackgroundColor }}><Icon type="upload" />Upload</Button>
                                </Upload>
                            </div>
                            : <div style={{ 'line-height': '30px' }}>No File Exists</div>}
                    </div>
                </Col>

            const translatedSection = this.props.selectedLanguage.id !== 1 ? <React.Fragment>
                {translatedPdfUploadIcon}
            </React.Fragment> : null;

            const ignoreIcon = this.props.userInfo.role === Roles.Translator && !this.props.selectedApp.IsLocked ? (
                <Col span={1} title={t.isVisible ? 'Ignore translation' : 'Un-ignore translation'}>
                    <Icon type={t.isVisible ? "eye-invisible" : "eye"} theme="twoTone" data-id={t.textId} onClick={() => this.handleIgnoreClick(t)} className="translation-row-icon" />
                </Col>
            ) : '';
            const self = this;
            t.renderModal = function () {

                const handleModalSaveClick = self.handleSaveClick.bind(this);
                return <Modal
                    title="Translation"
                    visible={self.state.isModalVisible}
                    onCancel={() => { self.onTranslationModalCancel(this) }}
                    destroyOnClose={true}
                    footer={[
                        <Button key="cancel" type="danger" onClick={() => { self.onTranslationModalCancel(this) }} >Cancel</Button>,
                        <Button
                            key="submit" type="primary"
                            onClick={(e) => handleModalSaveClick(e, this)}
                            data-id={this.id} data-translationid={this.translationId}
                            data-statusid={this.statusId} disabled={!this.shouldRenderSaveIcon}>Save</Button>,
                    ]}
                >
                    <div>
                        {self.props.userInfo.role === Roles.Developer ?
                            <Select
                                style={{ marginBottom: "20px", width: "50%" }}
                                defaultValue={this.releaseId}
                                autosize={{ minRows: 3 }}
                                onChange={(e) => self.handleReleaseChange(this.id, e)}
                            >
                                {self.renderReleaseOPtions()}
                            </Select>
                            : null}

                        {<Input value={this.key} readOnly className={statusClass} style={{ marginBottom: "20px" }} />}
                    </div>
                </Modal>
            }
            return (

                <Row type="flex" key={t.id}>
                    <Col span={2} className="sno-pos">
                        {t.id}
                    </Col>
                    <React.Fragment key={"key-pdf-fragement"}>
                        {/* {this.props.userInfo.role === Roles.Developer ? */}
                        <Col span={4} title={t.key} className="key-section">
                            <Input style={{ 'height': '100%' }} size="default" className={statusClass} placeholder="large size" disabled={keyDisabled} data-id={t.textId} data-name="key" value={t.key} />
                        </Col>
                        {/* : '' } */}
                        {englishPdfUploadIcon}
                        {translatedSection}
                    </React.Fragment>

                    <Col span={2} >
                        {addIcon && <Col span={8} title="Add File">
                            {addIcon}
                        </Col>}
                        <Col span={8}>
                            {editIcon}
                        </Col>
                        <Col span={8} >
                            {deleteIcon}
                        </Col>
                        {ignoreIcon}
                    </Col>
                </Row>

            );
        });
    }
    uploadFileToKey = (e, t) => {
    }
    onPdfDownload = (path) => {
        this.setState({ loaded: false });
        axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}PdfTranslation/DownloadPdf?path=${encodeURIComponent(path)}`, { headers: this.headers })
            .then(res => {
                var element = document.createElement('a');
                element.setAttribute('href', res.data);
                element.setAttribute('download', path.split("/").pop() + ".pdf");

                element.style.display = 'none';
                document.body.appendChild(element);

                element.click();
                document.body.removeChild(element);
                this.setState({ loaded: true });
            });
    }
    selectFileOption = () => {
        if (this.props.userInfo && this.props.userInfo.role === Roles.Developer) {
            return (
                <Row className="buttons">
                    <Upload
                        beforeUpload={() => false}
                        fileList={this.state.sectionFileListMap && this.state.sectionFileListMap[0]}
                        className='upload-list-inline'
                        listType='text'
                        onChange={this.onNewPdfStatusChange}
                        onRemove={this.onImageRemove}
                        multiple={false}
                    >
                        <Button> <Icon type="upload" /> Select File </Button>
                        {/* <Button className="links" type="primary" icon="upload" size="large">Upload English Pdf</Button> */}
                    </Upload>
                </Row>
            );
        }
        else if (this.state.sectionFileListMap &&
            this.state.sectionFileListMap[0] &&
            this.state.sectionFileListMap[0][0] &&
            this.state.sectionFileListMap[0][0].thumbUrl) {
            return (
                <Row className="buttons" style={{ top: '20px', position: 'relative' }}>
                    <a href={this.state.sectionFileListMap[0][0].thumbUrl} target="_blank">
                        <img src={this.state.sectionFileListMap[0][0].thumbUrl} width="100px" height="100px" />
                    </a>
                    <div>
                        <a href={this.state.sectionFileListMap[0][0].thumbUrl} target="_blank">
                            {this.state.sectionFileListMap[0][0].name}
                        </a>
                    </div>
                </Row>
            );
        }
    }

    showLockOption = () => {
        if (this.props.userInfo && this.props.userInfo.role === Roles.Developer && !this.props.isGroupView) {
            const btnText = this.props.selectedApp.IsLocked ? 'Unlock' : 'Lock';

            return (
                <Row className="buttons">
                    <Button className="links" type="primary" size="large" onClick={this.props.toggleAppLock}>{btnText}</Button>
                </Row>
            )
        }
    }

    renderMaskContent = () => {
        if (!this.props.applications || !this.props.selectedApplicationId) {
            return;
        }

        if (!this.props.selectedApp || !this.props.selectedApp.IsLocked || !this.state.translations.length) {
            return;
        }
        const translations = this.props.userInfo && this.props.userInfo.role === Roles.Translator && !this.props.includeIgnored ? this.state.translations.filter(translation => translation.isVisible) : this.state.translations;
        const versionTranslations = this.props.versionId === 0 ? translations : translations.filter(trans => trans.releaseId === this.props.versionId);
        if (!versionTranslations.length) {
            return;
        }
        let width = '';
        let left = 8.2;

        if (this.props.selectedLanguage.id === 1) {
            if (this.props.userInfo.role === Roles.Translator) {
                width = '46.2'
            } else {
                width = '49';
            }
        } else {
            if (this.props.userInfo.role === Roles.Translator) {
                width = '75.3'
            } else {
                width = '78.3';
            }

        }

        return (
            <Tooltip placement="right" title="Application locked for publish">
                <div style={{ width: `${width}%`, left: `${left}%` }} className="mask-section"></div>
            </Tooltip>
        );
    }
    onDownloadAllFiles(isTranslated) {
        this.setState({ loaded: false });
        let languageId = isTranslated ? this.props.selectedLanguage.id : Languages.English;
        let sectionId = this.props.selectedSection;
        let includeIgnored = this.props.includeIgnored;
        axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}PdfTranslation/DownloadAllFiles?sectionId=${sectionId}&languageId=${languageId}&includeIgnored=${includeIgnored}&selectedLanguage=${this.props.selectedLanguage.id}`, { headers: this.headers })
            .then(res => {
                let zip = new JSzip();
                res.data.forEach(url => {
                    let fileName = url.split('?').shift().split('/').pop();
                    zip.file(decodeURIComponent(fileName), this.urlToPromise(url), { binary: true });

                });
                let self = this;
                zip.generateAsync({ type: "blob" })
                    .then(function callback(blob) {
                        let sectionName = self.props.applications.find(x => x.Id == self.props.selectedApplicationId)
                            .Section.find(x => x.Id == self.props.selectedSection).Name.trim();
                        FileSaver.saveAs(blob, sectionName + ".zip");
                        self.setState({ loaded: true });
                    }, function (e) {
                        self.setState({ loaded: true });
                    });
            });
    }

    urlToPromise(url) {
        return new Promise(function (resolve, reject) {
            JSZipUtils.getBinaryContent(url, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    renderDownloadAllOptions = () => {
        if (this.props.selectedApp.IsLocked) {
            return;
        }

        const trans = this.props.userInfo && this.props.userInfo.role === Roles.Translator && !this.props.includeIgnored ? this.state.translations.filter(translation => translation.isVisible) : this.state.translations;
        const versionTranslations = this.props.versionId === 0 ? trans : trans.filter(trans => trans.releaseId === this.props.versionId);

        if (versionTranslations.length === 0) {
            return;
        }
        let englishPdfLength = this.state.translations.filter(item => item.englishPdfPath[0]).length;
        let translations = this.state.translations.filter(item => item.translatedPdfPath);
        let translatedPdfLength = translations.length;

        return (<Row className="download-all">
            <Col span={6}></Col>
            {englishPdfLength > 0 && <Col span={7}><a onClick={(e) => this.onDownloadAllFiles(false)}><Icon type="download" /> Download All</a></Col>}
            {translatedPdfLength > 0 && this.props.selectedLanguage.id !== 1 && this.shouldShowDownloadAllButton(translations) && <Col span={7}><a onClick={(e) => this.onDownloadAllFiles(true)}><Icon type="download" /> Download All</a></Col>}
        </Row>);
    }

    // returns true 
    // 1. if any one translation is visible
    // 2. if there is any translation and this.props.includeIgnored === true
    shouldShowDownloadAllButton = (translations) => {
        if (translations.findIndex(t => t.isVisible) > -1) {
            return true;
        }
        if (translations.length > 0 && this.props.includeIgnored) {
            return true;
        }
        return false;
    }
    renderReleaseVersion = () => {
        const versions = this.props.version;
        return versions.map(ver => {
            return (
                <Option value={ver.id} key={ver.Id + ver.versionName}>{ver.versionName}</Option>
            )
        })
    }

    selectedRelease = (value) => {
        this.setState({ newPdfRelease: value })
    }

    render() {
        let buttonSection = null;
        let loader = null;
        let isUniqueICon = '';
        const selectedSection = this.props.selectedApp
            && this.props.selectedApp.Section
            && this.props.selectedApp.Section.find(section => section.Id == this.props.selectedSection);
        const note = selectedSection && selectedSection.Note;

        // Notes will now be shown on Group view.
        const selectedGroup = this.props.selectedGroup
            && this.props.selectedApp.Group
            && this.props.selectedApp.Group.find(group => group.Id == this.props.selectedGroup);

        const groupNote = selectedGroup && selectedGroup.Note;

        if (this.state.loaded && this.props.applications.length > 0) {
            buttonSection = <Col span={6}>
                <div className="buttons-block">
                    {this.showAddTranslationOption()}
                    {this.showPublishOption()}
                    {this.showLockOption()}
                    {this.showAddNotesOption(groupNote)}
                    {this.props.ignoreButtons}
                </div>
            </Col>
        }

        if (this.state.loaded == false) {
            loader = <div className="pdf-page-loader">
                <Spin size="large" />
            </div>
        }

        return (
            <React.Fragment>
                {loader}

                <Row>
                    <Col span={18}>
                        <Row>
                            {this.props.renderNote()}
                        </Row>
                        {this.renderDownloadAllOptions()}
                        <div id="text-area">
                            <Row>
                                {this.renderMaskContent()}
                                {this.renderTranslations()}
                            </Row>
                        </div>

                    </Col>
                    {buttonSection}
                </Row>
                <Drawer
                    title="Add New Key"
                    width={500}
                    placement="right"
                    onClose={this.hideAddTranslation}
                    maskClosable={false}
                    visible={this.state.addTranslateOptionVisible}
                    style={{
                        height: 'calc(100% - 55px)',
                        overflow: 'auto',
                        paddingBottom: 53,
                    }}
                    destroyOnClose={true}
                >
                    <Form layout="vertical" hideRequiredMark>
                        <Row gutter={6}>
                            <Col span={24} className='field'>
                                <Row>
                                    <Col span={20}>
                                        <Input maxLength="45" onChange={this.handleKeyChange} data-name="newKey" value={this.state.newKey} placeholder="Key" />
                                    </Col>
                                    <Col span={3} offset={1}>
                                        {isUniqueICon}
                                    </Col>
                                </Row>
                            </Col>
                            <Col span={20} style={{
                                marginBottom: '20px',
                            }} className='feild'>
                                <Select
                                    placeholder="Release"
                                    onChange={this.selectedRelease}
                                    optionFilterProp="children"
                                    filterOption={(input, option) =>
                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                >
                                    {this.renderReleaseVersion()}
                                </Select>
                            </Col>
                            <Col span={24} className='field'>
                                {this.selectFileOption()}
                            </Col>
                        </Row>
                    </Form>
                    <div
                        style={{
                            position: 'absolute',
                            bottom: 0,
                            width: '100%',
                            borderTop: '1px solid #e8e8e8',
                            padding: '10px 16px',
                            textAlign: 'right',
                            left: 0,
                            background: '#fff',
                            borderRadius: '0 0 4px 4px',
                        }}
                    >
                        <Button
                            style={{
                                marginRight: 8,
                            }}
                            onClick={this.hideAddTranslation}
                        >
                            Cancel
                        </Button>
                        <Button onClick={this.addTranslation} type="primary">Add</Button>
                    </div>
                </Drawer>
                {this.renderTranslationModal()}
            </React.Fragment >
        );
    }

    resetTranslationState = (resetCommands) => {
        const partialState = {};

        if (resetCommands.indexOf(RESET_COMMANDS.RESET_LOADED) >= 0) {
            partialState.loaded = true;
        }

        this.setState(partialState);
    }

    setTranslationState = (setCommands) => {
        const partialState = {};

        if (setCommands.indexOf(SET_COMMANDS.SET_LOADED) >= 0) {
            partialState.loaded = false;
        }

        this.setState(partialState);
    }
}
