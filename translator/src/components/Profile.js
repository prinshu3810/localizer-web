import React, { Component } from 'react';
import { Row, Col, Card, Checkbox } from 'antd';
import ChangePassword from './ChangePassword';
import Header from './Header';
import { PageRoutes } from '../constants/Constants';
import { get } from 'axios';
import Strings from '../constants/String';
import Notifications, { notify } from 'react-notify-toast';

class Profile extends Component{
    constructor(props){
        super(props);
        this.state={
            notificationCheckboxChecked: true, // get it from localstore
            notificationCheckboxdisabled: false // change it when changing status
        }
    }

    componentDidMount () {
        let user = localStorage.getItem('gsuser');
        if(!user){
            this.props.history.push('/');
        }
        this.setState({ notificationCheckboxChecked: !JSON.parse(user).notificationsEnabled});
    }

    // Handles the email notification user setting
    onNotificationCheckboxChange = (e) => {
        // assume API call was successful
        this.toggleNotificationsEnabledLocalStorage();
        this.setState({notificationCheckboxChecked: e.target.checked});
                this.toggleNotifications()
                            .catch(e=>{
                                this.showToast(Strings.Texts.Danger, this.errorMessage());
                                 // revert back assumption
                                this.setState({notificationCheckboxChecked: e.target.checked});
                                this.toggleNotificationsEnabledLocalStorage();
                            });
    }

    // toggle notificationsEnabled in localStorage
    toggleNotificationsEnabledLocalStorage = () => {
        let user = JSON.parse(localStorage.getItem('gsuser'));
        user.notificationsEnabled = !user.notificationsEnabled;
        localStorage.setItem('gsuser', JSON.stringify(user));

    }

    // returns the error message on the basis of checked status of notification enabled box
    errorMessage = () => {
        if(this.state.notificationCheckboxChecked){
            return("Error while enabling notifications");
        }
        else{
            return("Error while disabling notifications");
        }
    }

    // Calls the API to change the email notifications status and returns promise.
    toggleNotifications = () => {
        let headers = {
            authorization: `Bearer ${JSON.parse(localStorage.getItem("gsuser")).token}`
        };
        return get(Strings.URL.GcLocalizerServiceBaseUrl + "Login/toggleEmailNotifications", {headers: headers});
    }

    showToast = (type, message) => { console.log("called")
        let color = "";
        if (type == Strings.Texts.Danger) {
            color = { background: '#ee3647', text: "#FFFFFF" };
            notify.show(message, "custom", 2500, color);
        }
        else if (type == Strings.Texts.Info) {
            color = { background: '#0E1717', text: "#FFFFFF" };
            notify.show(message, "custom", 2000, color);
        }
        else if (type == Strings.Texts.Success) {
            color = { background: '#008000', text: "#FFFFFF" };
            notify.show(message, "custom", 5000, color);
        }
    }

    render() {
        return (<div>
                <Header history={this.props.history} pageRoute={PageRoutes.Profile} />
                <Row className="height-buffer"></Row>
                <Row className="profile-align">
                <div className="height-50">
                        {/* User Settings Section*/}
                        <Card  title="User Settings" className="profile-card">
                            <Row span={24}>
                                <Col lg={12} md={11} sm={12} xs={16}>Turn off email notifications</Col>
                                <Col lg={6} md={7} sm={12} xs={8}>
                                <Checkbox
                                        checked={this.state.notificationCheckboxChecked}
                                        disabled={this.state.notificationCheckboxdisabled}
                                        onChange={this.onNotificationCheckboxChange}
                                ></Checkbox>
                                </Col>
                            </Row>
                        </Card>
                    </div>
                    <div className="height-50">
                    <ChangePassword history={this.props.history}/>
                    </div >
                </Row>
                <Notifications/>
            </div>
        );
    }
}

export default Profile;