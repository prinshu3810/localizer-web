import React, { Component } from 'react';
import { Input, Row, Col, Menu, Select, Icon, Button, Modal, Spin } from 'antd';
import axios from 'axios';
import Strings from '../constants/String';
import _ from 'lodash';
import Roles, { TranslationType } from '../constants/Constants';

const InputGroup = Input.Group;
const Search = Input.Search;
const Option = Select.Option;
const SearchOptions = { 'Page': 1, 'App': 2, 'AllApps': 3, 'Group': 4 };
export default class SearchBox extends Component {
    headers = {
        authorization: localStorage.getItem("gsuser") ? `Bearer ${JSON.parse(localStorage.getItem("gsuser")).token}` : null
    }
    delayedTextCallback;
    delayedFileCallback;
    selectedApplicationDetails;
    translationTypeId = null;
    constructor() {
        super();
        this.state = {
            loading: false,
            selectedSearchOption: null,
            searchResult: [],
            fileSearchResult: [],
            searchKeyword: "",
            noSearchOptionSelected: false,
            noTranslationTypeSelected: false,
            noSearchText: true,
            selectedTranslationType: "1"
        }
    }

    componentWillReceiveProps(props) {

        if (props.selectedSection != this.props.selectedSection || props.selectedApplicationId != this.props.selectedApplicationId) {
            let translationTypeId = props.selectedApplicationId && props.applications.find(app => app.Id === props.selectedApplicationId).TranslationTypeId.toString();
            this.setState({
                loading: false,
                selectedSearchOption: null,
                searchResult: [],
                fileSearchResult: [],
                searchKeyword: "",
                noSearchOptionSelected: false,
                noTranslationTypeSelected: false,
                noSearchText: true,
                selectedTranslationType: translationTypeId,
                searchKeyword: ""
            });
        }
    }

    searchOptionChange = (selectedSearchOption) => {
        let translationTypeId = this.props.selectedApplicationId && this.props.applications.find(app => app.Id === this.props.selectedApplicationId).TranslationTypeId.toString();
        this.setState({
            selectedSearchOption, noSearchOptionSelected: false, noTranslationTypeSelected: false, selectedTranslationType: translationTypeId
        });
    }

    getTextSearchResult = (searchKeyword = "") => {
        let keyword = searchKeyword || this.state.searchKeyword;
        let searchOption = this.state.selectedSearchOption;
        this.selectedApplicationDetails = this.props.applications.find(app => app.Id === this.props.selectedApplicationId);
        let sectionIds = "";
        let groupIds = "";

        if (this.props.isGroupView) {
            if (this.props.includeIgnored) {
                groupIds = (searchOption && searchOption == SearchOptions.App) ?
                    [this.selectedApplicationDetails.Group.map(item => item.Id).join()] :
                    (searchOption && searchOption == SearchOptions.Group) ? [this.props.selectedGroup.toString()] : [this.props.applications.filter(app => app.Group.length > 0).map(item => item.Group.map(group => group.Id)).join()];
            } else {
                groupIds = (searchOption && searchOption == SearchOptions.App) ?
                    [this.selectedApplicationDetails.Group.filter(grp => grp.IsVisible == true).map(item => item.Id).join()] :
                    (searchOption && searchOption == SearchOptions.Group) ? [this.props.selectedGroup.toString()] : [this.props.applications
                        .filter(app => app.Group.length > 0)
                        .map(item => {
                            if (this.props.userInfo.role == Roles.Developer) {
                                return item.Group.filter(grp => grp.IsVisible == true)
                                    .map(group => group.Id)
                            } else {
                                return item.Group.map(group => group.Id);
                            }
                        }).filter(item => item.length).join()];

            }
            const data = {
                keyword: keyword,
                languageId: this.props.selectedLanguage.id,
                roleId: this.props.userInfo.role,
                groupIds: groupIds[0]
            }

            axios.post(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/SearchTextTranslationForGroupV2`, data, { headers: this.headers })
                .then(res => {
                    let searchResult = res.data.translations;
                    let imageList = res.data.imageList;
                    searchResult.map(item => {
                        let currentApp = this.props.applications.find(app => app.Group.find(group => group.Id == item.groupId));

                        if (currentApp) {
                            const currentSection = currentApp.Group.find(group => group.Id == item.groupId);
                            const sectionName = currentSection.DisplayName || currentSection.Name;

                            Object.assign(item,
                                { applicationId: currentApp.Id },
                                { applicationName: currentApp.Name },
                                { isLocked: currentApp.IsLocked },
                                { sectionName: sectionName })
                        }
                    })
                    this.props.onSearch(searchResult, imageList, TranslationType.Text);
                    this.setState({ searchKeyword: keyword, loading: false });
                })
                .catch(err => {
                    this.setState({ loading: false });
                })
        } else {
            sectionIds = (searchOption && searchOption == SearchOptions.App) ?
                [this.selectedApplicationDetails.Section.map(item => item.Id).join()] :
                (searchOption && searchOption == SearchOptions.Page) ? [this.props.selectedSection.toString()] : [this.props.applications.filter(app => app.Section.length > 0).map(item => item.Section.map(section => section.Id)).join()];
            const data = {
                keyword: keyword,
                sectionIds: sectionIds[0],
                languageId: this.props.selectedLanguage.id,
                roleId: this.props.userInfo.role,
            }

            axios.post(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/SearchTextTranslationV2`, data, { headers: this.headers },)
                .then(res => {
                    let searchResult = res.data.translations;
                    let imageList = res.data.imageList;
                    searchResult.map(item => {
                        let currentApp = this.props.applications.find(app => app.Section.find(section => section.Id == item.sectionId));

                        if (currentApp) {
                            const currentSection = currentApp.Section.find(section => section.Id == item.sectionId);
                            const sectionName = currentSection.DisplayName || currentSection.Name;

                            Object.assign(item,
                                { applicationId: currentApp.Id },
                                { applicationName: currentApp.Name },
                                { isLocked: currentApp.IsLocked },
                                { sectionName: sectionName })
                        }
                    })
                    this.props.onSearch(searchResult, imageList, TranslationType.Text);
                    this.setState({ searchKeyword: keyword, loading: false });
                })
                .catch(err => {
                    this.setState({ loading: false });
                })
        }
    }

    getFileSearchResult = (searchKeyword = "") => {
        let keyword = searchKeyword ? searchKeyword : this.state.searchKeyword;
        let searchOption = this.state.selectedSearchOption;
        let groupIds = "";
        if (this.props.isGroupView) {
            if (this.props.includeIgnored) {
                groupIds = (searchOption && searchOption == SearchOptions.App) ?
                    [this.selectedApplicationDetails.Group.map(item => item.Id).join()] :
                    (searchOption && searchOption == SearchOptions.Group) ? [this.props.selectedGroup.toString()] : [this.props.applications.filter(app => app.Group.length > 0).map(item => item.Group.map(group => group.Id)).join()];
            } else {
                groupIds = (searchOption && searchOption == SearchOptions.App) ?
                    [this.selectedApplicationDetails.Group.filter(grp => grp.IsVisible == true).map(item => item.Id).join()] :
                    (searchOption && searchOption == SearchOptions.Group) ? [this.props.selectedGroup.toString()] :
                        [this.props.applications
                            .filter(app => app.Group.length > 0)
                            .map(item => {
                                if (this.props.userInfo.role === Roles.Developer) {
                                    return item.Group.filter(grp => grp.IsVisible == true)
                                        .map(group => group.Id)
                                } else {
                                    return item.Group.map(group => group.Id)
                                }
                            }).filter(item => item.length).join()];
            }
            const data = {
                keyword: keyword,
                languageId: this.props.selectedLanguage.id,
                roleId: this.props.userInfo.role,
                groupIds: groupIds[0]
            }

            axios.post(`${Strings.URL.GcLocalizerServiceBaseUrl}PdfTranslation/SearchFileTranslationForGroupV2`, data, { headers: this.headers })
                .then(res => {
                    let searchResult = res.data.pdfTranslations;
                    searchResult.map(item => {
                        let currentApp = this.props.applications.find(app => app.Group.find(group => group.Id == item.groupId));

                        if (currentApp) {
                            const currentSection = currentApp.Group.find(group => group.Id == item.groupId);
                            const sectionName = currentSection.DisplayName || currentSection.Name;

                            Object.assign(item,
                                { applicationId: currentApp.Id },
                                { applicationName: currentApp.Name },
                                { isLocked: currentApp.IsLocked },
                                { sectionName: sectionName })
                        }
                    })
                    this.props.onSearch(searchResult, null, TranslationType.Pdf);
                    this.setState({ searchKeyword: keyword, loading: false });
                })
                .catch(err => {
                    this.setState({ loading: false });
                })
        } else {
            this.selectedApplicationDetails = this.props.applications.find(app => app.Id === this.props.selectedApplicationId);
            let sectionIds = (searchOption && searchOption == SearchOptions.App) ?
                [this.selectedApplicationDetails.Section.map(item => item.Id).join()] :
                (searchOption && searchOption == SearchOptions.Page) ? [this.props.selectedSection.toString()] : [this.props.applications.map(item => item.Section.map(section => section.Id)).join()];

            const data = {
                keyword: keyword,
                sectionIds: sectionIds[0],
                languageId: this.props.selectedLanguage.id,
                roleId: this.props.userInfo.role,
            }
            axios.post(`${Strings.URL.GcLocalizerServiceBaseUrl}PdfTranslation/SearchFileTranslationV2`, data, { headers: this.headers })
                .then(res => {
                    let searchResult = res.data.pdfTranslations;
                    searchResult.map(item => {
                        let currentApp = this.props.applications.find(app => app.Section.find(section => section.Id == item.sectionId));
                        let currentSection = currentApp.Section.find(section => section.Id == item.sectionId);
                        let sectionName = currentSection.DisplayName || currentSection.Name;

                        Object.assign(item,
                            { applicationId: currentApp.Id },
                            { applicationName: currentApp.Name },
                            { isLocked: currentApp.IsLocked },
                            { sectionName: sectionName },
                            { sectionId: currentSection.Id })
                    })
                    this.props.onSearch(searchResult, null, TranslationType.Pdf);
                    this.setState({ searchKeyword: keyword, loading: false });
                })
                .catch(err => {
                    this.setState({ loading: false });
                })
        }
    }

    validateSearch(searchKeyword) {
        let searchOption = this.state.selectedSearchOption;


        if (!this.props.applications || this.props.applications.length == 0) { // return if no app exists
            return false;
        }
        if (!searchOption) { // return if no search option selected
            this.setState({ noSearchOptionSelected: true });
            return false;
        }
        else {
            this.setState({ noSearchOptionSelected: false });
        }
        this.selectedApplicationDetails = this.props.applications.find(app => app.Id === this.props.selectedApplicationId);
        this.translationTypeId = this.selectedApplicationDetails.TranslationTypeId;

        if (this.state.selectedSearchOption == SearchOptions.AllApps) {
            if (this.state.selectedTranslationType) {
                this.translationTypeId = this.state.selectedTranslationType;
                this.setState({ noTranslationTypeSelected: false });
            }
            else {
                this.setState({ noTranslationTypeSelected: true });
                return false;
            }
        }

        if (!searchKeyword || (searchKeyword && searchKeyword.length < 3)) {
            this.setState({ noSearchText: true });
            return false;
        }
        else {
            this.setState({ noSearchText: false });
        }

        return true;
    }

    updateSearchResult = () => {
        this.selectedApplicationDetails = this.props.applications.find(app => app.Id === this.props.selectedApplicationId);
        this.translationTypeId = this.selectedApplicationDetails.TranslationTypeId;

        if (this.state.selectedSearchOption == SearchOptions.AllApps) {
            if (this.state.selectedTranslationType) {
                this.translationTypeId = this.state.selectedTranslationType;
            }
        }
        if (this.translationTypeId == TranslationType.Text) {
            this.getTextSearchResult();
        }
        else {
            this.getFileSearchResult();
        }
    }

    onSearchClick = (searchKeyword = "") => {
        if (typeof searchKeyword != "string") { // to react on enter button
            searchKeyword = searchKeyword.target.value;
        }
        if (!this.validateSearch(searchKeyword.trim())) {
            return false;
        }
        this.setState({ loading: true });
        if (this.translationTypeId == TranslationType.Text) {
            this.getTextSearchResult(searchKeyword);
        }
        else {
            this.getFileSearchResult(searchKeyword);
        }
    }

    translationTypeOptionChange = (selectedTranslationType) => {
        this.setState({ selectedTranslationType, noTranslationTypeSelected: false });
    }

    resetSearchBox() {
        let translationTypeId = this.props.selectedApplicationId && this.props.applications.find(app => app.Id === this.props.selectedApplicationId).TranslationTypeId.toString();

        this.setState({
            loading: false,
            selectedSearchOption: null,
            searchResult: [],
            fileSearchResult: [],
            searchKeyword: "",
            noSearchOptionSelected: false,
            noTranslationTypeSelected: false,
            noSearchText: true,
            selectedTranslationType: translationTypeId,
            searchKeyword: ""
        });
    }

    onSearchInputChange(e) {
        this.setState({ searchKeyword: e.target.value });
        if (!e.target.value || e.target.value.trim().length < 3) {
            this.setState({ noSearchText: true });
        }
        else {
            this.setState({ noSearchText: false });
        }
    }

    render() {
        let loader = null;
        if (this.state.loading) {
            loader = <div className="pdf-page-loader">
                <Spin size="large" />
            </div>
        }
        const { userInfo } = this.props;

        return (
            <div className="page-header">
                {this.props.backButtonShow && (
                    <Button type="primary" className="lz-back-button" onClick={this.props.backToTranslations}>
                        <Icon type="arrow-left" /> Go Back
                    </Button>
                )}
                <div className="search-panel">
                    <InputGroup compact>
                        <Select
                            className={this.state.noSearchOptionSelected ? 'no-search-option-error' : ''}
                            showSearch
                            style={{ width: '20%' }}
                            placeholder="Search within - "
                            onChange={this.searchOptionChange}
                            value={this.state.selectedSearchOption ? this.state.selectedSearchOption : undefined}
                            defaultValue="Search within - "
                        >
                            {
                                this.props.isGroupView ?
                                    <Option value="4">Selected Group</Option>
                                    :
                                    <Option value="1">Selected section</Option>
                            }
                            <Option value="2">Selected app</Option>
                            <Option value="3">All apps</Option>
                        </Select>
                        {this.state.selectedSearchOption && this.state.selectedSearchOption == SearchOptions.AllApps &&
                            <Select
                                className={this.state.noTranslationTypeSelected ? 'no-search-option-error' : ''}
                                showSearch
                                style={{ width: '10%' }}
                                placeholder="Type"
                                onChange={value => { this.translationTypeOptionChange(value) }}
                                value={this.state.selectedTranslationType}>
                                <Option value="1">Text</Option>
                                <Option value="2">File</Option>
                            </Select>}
                        <Search
                            className={this.state.noSearchText ? 'no-search-input-error' : ''}
                            style={{ width: '70%' }}
                            placeholder="Enter Search Text"
                            onSearch={value => { this.onSearchClick(value) }}
                            onPressEnter={value => { this.onSearchClick(value) }}
                            value={this.state.searchKeyword}
                            onChange={e => { this.onSearchInputChange(e) }}
                            enterButton
                        />
                    </InputGroup>
                </div>
                {loader}
            </div>
        )
    }
}