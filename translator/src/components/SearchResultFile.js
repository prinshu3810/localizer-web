import React, { Component } from 'react';
import { Row, Col, Input, Button, Icon, Spin, Drawer, Form, Modal, Menu, Upload, Tooltip, Tabs } from 'antd';
import axios from 'axios';
import { Link } from 'react-router-dom'
import Strings from '../constants/String';
import Roles from '../constants/Constants';
import { notify } from 'react-notify-toast';
import _ from 'lodash';

const Status = { 'New': 1, 'Updated': 2, 'Published': 3 };
const Languages = { 'English': 1 };

export default class SearchResultFile extends Component {
    imagePreviewUrl = "";
    headers = {
        authorization: `Bearer ${JSON.parse(localStorage.getItem("gsuser")).token}`
    };
    constructor() {
        super();
        this.state = {
            translations: null
        }
    }

    componentDidMount() {
        let translations = this.props.searchResult && this.props.selectedLanguage.id ? this.createPdfTranslationList(this.props.searchResult, this.props.selectedLanguage.id) : [];
        this.setState({ translations });
    }

    componentWillReceiveProps(props) {
        if (props.searchResult !== this.props.searchResult) {
            let translations = this.createPdfTranslationList(props.searchResult, this.props.selectedLanguage.id)
            // let translations = this.createTranslationList(props.filteredTranslations, props.selectedLanguage.id, false);
            this.setState({ translations });
        }
    }

    createPdfTranslationList(list, langId) {
        let pdfTranslations = [];
        for (let i = 0; i < list.length; i++) {
            const pdfTranslation = {
                applicationName: list[i].applicationName,
                applicationId: list[i].applicationId,
                groupId: list[i].groupId,
                sectionId: list[i].sectionId,
                sectionName: list[i].sectionName,
                id: list[i].pdfId,
                key: list[i].key,
                statusId: list[i].pdfTranslation[0].statusId,
                englishPdfPath: list[i].pdfTranslation.filter(x => x.languageId == 1).map(item => item.path),
                englishPdfTranslationId: list[i].pdfTranslation.filter(x => x.languageId == 1).map(item => item.pdfTranslationId),
                translatedPdfPath: '',
                translatedPdfTranslationId: null,
                isLocked: list[i].isLocked
            };

            const obj = _.find(list[i].pdfTranslation, function (o) { return o.languageId === langId; });

            if (obj) {
                pdfTranslation.isVisible = obj.isVisible;
                if (this.props.selectedLanguage.id != 1) {
                    pdfTranslation.statusId = obj.statusId;
                }
                pdfTranslation.translatedPdfPath = obj.path;
                pdfTranslation.translatedPdfTranslationId = obj.pdfTranslationId;
            }
            else {
                pdfTranslation.isVisible = true;
                pdfTranslation.statusId = 1;
            }

            pdfTranslations.push(pdfTranslation);
        }
        return pdfTranslations;
    }


    onPdfStatusChange = (info, translation, isTranslated = false, isReplace = false, isAddMultiple = false) => {
        if (info.file.status !== "done" && info.file.status !== "error") {
            if (!isTranslated && translation.englishPdfPath.find(x => x.split("/").pop() === info.file.name)) {
                Modal.info({
                    title: "File with same name already exists.",
                    content: ''

                });
                return false;
            }
            const pdfTranslationObj = {
                PdfTranslationId: isTranslated ? translation.translatedPdfTranslationId : translation.englishPdfTranslationId[0],
                LanguageId: isTranslated ? this.props.selectedLanguage.id : Languages.English,
                Language: isTranslated ? this.props.selectedLanguage.name : "English",
                Key: translation.key,
                SectionId: this.props.selectedSection,
                StatusId: 2,
                Path: "",
                PdfId: translation.id,
                isAddMultiple: isAddMultiple,
            };
            const formData = new FormData();
            if (isTranslated) {
                formData.append(info.file.name, info.file);
            }
            else {
                formData.append(info.file.name, info.file.originFileObj);
            }
            formData.append("section", this.props.selectedApp
                .Section.find(x => x.Id == this.props.selectedSection).Name.trim());
            formData.append("application", this.props.selectedApp.Name.trim());
            formData.append("sectionId", this.props.selectedSection);
            formData.append("pdfTranslation", JSON.stringify(pdfTranslationObj));
            this.setState({ loaded: false });
            axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'PdfTranslation/AddPdfTranslation', formData, { headers: this.headers })
                .then((res) => {
                    let pdf = res.data;
                    let pdfTranslation = res.data.pdfTranslation[0];
                    let translations = _.cloneDeep(this.state.translations);
                    let index = translations.findIndex(x => x.key == pdf.key);
                    translations[index].statusId = 2;
                    if (isTranslated) {
                        translations[index].translatedPdfPath = pdfTranslation.path;
                        translations[index].translatedPdfTranslationId = pdfTranslation.id;
                    }
                    else {
                        translations[index].englishPdfPath = [pdfTranslation.path]
                        translations[index].englishPdfTranslationId = [pdfTranslation.id];
                        if (translation.englishPdfPath && translation.englishPdfPath[0]) {
                            translation.englishPdfPath.push(pdfTranslation.path);
                            translation.englishPdfTranslationId.push(pdfTranslation.id);
                            translations[index].englishPdfPath = translation.englishPdfPath;
                            translations[index].englishPdfTranslationId = translation.englishPdfTranslationId;
                        }


                    }
                    this.setState({ translations: translations, loaded: true });
                })
                .catch(err => {
                    console.log(err);
                    this.props.errorHandler(err);
                    this.setState({ loaded: true });
                });
        }
    }

    renderTranslations = (translations) => {
        const keyDisabled = this.props.userInfo && this.props.userInfo.role === Roles.Developer ? 'disabled' : 'disabled';
        return translations.map((t, index) => {
            let statusClass = '';

            switch (t.statusId) {
                case 1:
                    statusClass = 'new-pdf-translation';
                    break;

                case 2:
                    statusClass = 'updated-pdf-translation';
                    break;

                case 3:
                    statusClass = 'published-pdf-translation';
                    break;

                default:
                    break;
            }

            if (!t.isVisible) {
                statusClass = 'ignored-pdf-translation';
            }

            let checkEnglishRole = (this.props.selectedLanguage.id == 1 && this.props.userInfo.userLanguage.findIndex(x => x.languageId == 1) > -1)
            let checkTranslatorRole = this.props.userInfo.role == 1 || this.props.userInfo.userLanguage.findIndex(x => x.languageId == this.props.selectedLanguage.id) > -1
            let translatedPdfName = t.translatedPdfPath ? t.translatedPdfPath.split("/").pop().split('.').slice(0, -1).join('.') : '';
            let trimTranslatedPdfName = translatedPdfName.length > 25 ? translatedPdfName.slice(0, 25) + "..." : translatedPdfName;
            const deleteIcon = this.props.userInfo.role === Roles.Developer && !this.props.selectedApp.IsLocked ? (
                <Col span={1} title="Delete translation">
                    <Icon type="delete" theme="twoTone" onClick={this.deleteTranslation} data-id={t.id} className="translation-row-icon" />
                </Col>
            ) : '';
            const addIcon = (this.props.userInfo.role === Roles.Developer || checkEnglishRole) && !this.props.selectedApp.IsLocked ? (
                <Upload
                    fileList={[]}
                    listType='text'
                    className='ant-upload-list-item'
                    onChange={(e) => this.onPdfStatusChange(e, t, false, false, true)}>
                    {/* <Col span={1} title="Add File"> */}
                    <Icon type="plus-circle" theme="twoTone" className="translation-row-icon" />
                    {/* </Col> */}
                </Upload>
            ) : '';
            let multipleEnglishFiles = t.englishPdfPath && t.englishPdfPath.map((ePdfPath, index) => {
                let englishPdfName = ePdfPath.split("/").pop();
                let trimEnglishPdfName = englishPdfName.length > 25 ? englishPdfName.slice(0, 25) + "..." : englishPdfName;
                return (<React.Fragment>

                    <div className="uploaded-pdf">
                        <div className={statusClass}>
                            <div className="ant-upload-list ant-upload-list-text">
                                <div className="ant-upload-list-item ant-upload-list-item-undefined">
                                    <div className="ant-upload-list-item-info">
                                        <Icon type="paper-clip" theme="outlined" />
                                        <i className="anticon anticon-paper-clip"></i>
                                        <a rel="noopener noreferrer"
                                            className="ant-upload-list-item-name" title={englishPdfName}
                                            onClick={() => this.onPdfDownload(ePdfPath)}
                                            href="javascript:void(0);" >{trimEnglishPdfName}</a>
                                    </div>
                                    {/* {this.props.userInfo.role == 1 && checkEnglishRole && editEnglishPdfIcon} */}
                                    {(this.props.userInfo.role == 1 || checkEnglishRole) && <Icon type="close" theme="outlined" onClick={() => this.deletePdfConfirm(t.englishPdfTranslationId[index], ePdfPath, false, t.englishPdfPath.length > 1)} />}
                                </div>
                            </div>
                        </div>
                    </div>

                </React.Fragment >)
            });

            var englishPdfUploadIcon = t.englishPdfPath && t.englishPdfPath[0] ? <Col span={7} type="flex" className="wrapper-upload-multiple-pdf"> {multipleEnglishFiles}</Col> :
                <Col span={7} className="uploaded-pdf">
                    <div className={statusClass}>
                        {this.props.userInfo.role == 1 || checkEnglishRole ?
                            <Upload
                                fileList={[]}
                                className='upload-list-inline'
                                listType='text'
                                onChange={(e) => this.onPdfStatusChange(e, t)}
                            >
                                <Button type="file"><Icon type="upload" />Upload</Button>
                            </Upload> : <div style={{ 'line-height': '30px' }}>No File Exists</div>}</div></Col>

            var translatedPdfUploadIcon = t.translatedPdfPath ?
                <Col span={7} className="uploaded-pdf">
                    <div className={statusClass}>
                        <div className="ant-upload-list ant-upload-list-text">
                            <div className="ant-upload-list-item ant-upload-list-item-undefined">
                                <div className="ant-upload-list-item-info">
                                    <span>
                                        <Icon type="paper-clip" theme="outlined" />
                                        <a rel="noopener noreferrer"
                                            className="ant-upload-list-item-name" title={translatedPdfName}
                                            onClick={() => this.onPdfDownload(t.translatedPdfPath)}
                                            href="javascript:void(0);" >{trimTranslatedPdfName}</a>
                                    </span>
                                </div>
                                {checkTranslatorRole && <Icon type="close" theme="outlined" onClick={() => this.deletePdfConfirm(t.translatedPdfTranslationId, t.translatedPdfPath, true)} />}
                            </div>
                        </div>
                    </div>
                </Col>
                :
                <Col span={7} className="uploaded-pdf">
                    <div className={statusClass}>
                        {checkTranslatorRole ? <Upload
                            beforeUpload={() => false}
                            fileList={[]}
                            className='upload-list-inline'
                            listType='text'
                            onChange={(e) => this.onPdfStatusChange(e, t, true)}
                        >
                            <Button><Icon type="upload" />Upload</Button>
                        </Upload> : <div style={{ 'line-height': '30px' }}>No File Exists</div>}
                    </div>
                </Col>


            var translatedSection = this.props.selectedLanguage.id !== 1 ? <React.Fragment>
                {translatedPdfUploadIcon}
            </React.Fragment> : null;
            return (

                <Row type="flex" key={t.id}>
                    <Col span={1}>
                    </Col>
                    <React.Fragment>
                        <Col span={4} title="Key" className="key-section">
                            <Input style={{ 'height': '100%' }} size="default" className={statusClass} placeholder="large size" disabled={keyDisabled} data-id={t.textId} data-name="key" value={t.key} />
                        </Col>
                        {englishPdfUploadIcon}
                        {translatedSection}
                    </React.Fragment>

                    <Col span={2} >
                        <Col span={12} title="Add File">
                            {addIcon}
                        </Col>
                        <Col span={12} >
                            {deleteIcon}
                        </Col>
                    </Col>
                </Row>

            );
        });
    }

    onPdfDownload = (path) => {
        this.setState({ loaded: false });
        axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}PdfTranslation/DownloadPdf?path=${encodeURIComponent(path)}`, { headers: this.headers })
            .then(res => {
                var element = document.createElement('a');
                element.setAttribute('href', res.data);
                element.setAttribute('download', path.split("/").pop() + ".pdf");

                element.style.display = 'none';
                document.body.appendChild(element);

                element.click();
                document.body.removeChild(element);
                this.setState({ loaded: true });
            });
    }

    deletePdfConfirm = (pdfTranslationId, path, isTranslated = false, isMultiple) => {
        Modal.confirm({
            title: "Confirm file delete",
            content: '',
            okText: Strings.Texts.Yes,
            cancelText: Strings.Texts.No,
            onOk: () => {
                this.onPdfRemove(pdfTranslationId, path, isTranslated, isMultiple);
            },
            onCancel() {

            }
        });

    }


    onPdfRemove = (pdfTranslationId, path, isTranslated = false, isMultiple = false) => {
        this.setState({ loaded: false });
        axios
            .delete(`${Strings.URL.GcLocalizerServiceBaseUrl}PdfTranslation/DeletePdf?pdfTranslationId=${pdfTranslationId}&path=${path}&isMultiple=${isMultiple}`, { headers: this.headers })
            .then(res => {
                let translations = _.cloneDeep(this.state.translations);
                let index = -1;
                if (isTranslated) {
                    index = translations.findIndex(x => x.translatedPdfTranslationId == pdfTranslationId)
                    translations[index].translatedPdfPath = null;
                    translations[index].statusId = 1;

                }
                else {
                    index = translations.findIndex(x => x.englishPdfTranslationId.find(id => id == pdfTranslationId));
                    if (isMultiple) {
                        translations[index].englishPdfPath = translations[index].englishPdfPath.filter(x => x != path);
                    }
                    else {
                        translations[index].englishPdfTranslationId = [];
                        translations[index].englishPdfPath = [];
                        translations[index].statusId = 1;
                    }

                }
                this.setState({ translations: translations, loaded: true });
            })
            .catch(reason => {
                console.log(reason);
                this.props.errorHandler(reason);
            });
    }

    deleteTranslation = (event) => {
        const target = event.currentTarget;
        const id = parseInt(target.getAttribute("data-id"));

        Modal.confirm({
            title: Strings.Texts.TranslationDeleteConfirmation,
            content: '',
            okText: Strings.Texts.Yes,
            cancelText: Strings.Texts.No,
            onOk: () => {
                this.setState({ loaded: false });
                axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}PdfTranslation/DeletePdfTranslation?pdfId=${id}`, { headers: this.headers })
                    .then(res => {
                        this.setState({ loaded: true });
                        this.showToast(Strings.Texts.Success, Strings.Texts.TranslationDeleteSucess);
                        this.props.updateSearchResult();
                    }).catch(error => {
                        this.setState({ loaded: true });
                        this.props.errorHandler(error);
                    });
            },
            onCancel() {

            }
        });
    }

    generateDefaultSectionId = (applicationId) => {
        let sections = [];
        const applications = this.props.applications;
        for (var i = 0; i < applications.length; i++) {
            if (applications[i].Id == applicationId) {
                sections = applications[i].Section;
                break;
            }
        }
        if (sections.length) {
            if (!sections[0].Id.IsVisible) {
                const visibleSection = sections.find(s => s.IsVisible);

                if (visibleSection) {
                    return visibleSection.Id;
                }
            } else {
                return sections[0].Id;
            }
        } else {
            return 0;
        }
    }

    generateDefaultGroupId = (applicationId) => {
        let groups = [];
        const applications = this.props.applications;
        for (var i = 0; i < applications.length; i++) {
            if (applications[i].Id == applicationId) {
                groups = applications[i].Group.sort((a, b) => a.Sno - b.Sno);;
                break;
            }
        }

        if (groups.length) {
            if (this.props.userInfo.role === Roles.Translator || this.props.isGroupView == true) {
                const visibleGroups = groups.filter(grp => grp.IsVisible == true);
                if (visibleGroups.length) {
                    return visibleGroups[0].Id;
                } else {
                    return 0;
                }
            } else {
                return groups[0].Id;
            }
        } else {
            return 0;
        }
    }


    showToast = (type, message) => {
        let color = "";
        if (type == "danger") {
            color = { background: '#ee3647', text: "#FFFFFF" };
            notify.show(message, "custom", 2500, color);
        }
        else if (type == "info") {
            color = { background: '#0E1717', text: "#FFFFFF" };
            notify.show(message, "custom", 2000, color);
        }
        else if (type == "success") {
            color = { background: '#008000', text: "#FFFFFF" };
            notify.show(message, "custom", 5000, color);
        }
    }


    displaySearchResult() {
        let searchResult = _.cloneDeep(this.state.translations);

        if (!this.props.includeIgnored && this.props.userInfo.role === Roles.Translator) {
            searchResult = searchResult.filter(item => item.isVisible);
        }

        let groupedByApp = _.groupBy(searchResult, x => x.applicationName);
        let groupedByAppArr = Array.from(Object.keys(groupedByApp), k => groupedByApp[k])
        let groupedSearchResult = groupedByAppArr.map(item => Array.from(Object.keys(_.groupBy(item, x => x.sectionName)), k => _.groupBy(item, x => x.sectionName)[k]));

        return groupedSearchResult.map(resultGroupedByApp => {
            let appurl = "";
            let appName = resultGroupedByApp[0][0].applicationName;
            const applicationId = resultGroupedByApp[0][0].applicationId;
            if (this.props.isGroupView) {
                const groupId = this.generateDefaultGroupId(applicationId);
                appurl = `/Translations/?applicationId=${applicationId}&groupId=${groupId}`
            } else {
                const sectionId = this.generateDefaultSectionId(applicationId);
                appurl = `/Translations/?applicationId=${applicationId}&sectionId=${sectionId}`
            }

            return <div>
                <Row className="app-name-section">
                    <Tabs defaultActiveKey="1">
                        <Tabs.TabPane key={applicationId + appName} tab={<Link to={appurl}>{appName}</Link>}></Tabs.TabPane>
                    </Tabs>
                </Row>
                {
                    resultGroupedByApp.map(resultGroupedBySection => {
                        let url = "";
                        if (this.props.isGroupView) {
                            url = `/Translations/?applicationId=${applicationId}&groupId=${resultGroupedBySection[0].groupId}`
                        } else {
                            url = `/Translations/?applicationId=${applicationId}&sectionId=${resultGroupedBySection[0].sectionId}`
                        }

                        return (<Row>
                            <Link to={url} className="section-title" key={resultGroupedBySection[0].sectionName}>{resultGroupedBySection[0].sectionName}</Link>
                            <div id="text-area">
                                <Row>
                                    {resultGroupedBySection[0].isLocked && this.renderMaskContent()}
                                    {this.renderTranslations(resultGroupedBySection)}
                                </Row>
                            </div>
                        </Row>)
                        // })
                    })
                }
            </div>
        })
    }
    renderMaskContent = () => {
        let width = '';

        if (this.props.selectedLanguage.id === 1) {
            width = '46%';
        } else {
            width = '75.3%';
        }

        return (
            <Tooltip placement="right" title="Application locked for publish">
                <div style={{ width }} className="mask-section"></div>
            </Tooltip>
        );
    }

    render() {
        let loader = null;
        if (this.state.loaded == false) {
            loader = <div className="pdf-page-loader">
                <Spin size="large" />
            </div>
        }

        return <React.Fragment>
            {loader}
            {this.state.translations && this.state.translations.length > 0 && <div className="height-100">
                <Row>
                    <Col span={18}>
                        <div id="text-area">
                            {this.state.translations && this.displaySearchResult()}
                        </div>
                    </Col>
                </Row>
            </div>}
            {this.state.translations && this.state.translations.length == 0 && <div className="no-result-section" ><Icon type="warning" /> No matching results found.</div>}
        </React.Fragment>
    }
}


