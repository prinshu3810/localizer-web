import React, { PureComponent } from 'react';
import { Row, Col, Input, Button, Icon, Spin, Modal, Popover, Tabs, Tooltip } from 'antd';
import Roles, { Languages } from '../constants/Constants';
import _ from 'lodash';
import { Link } from 'react-router-dom'
import { notify } from 'react-notify-toast';
import Strings from '../constants/String';
import axios from 'axios';
import TranslationsBySection from './TranslationsBySection';

const Status = { 'New': 1, 'Updated': 2, 'Published': 3 };
const TabPane = Tabs.TabPane;

const getAllTranslationMatches = (regex, value) => {
    const matches = [];
    let result = true;

    while (result) {
        const regexResult = regex.exec(value);

        if (regexResult && regexResult[0]) {
            matches.push(regexResult[0]);
        } else {
            result = false;
        }
    }

    return matches;
}

const isTranslationChangeValid = (englishTranslation, translation) => {
    const regex = new RegExp(/(\{{1,})(.*?)(\}{1,})/g);

    // If the english translation does not contain placeholders, then there is no need to perform matches.
    if (!regex.test(englishTranslation)) {
        return true;
    }

    regex.lastIndex = 0;

    const englishMatches = getAllTranslationMatches(regex, englishTranslation);
    const translationMatches = getAllTranslationMatches(regex, translation);
    return _.difference(englishMatches, translationMatches).length === 0;
}
export default class SearchResultText extends PureComponent {
    selectedTranslation;
    englishDisabled = false;
    headers = {
        authorization: `Bearer ${JSON.parse(localStorage.getItem("gsuser")).token}`
    };
    constructor() {
        super();
        this.state = {
            loaded: true,
            translations: null,
            showTranslationModal: false
        }
    }

    componentDidMount() {
        let translations = this.props.searchResult && this.props.selectedLanguage.id ? this.createTranslationList(this.props.searchResult, this.props.selectedLanguage.id) : [];
        if (this.props.userInfo && this.props.userInfo.role === Roles.Translator) {
            this.englishDisabled = true;
            if (this.props.selectedLanguage && this.props.selectedLanguage.id == 1 && this.props.userLanguages && this.props.userLanguages.findIndex(x => x.languageId == 1) > -1) {
                this.englishDisabled = false;
            }
        } else if (this.props.selectedLanguage && this.props.selectedLanguage.id !== 1) {
            this.englishDisabled = true;
        }
        this.setState({ translations });
    }

    componentWillReceiveProps(props) {
        if (props.searchResult !== this.props.searchResult || props.imageList !== this.props.imageList) {
            if (props.userInfo && props.userInfo.role === Roles.Translator) {
                this.englishDisabled = true;
                if (props.selectedLanguage && props.selectedLanguage.id == 1 && props.userLanguages && props.userLanguages.findIndex(x => x.languageId == 1) > -1) {
                    this.englishDisabled = false;
                }
            } else if (props.selectedLanguage && props.selectedLanguage.id !== 1) {
                this.englishDisabled = true;
            }

            let translations = this.createTranslationList(props.searchResult, props.selectedLanguage.id);
            this.setState({ translations });
        }
    }

    createTranslationList = (list, langId) => {
        let translations = [];
        for (let i = 0; i < list.length; i++) {
            // Get the english translation from the list of translations.
            const english = list[i].translations.find(o => o.languageId === 1);
            const englishTranslation = english ? english.translation : '';

            let translation = {
                applicationId: list[i].applicationId,
                sectionId: list[i].sectionId,
                groupId: list[i].groupId,
                applicationName: list[i].applicationName,
                sectionName: list[i].sectionName,
                key: list[i].key,
                textId: list[i].textId,
                statusId: 0,
                translation: '',
                originalTranslation: '',
                englishTranslation,
                originalEnglishTranslation: englishTranslation,
                translationId: 0,
                shouldRenderSaveIcon: false,
                isLocked: list[i].isLocked

            };

            if (list[i].translations.length > 1) {
                const obj = _.find(list[i].translations, function (o) { return o.languageId === langId; });
                translation.isVisible = obj.isVisible;

                if (obj) {
                    translation.id = obj.translationId;
                    translation.statusId = obj.statusId;
                    translation.translation = obj.translation;
                    translation.originalTranslation = obj.translation;
                    translation.translationId = obj.translationId;
                    translation.sno = obj.sno;
                    translation.previousTranslation = obj.previousTranslation;
                    translation.previousEnglish = english.previousTranslation
                }
            }
            else {
                translation.sno = list[i].translations[0].sno;
                translation.id = list[i].translations[0].translationId;
                translation.isVisible = list[i].translations[0].isVisible;

                if (langId != 1) {
                    translation.statusId = Status.New;
                    translation.translation = '';
                    translation.originalTranslation = '';
                    translation.translationId = null;
                    translation.previousEnglish = list[i].translations[0].previousTranslation || '';
                }
                else {
                    translation.statusId = list[i].translations[0].statusId;
                    translation.translation = '';
                    translation.originalTranslation = '';
                    translation.translationId = list[i].translations[0].translationId;
                    translation.previousTranslation = list[i].translations[0].previousTranslation || '';
                }
            }

            let statusClass = '';

            switch (translation.statusId) {
                case 1:
                    statusClass = 'new-translation';
                    break;

                case 2:
                    statusClass = 'updated-translation';
                    break;

                case 3:
                    statusClass = 'published-translation';
                    break;

                default:
                    break;
            }

            if (!translation.isVisible) {
                statusClass = 'ignored-translation';
            }

            let englishPlaceHolder = translation.originalEnglishTranslation ? "English translation" : Strings.Texts.TranslationPlaceholder;
            let historyIconContent;
            let popoverTitle;

            if (translation.previousTranslation && this.props.selectedLanguage && this.props.selectedLanguage.id !== 1) {
                historyIconContent = (
                    <Tabs defaultActiveKey="1">
                        <Tabs.TabPane tab="Previous Translation" key="1">
                            {translation.previousTranslation}
                        </Tabs.TabPane>
                        <Tabs.TabPane tab="Previous English" key="2">
                            {translation.previousEnglish}
                        </Tabs.TabPane>
                    </Tabs>
                );
            } else {
                popoverTitle = 'Previous Translation';
                historyIconContent = translation.previousTranslation;
            }

            let historyIcon;

            if (translation.previousTranslation) {
                historyIcon = (<Col span={1}>
                    <div className="">
                        <Popover overlayStyle={{ maxWidth: '500px', maxHeight: '150px' }} title={popoverTitle} content={historyIconContent} autoAdjustOverflow={false}>
                            <Icon type="clock-circle" theme="twoTone" className="translation-row-icon" />
                        </Popover>
                    </div>
                </Col>);
            }
            let translationProps = this.createHistoryIcon(translation)
            let self = this;
            translation = this.createRenderModal(self, translation, translationProps.historyIcon, translationProps.statusClass, translationProps.englishPlaceHolder);
            translations.push(translation);
        }
        return translations;
    }

    createHistoryIcon(translation) {
        let historyIcon = '';
        let historyIconContent;
        let popoverTitle = '';
        let statusClass = null;
        let englishPlaceHolder = null;
        statusClass = translation.statusId == 1 ? 'new-translation' : (translation.statusId == 2 ? 'updated-translation' : 'published-translation');
        englishPlaceHolder = translation.originalEnglishTranslation ? "English translation" : Strings.Texts.TranslationPlaceholder;
        if (translation.previousTranslation && this.props.selectedLanguage && this.props.selectedLanguage.id !== 1) {
            historyIconContent = (
                <Tabs defaultActiveKey="1">
                    <Tabs.TabPane tab="Previous Translation" key="1">
                        {translation.previousTranslation}
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Previous English" key="2">
                        {translation.previousEnglish}
                    </Tabs.TabPane>
                </Tabs>
            );
        } else {
            popoverTitle = 'Previous Translation'
            historyIconContent = translation.previousTranslation;
        }

        if (translation.previousTranslation) {
            historyIcon = (<Col span={1}>
                <div className="">
                    <Popover overlayStyle={{ maxWidth: '500px', maxHeight: '150px' }} title={popoverTitle} content={historyIconContent} autoAdjustOverflow={false}>
                        <Icon type="clock-circle" theme="twoTone" className="translation-row-icon" />
                    </Popover>
                </div>
            </Col>);
        }
        return { statusClass, englishPlaceHolder, historyIcon }
    }

    createRenderModal(self, translation, historyIcon, statusClass, englishPlaceHolder) {
        translation.renderModal = function () {
            return <Modal
                title="Translation"
                visible={self.state.showTranslationModal}
                onCancel={() => { self.onTranslationModalCancel(this) }}
                footer={[
                    <Button key="cancel" type="danger" onClick={() => { self.onTranslationModalCancel(this) }} >Cancel</Button>,
                    <Button
                        key="submit" type="primary" onClick={(e) => self.handleSaveClick(e, this)}
                        data-id={this.textId} data-translationid={this.translationId}
                        data-statusid={this.statusId} disabled={!this.shouldRenderSaveIcon}>Save</Button>,
                    historyIcon
                ]}
            >
                <div>
                    {
                        self.props.selectedLanguage.id !== 1 ?
                            (<React.Fragment>
                                <div id="modal-english-translation">
                                    <Input.TextArea
                                        autosize={{ minRows: 3 }}
                                        size="default"
                                        className={statusClass}
                                        placeholder={Strings.Texts.TranslationPlaceholder}
                                        onChange={self.handleTranslationInputChange}
                                        disabled={self.englishDisabled}
                                        data-id={this.textId}
                                        data-name="englishTranslation"
                                        value={this.englishTranslation} />
                                </div>
                                <div className="copy-modal">
                                    <Tooltip placement="right" title="Copy same as English">
                                        <Button icon="arrow-down" type="primary" data-id={this.textId} onClick={(e) => { self.onModalCopyClick(this, e) }} />
                                    </Tooltip>
                                </div>
                            </React.Fragment>)
                            : ''
                    }
                    <div id="modal-translation">
                        <Input.TextArea
                            autosize={{ minRows: 3 }}
                            autoFocus
                            className={statusClass}
                            data-id={this.textId}
                            onChange={self.handleTranslationInputChange}
                            data-name={self.props.selectedLanguage.id === 1 ? "englishTranslation" : "translation"}
                            placeholder={self.props.selectedLanguage.id === 1 ? englishPlaceHolder : "Translation"}
                            value={self.props.selectedLanguage.id === 1 ? this.englishTranslation : this.translation}
                        />
                    </div>
                </div>
            </Modal>


        }
        return translation;
    }

    onImagePreview(thumbUrl) {
        let url = window.location.origin + "/Preview?prurl=" + thumbUrl;
        window.open(url);
    }

    generateDefaultSectionId = (applicationId) => {
        let sections = [];
        const applications = this.props.applications;
        for (var i = 0; i < applications.length; i++) {
            if (applications[i].Id == applicationId) {
                sections = applications[i].Section;
                break;
            }
        }
        if (sections.length) {
            if (!sections[0].Id.IsVisible) {
                const visibleSection = sections.find(s => s.IsVisible);

                if (visibleSection) {
                    return visibleSection.Id;
                }
            } else {
                return sections[0].Id;
            }
        } else {
            return 0;
        }
    }

    generateDefaultGroupId = (applicationId) => {
        let groups = [];
        const applications = this.props.applications;
        for (var i = 0; i < applications.length; i++) {
            if (applications[i].Id == applicationId) {
                groups = applications[i].Group.sort((a, b) => a.Sno - b.Sno);;
                break;
            }
        }

        if (groups.length) {
            if (this.props.userInfo.role === Roles.Translator || this.props.isGroupView == true) {
                const visibleGroups = groups.filter(grp => grp.IsVisible == true);
                if (visibleGroups.length) {
                    return visibleGroups[0].Id;
                } else {
                    return 0;
                }
            } else {
                return groups[0].Id;
            }
        } else {
            return 0;
        }
    }

    displaySearchResult() {
        let searchResult = _.cloneDeep(this.state.translations);
        if (!this.props.includeIgnored && this.props.userInfo.role === Roles.Translator) {
            searchResult = searchResult.filter(item => item.isVisible);
        }

        let groupedByApp = _.groupBy(searchResult, x => x.applicationName);
        let groupedByAppArr = Array.from(Object.keys(groupedByApp), k => groupedByApp[k])
        let groupedSearchResult = groupedByAppArr.map(item => Array.from(Object.keys(_.groupBy(item, x => x.sectionName)), k => _.groupBy(item, x => x.sectionName)[k]));
        let groupedImagesBySections = _.groupBy(this.props.imageList, x => x.sectionId);

        return groupedSearchResult.map(resultGroupedByApp => {
            let appurl = "";
            let appName = resultGroupedByApp[0][0].applicationName;
            const applicationId = resultGroupedByApp[0][0].applicationId;
            if (this.props.isGroupView) {
                const groupId = this.generateDefaultGroupId(applicationId);
                appurl = `/Translations/?applicationId=${applicationId}&groupId=${groupId}`
            } else {
                const sectionId = this.generateDefaultSectionId(applicationId);
                appurl = `/Translations/?applicationId=${applicationId}&sectionId=${sectionId}`
            }

            return <React.Fragment>
                <Row className="app-name-section">
                    <Tabs defaultActiveKey="1">
                        <Tabs.TabPane key={applicationId} tab={<Link to={appurl}>{appName}</Link>}></Tabs.TabPane>
                    </Tabs>
                </Row>
                {
                    resultGroupedByApp.map(resultGroupedBySection => {
                        let url = "";
                        if (this.props.isGroupView) {
                            url = `/Translations/?applicationId=${applicationId}&groupId=${resultGroupedBySection[0].groupId}`
                        } else {
                            url = `/Translations/?applicationId=${applicationId}&sectionId=${resultGroupedBySection[0].sectionId}`
                        }
                        let imagesSection = groupedImagesBySections[resultGroupedBySection[0].sectionId] &&
                            groupedImagesBySections[resultGroupedBySection[0].sectionId].map(item => {

                                return <Col span={2}>
                                    <a onClick={() => { this.onImagePreview(item.path) }} target="_blank">
                                        <img src={item.path} width="50px" height="30px" />
                                    </a>
                                    <div>
                                        <a onClick={() => { this.onImagePreview(item.path) }} target="_blank" title={item.path.split('?')[0].split("/").pop()}>
                                            {item.path.split('?')[0].split("/").pop().length > 10 ? item.path.split('?')[0].split("/").pop().slice(0, 10) + "..." : item.path.split('?')[0].split("/").pop()}
                                        </a>
                                    </div>
                                </Col>
                            })
                        return (<Row key={resultGroupedBySection[0].sectionName}>
                            <Link to={url} className="section-title" style={{ marginBottom: "10px", margintop: "10px" }}>{resultGroupedBySection[0].sectionName}</Link>
                            <div className="images-preview-section">
                                {imagesSection}
                            </div>
                            <Col span={24}>
                                <div id="text-area">
                                    <TranslationsBySection
                                        translationsofSection={resultGroupedBySection}
                                        handleSaveClick={this.handleSaveClick}
                                        handleTranslationInputChange={this.handleTranslationInputChange}
                                        toggleTranslationModal={this.toggleTranslationModal}
                                        selectedLanguage={this.props.selectedLanguage}
                                        userInfo={this.props.userInfo}
                                        userLanguages={this.props.userLanguages}
                                        selectedApp={this.props.selectedApp}
                                        onTranslationCopyClick={this.onTranslationCopyClick}
                                        deleteTranslation={this.deleteTranslation}
                                    />
                                </div>
                            </Col>
                        </Row>)

                    })
                }
            </React.Fragment>
        })
    }

    getExistingImage = () => {
        axios
            .get(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/Images?sectionId=${this.props.selectedSection}`, { headers: this.headers })
            .then(res => {
                let imageList = res.data;
                if (!imageList) {
                    this.setState({ sectionFileListMap: null });
                    return;
                }
                let sectionFileListMap = {
                    [this.props.selectedSection]: imageList.map(item => {
                        let obj = {
                            uid: item.id,
                            url: item.path,
                            thumbUrl: item.path,
                            name: item.path.split('?')[0].split("/").pop(),
                            response: {
                                imageId: (item.id)
                            }
                        }
                        return obj;
                    })
                };
                this.setState({
                    sectionFileListMap
                });
            })
            .catch(reason => {
                console.log(reason);
                this.props.errorHandler(reason);
            });
    }

    toggleTranslationModal = (event) => {
        const textId = event.target.getAttribute('data-id');

        if (textId) {
            this.selectedTranslation = _.cloneDeep(this.state.translations.find(translation => translation.textId == textId)); // deep clone to avoid mutation of state.
        } else {
            this.selectedTranslation = null;
        }

        this.setState(prevState => ({ showTranslationModal: !prevState.showTranslationModal }));
    }

    handleTranslationInputChange = (event) => {

        const name = event.target.getAttribute('data-name');
        const id = parseInt(event.target.getAttribute('data-id'));
        const translations = _.cloneDeep(this.state.translations); // deep clone to avoid direct mutation of current state.
        const translation = translations.find(translation => translation.textId === id);

        /* For a translator, save icon should be rendered if: 
           
           1. It is not equal to its previous value. 
           2. Its english translation is changed by the developer. */

        if (name == "englishTranslation") {
            translation.shouldRenderSaveIcon = translation.originalEnglishTranslation !== event.target.value;
            translation.englishTranslation = event.target.value;


            if (this.selectedTranslation) {
                this.selectedTranslation.englishTranslation = event.target.value;
                this.selectedTranslation.shouldRenderSaveIcon = translation.shouldRenderSaveIcon;
            }
        }
        else {

            translation.shouldRenderSaveIcon = translation.originalTranslation !== event.target.value
            translation[name] = event.target.value;

            if (this.selectedTranslation) {
                this.selectedTranslation[name] = translation[name];
                this.selectedTranslation.shouldRenderSaveIcon = translation.shouldRenderSaveIcon;
            }
        }

        this.setState({ translations });
    }

    onTranslationCopyClick = (event) => {
        const textId = event.target.getAttribute('data-id');
        const translations = _.cloneDeep(this.state.translations); // deep clone to avoid direct mutation of state.
        const translation = translations.find(trans => trans.textId == textId);
        translation.translation = translation.englishTranslation
        translation.shouldRenderSaveIcon = !!translation.translation && translation.translation !== translation.originalTranslation;
        this.setState({ translations });
    }


    deleteTranslation = (event) => {
        const target = event.currentTarget;
        const id = parseInt(target.getAttribute("data-id"));

        Modal.confirm({
            title: Strings.Texts.TranslationDeleteConfirmation,
            content: '',
            okText: Strings.Texts.Yes,
            cancelText: Strings.Texts.No,
            onOk: () => {
                this.setState({ loaded: false });
                axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/DeleteTranslation?textId=${id}`, { headers: this.headers })
                    .then(res => {
                        this.setState({ loaded: true });
                        this.showToast(Strings.Texts.Success, Strings.Texts.TranslationDeleteSucess);
                        this.props.updateSearchResult();
                    }).catch(error => {
                        this.setState({ loaded: true });
                        this.props.errorHandler(error);
                    });
            },
            onCancel() {

            }
        });
    }

    showToast = (type, message) => {
        let color = "";
        if (type === "danger") {
            color = { background: '#ee3647', text: "#FFFFFF" };
            notify.show(message, "custom", 2500, color);
        }
        else if (type === "info") {
            color = { background: '#0E1717', text: "#FFFFFF" };
            notify.show(message, "custom", 2000, color);
        }
        else if (type === "success") {
            color = { background: '#008000', text: "#FFFFFF" };
            notify.show(message, "custom", 5000, color);
        }
    }

    onTranslationModalCancel = (translation) => {
        const translations = _.cloneDeep(this.state.translations); // deep clone to avoid mutation of state.
        translation.translation = translation.originalTranslation;
        translation.englishTranslation = translation.originalEnglishTranslation;
        translation.shouldRenderSaveIcon = false;
        const currentTranslation = translations.find(t => t.textId === translation.textId);
        currentTranslation.translation = currentTranslation.originalTranslation;
        currentTranslation.englishTranslation = currentTranslation.originalEnglishTranslation;
        currentTranslation.shouldRenderSaveIcon = false;

        this.setState(prevState => ({
            showTranslationModal: !prevState.showTranslationModal,
            translations: translations
        }));
    }

    saveTranslationChanges = (obj) => {
        this.setState({ loaded: false });
        return axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/SaveTranslation', obj, { headers: this.headers })
            .then((response) => {
                this.props.updateSearchResult();
                this.setState({ loaded: true });
                this.showToast(Strings.Texts.Success, Strings.Texts.SavedSuccess);
                return true; // on success;
            }).catch(error => {
                this.setState({ loaded: true });
                this.props.errorHandler(error);
                return false; // on failure;
            });
    }


    handleSaveClick = (event, modalTranslation) => {
        let target = event.currentTarget;
        let id = parseInt(target.getAttribute("data-id"));
        let temp = _.cloneDeep(this.state.translations);
        let translation = temp.find(o => o.textId === id);

        if (this.props.selectedLanguage.id !== 1 && translation && !isTranslationChangeValid(translation.englishTranslation, translation.translation)) {

            if (this.state.showTranslationModal) {
                this.setState({ showTranslationModal: false });
            }

            this.showToast(Strings.Texts.Danger, 'Curly brackets do not match!');
            event.preventDefault();
            event.stopPropagation();
            return false;
        }

        translation.shouldRenderSaveIcon = false;

        var obj = {
            RoleId: this.props.userInfo.role,
            TextId: translation.textId,
            TranslationId: translation.translationId,
            LanguageId: this.props.selectedLanguage.id,
            SectionId: translation.sectionId,
            Key: translation.key,
            StatusId: translation.statusId,
            Translation: translation.translation
        }

        if (this.props.selectedLanguage.id == 1) {
            obj.Translation = translation.englishTranslation;
        }
        if (translation.key.trim().length != 0) {
            var proceedWithSave = true;
            if (obj.Translation.trim().length == 0 && obj.LanguageId === Languages.English) {
                Modal.confirm({
                    title: 'The translation for the key is empty. Do you still want to proceed?',
                    content: '',
                    cancelText: 'No',
                    onOk: () => {
                        this.saveTranslationChanges(obj)
                            .then();
                    },
                    onCancel: () => {
                        if (this.props.selectedLanguage.id == 1) {
                            if (this.state.showTranslationModal) {
                                modalTranslation.englishTranslation = translation.originalEnglishTranslation;
                            } else {
                                translation.englishTranslation = translation.originalEnglishTranslation;
                            }
                        } else {
                            if (this.state.showTranslationModal) {
                                modalTranslation.translation = translation.originalTranslation;
                            } else {
                                translation.translation = translation.originalTranslation;
                            }
                        }

                        this.setState({ translations: temp });
                        this.forceUpdate();
                    },
                });
            }
            else if (obj.Translation.trim().length === 0 && obj.LanguageId !== Languages.English) {
                this.deleteTextTranslation(obj.TranslationId);
            }
            else {
                this.saveTranslationChanges(obj)
                    .then();

                if (this.state.showTranslationModal) {
                    this.setState({ showTranslationModal: false });
                }
            }
        }
        else {
            this.showToast(Strings.Texts.Danger, "Error ! Key cannot be empty");
        }

        this.setState({ translations: temp });
    }

    deleteTextTranslation = (translationId) => {
        this.setState({ loaded: false });
        axios.delete(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/DeleteTextTranslation?translationId=' + translationId, { headers: this.headers })
            .then(() => {
                this.setState({ loaded: true });
                this.props.updateSearchResult();
            }).catch(error => {
                this.props.errorHandler(error);
            });
    }

    onModalCopyClick(translation, event) {
        translation.translation = translation.englishTranslation;
        const id = event.target.getAttribute('data-id');
        const translations = _.cloneDeep(this.state.translations); // deep clone to avoid direct mutation of state.
        const currentTranslation = translations.find(translation => translation.textId == id);
        currentTranslation.translation = translation.translation;
        currentTranslation.shouldRenderSaveIcon = currentTranslation.translation !== currentTranslation.originalTranslation;
        translation.shouldRenderSaveIcon = currentTranslation.shouldRenderSaveIcon;
        this.setState({ translations });
    }

    renderTranslationModal = () => {
        if (!this.selectedTranslation) {
            return;
        }

        return this.selectedTranslation.renderModal();
    }
    render() {
        let loader = null;
        if (this.state.loaded == false) {
            loader = <div className="pdf-page-loader">
                <Spin size="large" />
            </div>
        }

        return <React.Fragment>
            {loader}
            {this.state.translations && this.state.translations.length > 0 && <div className="height-100">
                {this.state.translations && this.displaySearchResult()}
                {this.renderTranslationModal()}
            </div>
            }
            {this.state.translations && this.state.translations.length == 0 && <div className="no-result-section" ><Icon type="warning" /> No matching results found.</div>}

        </React.Fragment>

    }
}