import React, { Component } from 'react';
import { Row, Col, Input, Button, Icon, Spin, Drawer, Form, Modal, Upload, Popover, Tabs, Tooltip, List, Select } from 'antd';
import axios from 'axios';
import { notify } from 'react-notify-toast';
import _ from 'lodash';
import Roles, { TranslationStatus, Languages, AppStates, VALID_TRANSLATIONS_CURLY_BRACE, STATUS_CODES, Env } from '../constants/Constants';
import Strings from '../constants/String';
import { PublishToEnv } from '../constants/Constants';

const TabPane = Tabs.TabPane;
const getAllTranslationMatches = (regex, value) => {
    const matches = [];
    let result = true;

    while (result) {
        const regexResult = regex.exec(value);

        if (regexResult && regexResult[0]) {
            matches.push(regexResult[0]);
        } else {
            result = false;
        }
    }

    return matches;
}

const isTranslationChangeValid = (englishTranslation, translation) => {
    const regex = new RegExp(/(\{{1,})(.*?)(\}{1,})/g);

    // If the english translation does not contain placeholders
    // OR, it belongs in the valid list of translations for curly brace presence, 
    // then there is no need to perform matches.
    if (!regex.test(englishTranslation) || VALID_TRANSLATIONS_CURLY_BRACE.indexOf(translation.trim()) >= 0) {
        return true;
    }

    regex.lastIndex = 0;

    const englishMatches = getAllTranslationMatches(regex, englishTranslation);
    const translationMatches = getAllTranslationMatches(regex, translation);
    return _.difference(englishMatches, translationMatches).length === 0;
}

const RESET_COMMANDS = {
    RESET_LOADED: 'RESET_LOADED',
}

const SET_COMMANDS = {
    SET_LOADED: 'SET_LOADED',
}

const { Option } = Select;
export default class TextTranslations extends Component {

    headers = {
        authorization: `Bearer ${JSON.parse(localStorage.getItem("gsuser")).token}`
    };
    user = {};
    newTranslationCounter = -1;

    constructor() {
        super();
        this.state = {
            size: 'large',
            loaded: false,
            translations: [],
            addTranslateOptionVisible: false,
            newKey: '',
            newTranslation: '',
            isUnique: AppStates.None,
            sectionFileListMap: null, // contains fileList per sectionId
            imageUrl: '',
            showTranslationModal: false,
            editedReleaseId: 0,
            newTranslationRelease: 0
        };
        this.onTranslationModalCancel = this.onTranslationModalCancel.bind(this);
        this.handleSaveClick = this.handleSaveClick.bind(this);
    }

    addNewTranslation = () => {
        var temp = this.state.translations;
        var translation = {
            key: '',
            textId: this.newTranslationCounter,
            statusId: 1,
            translation: '',
            englishTranslation: '',
            translationId: this.newTranslationCounter
        };
        this.newTranslationCounter--;
        temp.push(translation);
        this.setState({ translations: temp });
    }

    componentDidMount() {
        if (this.props.selectedLanguage && (this.props.selectedSection || this.props.selectedGroup) && this.props.selectedApplicationId && this.props.userInfo) {
            this.getTranslations();
        }
    }

    componentWillReceiveProps(props) {
        if (props.userInfo !== this.props.userInfo
            || props.selectedLanguage !== this.props.selectedLanguage
            || props.selectedApplicationId !== this.props.selectedApplicationId
            || props.selectedSection != this.props.selectedSection
            || props.selectedGroup != this.props.selectedGroup
            || props.isGroupView != this.props.isGroupView) {
            if (props.selectedLanguage && (props.selectedSection || props.selectedGroup) && props.selectedApplicationId && props.userInfo) {
                this.getTranslations(props);
            }
        }

        if (props.selectedApplicationId == this.props.selectedApplicationId) {
            const recievedApplication = props.applications.find(app => app.Id == props.selectedApplicationId);
            const currentApplication = this.props.applications.find(app => app.Id == this.props.selectedApplicationId);

            if (currentApplication.IsLocked !== recievedApplication.IsLocked) {
                currentApplication.IsLocked = recievedApplication.IsLocked;
            }
        }

        if (props.isVisible !== this.props.isVisible) {
            this.setState(prevState => {
                if (props.isAllIgnored) {
                    prevState.translations.forEach(item => {
                        item.isVisible = props.isVisible;
                    });
                }

                return prevState;
            }, () => {
                this.props.getNotifications();
            });
        }
    }

    shouldComponentUpdate(nextProps) {
        if (!this.state.loaded) {
            return true;
        }
        // change for group
        if (this.props.isGroupView) {
            const nextSelectedGroup = nextProps.selectedApp
                && nextProps.selectedApp.Group
                && nextProps.selectedApp.Group.find(group => group.Id === nextProps.selectedGroup);

            const currSelectedGroup = this.props.selectedApp
                && this.props.selectedApp.Group
                && this.props.selectedApp.Group.find(group => group.Id === this.props.selectedGroup);

            if (nextSelectedGroup && currSelectedGroup && this.props.notesModalVisible) {
                return nextSelectedGroup.Note !== currSelectedGroup.Note;
            } else {
                return true;
            }
        } else {
            const nextSelectedSection = nextProps.selectedApp
                && nextProps.selectedApp.Section
                && nextProps.selectedApp.Section.find(section => section.Id === nextProps.selectedSection);
            const currSelectedSection = this.props.selectedApp
                && this.props.selectedApp.Section
                && this.props.selectedApp.Section.find(section => section.Id === this.props.selectedSection);

            if (nextSelectedSection && currSelectedSection && this.props.notesModalVisible) {
                return nextSelectedSection.Note !== currSelectedSection.Note;
            } else {
                return true;
            }
        }
    }

    getExistingImage = () => {
        axios
            .get(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/Images?groupId=${this.props.selectedGroup}`, { headers: this.headers })
            .then(res => {
                let imageList = res.data;
                if (!imageList) {
                    this.setState({ sectionFileListMap: null });
                    return;
                }
                let sectionFileListMap = {
                    [this.props.selectedGroup]: imageList.map(item => {
                        let obj = {
                            uid: item.id,
                            url: item.path,
                            thumbUrl: item.path,
                            name: item.path.split('?')[0].split("/").pop(),
                            response: {
                                imageId: (item.id)
                            }
                        }
                        return obj;
                    })
                };
                this.setState({
                    sectionFileListMap
                });
            })
            .catch(reason => {
                this.props.errorHandler(reason);
            });
    }

    download = () => {
        const langId = this.props.selectedLanguage.id;
        let sections = this.props.applications.find(x => x.Id == this.props.selectedApplicationId).Section;
        const sectionIds = sections.map(x => x.Id).join(',');
        axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/GetAllSectionsTranslation?languageId=${langId}&sectionIds=${sectionIds}&userId=${this.props.userInfo.id}`, { headers: this.headers })
            .then(res => {
                if (!res.data.translations) {
                    return;
                }
                var list = res.data.translations;
                let allSectionsTranslation = this.createTranslationList(list, langId);
                let obj = {};

                if (this.props.selectedLanguage.id != Languages.English) {
                    obj = allSectionsTranslation.reduce((acc, item) => {
                        if (!item.translation) {
                            acc[item.key] = item.englishTranslation; // take english translation if translation is not updated
                        } else {
                            acc[item.key] = item.translation;
                        }

                        return acc;
                    }, obj);
                }
                else {
                    obj = allSectionsTranslation.reduce((acc, item) => {
                        acc[item.key] = item.englishTranslation;
                        return acc;
                    }, obj);
                }
                let languageCode = this.props.selectedLanguage.languageCode;

                var element = document.createElement('a');
                element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(JSON.stringify(obj, null, '\t')));
                element.setAttribute('download', languageCode + ".json");

                element.style.display = 'none';
                document.body.appendChild(element);

                element.click();
                document.body.removeChild(element);

            });
    }

    getTranslations = (propsParam = null, shouldUpdateNotifications = false, shouldPreserveTranslations = false) => {
        let props = propsParam ? propsParam : this.props
        let langId = props.selectedLanguage.id;
        let sectionId = props.selectedSection;
        let groupId = props.selectedGroup;
        let userInfo = props.userInfo;
        this.setState({
            loaded: false
        });
        if (props.isGroupView) {
            axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/GetGroupTranslations?groupId=${groupId}&languageId=${langId}&userId=${userInfo.id}`, { headers: this.headers })
                .then(res => {
                    let translations;
                    if (res.status === 204) { // if there are no groups then no translation are returned
                        translations = [];
                    } else {
                        const list = res.data.translations;
                        translations = this.createTranslationList(list, langId, shouldPreserveTranslations);

                        if (res.data.application) {
                            if (props.selectedApp) {
                                props.selectedApp.IsLocked = res.data.application.isLocked;
                                this.props.selectedApp.IsLocked = props.selectedApp.IsLocked;
                            }
                        }
                    }

                    this.setState({
                        loaded: true,
                        translations,
                        addTranslateOptionVisible: false,
                        newKey: '',
                        newTranslation: '',
                        newTranslationRelease: 0,
                        isUnique: AppStates.None
                    }, () => {
                        if (shouldUpdateNotifications) {
                            this.props.getNotifications();
                        }
                    });
                    this.getExistingImage();
                }).catch(error => {
                    this.props.errorHandler(error);
                });
        } else {
            axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/GetTranslations?sectionId=${sectionId}&languageId=${langId}&userId=${userInfo.id}`, { headers: this.headers })
                .then(res => {
                    const list = res.data.translations;
                    const translations = this.createTranslationList(list, langId, shouldPreserveTranslations);

                    if (res.data.application) {
                        if (props.selectedApp) {
                            props.selectedApp.IsLocked = res.data.application.lockedCode === STATUS_CODES.ApplicationLocked;
                            this.props.selectedApp.IsLocked = props.selectedApp.IsLocked;
                        }
                    }

                    this.setState({
                        loaded: true,
                        translations,
                        addTranslateOptionVisible: false,
                        newKey: '',
                        newTranslation: '',
                        newTranslationRelease: 0,
                        isUnique: AppStates.None
                    }, () => {
                        if (shouldUpdateNotifications) {
                            this.props.getNotifications();
                        }
                    });
                }).catch(error => {
                    this.props.errorHandler(error);
                });
        }
    }

    createTranslationList(list, langId, shouldPreserveTranslations = false) {
        let translations = [];
        for (let i = 0; i < list.length; i++) {
            // Get the english translation from the list of translations.
            const english = list[i].translations.find(o => o.languageId === 1);
            const englishTranslation = english ? english.translation : '';

            const translation = {
                key: list[i].key,
                textId: list[i].textId,
                statusId: 0,
                translation: '',
                originalTranslation: '',
                englishTranslation,
                originalEnglishTranslation: englishTranslation,
                translationId: 0,
                shouldRenderSaveIcon: false,
                releaseId: list[i].releaseId,
                originalRelease: list[i].releaseId
            };

            if (list[i].translations.length > 1) {
                const obj = _.find(list[i].translations, function (o) { return o.languageId === langId; });
                if (obj) {
                    translation.id = obj.translationId;
                    translation.statusId = obj.statusId;
                    translation.isVisible = obj.isVisible;
                    translation.translation = obj.translation;
                    translation.originalTranslation = obj.translation;
                    translation.translationId = obj.translationId;
                    translation.sno = obj.sno;
                    translation.previousTranslation = obj.previousTranslation;
                    translation.previousEnglish = english.previousTranslation;
                }
            }
            else {
                translation.sno = list[i].translations[0].sno;
                translation.id = list[i].translations[0].translationId;

                if (list[i].translations[0].languageId === langId) {
                    translation.isVisible = list[i].translations[0].isVisible;
                } else {
                    translation.isVisible = true;
                }

                if (langId != 1) {
                    translation.statusId = TranslationStatus.New;
                    translation.translation = '';
                    translation.originalTranslation = '';
                    translation.translationId = null;
                    translation.previousEnglish = list[i].translations[0].previousTranslation || '';
                }
                else {
                    translation.statusId = list[i].translations[0].statusId;
                    translation.translation = '';
                    translation.originalTranslation = '';
                    translation.translationId = list[i].translations[0].translationId;
                    translation.previousTranslation = list[i].translations[0].previousTranslation || '';
                }
            }

            // Preserve the previously rendered save icon and its translation.
            if (shouldPreserveTranslations) {
                const previouslyRenderedTranslation = this.state.translations.find(o => o.id === translation.id && o.shouldRenderSaveIcon);
                if (this.state.translations && this.state.translations.length && previouslyRenderedTranslation) {
                    translation.shouldRenderSaveIcon = previouslyRenderedTranslation.shouldRenderSaveIcon;

                    // Property 'englishTranslation' and 'translation' is updated for english and non-english languages respectively.
                    if (previouslyRenderedTranslation.translation) {
                        translation.translation = previouslyRenderedTranslation.translation;
                    } else if (previouslyRenderedTranslation.englishTranslation) {
                        translation.englishTranslation = previouslyRenderedTranslation.englishTranslation;
                    }
                }
            }

            translations.push(translation);
        }
        return translations;
    }

    checkUniqueness = (value) => {
        axios.get(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/CheckUniqueKey?sectionId=' + this.props.selectedSection + "&key=" + value, { headers: this.headers })
            .then(res => {
                if (res.data.isUnique == true) {
                    this.setState({ isUnique: AppStates.True });
                }
                else {
                    this.setState({ isUnique: AppStates.False });
                }
            }).catch(error => {
                this.props.errorHandler(error);
            });
    }

    handleTranslationInputChange = (event) => {
        const name = event.target.getAttribute('data-name');
        const id = parseInt(event.target.getAttribute('data-id'));
        const translations = _.cloneDeep(this.state.translations); // deep clone to avoid direct mutation of current state.
        const translation = translations.find(translation => translation.textId === id);

        /* For a translator, save icon should be rendered if: 
           
           1. It is not equal to its previous value. 
           2. Its english translation is changed by the developer. */

        if (name == "englishTranslation") {
            translation.shouldRenderSaveIcon = translation.originalEnglishTranslation !== event.target.value;
            translation.englishTranslation = event.target.value;


            if (this.selectedTranslation) {
                this.selectedTranslation.englishTranslation = event.target.value;
                this.selectedTranslation.shouldRenderSaveIcon = translation.shouldRenderSaveIcon;
            }
        } else {
            translation.shouldRenderSaveIcon = translation.originalTranslation !== event.target.value
            translation[name] = event.target.value;

            if (this.selectedTranslation) {
                this.selectedTranslation[name] = translation[name];
                this.selectedTranslation.shouldRenderSaveIcon = translation.shouldRenderSaveIcon;
            }
        }

        this.setState({ translations });
    }

    renderReleaseOPtions = () => {
        return (
            this.props.version.map(ver => {
                return (
                    <Option value={ver.id} key={ver.id + ver.versionName} > {ver.versionName}</Option >
                );
            })
        );

    }

    handleReleaseChange = (textId, value) => {
        const translations = _.cloneDeep(this.state.translations); // deep clone to avoid direct mutation of current state.
        const translation = translations.find(translation => translation.textId === textId);
        translation.releaseId = value;
        this.selectedTranslation.shouldRenderSaveIcon = translation.releaseId !== translation.originalRelease;
        this.setState({ translations });
    }

    handleKeyChange = (event) => {
        var target = event.target,
            value = target.value.trim();
        this.setState({ [target.getAttribute("data-name")]: value, isUnique: AppStates.None });

        if (target.getAttribute("data-name") == "newKey" && value.length > 2) {
            this.checkUniqueness(value);
        }
        else if (value.length > 0 && value.length < 3) {
            this.setState({ isUnique: AppStates.Loading });
        }
    }

    publishTranslations = () => {
        var temp = this.props.versionId === 0 ? this.state.translations : this.state.translations.filter(trans => trans.releaseId === this.props.versionId), ids = [];
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].statusId === TranslationStatus.Updated) {
                ids.push(temp[i].translationId);
            }
        }

        axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/PublishTranslations?languageId=${this.props.selectedLanguage.id}&applicationId=${this.props.selectedApplicationId}&releaseId=${this.props.versionId}`, { headers: this.headers })
            .then(res => {
                Modal.success({
                    title: 'Success',
                    content: (
                        <div>
                            <p>Translations published successfully.</p>
                        </div>
                    ),
                    onOk() { },
                });
                this.getTranslations(null, true);
            }).catch(error => {
                this.props.errorHandler(error);
            });
    }

    publishToEnvTranslations = (env) => {
        const langId = this.props.selectedLanguage.id;
        const sections = this.props.applications.find(x => x.Id == this.props.selectedApplicationId).Section;
        let sectionIds = '';
        const groups = this.props.applications.find(x => x.Id == this.props.selectedApplicationId).Group;
        let groupIds = '';
        if (this.props.isGroupView) {
            groupIds = groups.map(x => x.Id).join(',');
        } else {
            sectionIds = sections.map(x => x.Id).join(',');
        }

        axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/PublishToEnv/{env}?languageId=${langId}&sectionIds=${sectionIds}&applicationId=${this.props.selectedApplicationId}&languageCode=${this.props.selectedLanguage.languageCode}&userId=${this.props.userInfo.id}&groupIds=${groupIds}`, { headers: this.headers, params: { env } })
            .then(res => {
                Modal.success({
                    title: 'Success',
                    content: (
                        env === PublishToEnv.Test ?
                            <div>
                                <p>Translations published to Test successfully.</p>
                            </div> :
                            <div>
                                <p>Translations published to Live successfully.</p>
                            </div>
                    ),
                    onOk() { },
                });
            })

    }

    showAddTranslationOption = () => {
        if (this.props.userInfo && !this.props.isGroupView && !this.props.selectedApp.IsLocked) {
            return (
                <Row className="buttons">
                    <Button className="links" onClick={this.showAddTranslation} type="primary" icon="plus" size="large">
                        Add
                    </Button>
                </Row>
            )
        }
    }

    showDownloadOption = () => {
        if (this.props.userInfo && !this.props.isGroupView) {
            return (
                <Row className="buttons">
                    <Button className="links" type="primary" icon="download" onClick={this.download} size="large">
                        Download
                    </Button>
                </Row>
            )
        }
    }

    beforeImageUpload = (file) => {
        const acceptedFileTypes = ['image/jpeg', 'image/png', 'image/bmp', 'image/gif'];
        if (acceptedFileTypes.indexOf(file.type) < 0) {
            Modal.warning({
                title: `Warning: ${file.name} is an invalid file type.`,
                content: (
                    <List
                        size='small'
                        header={<div>Supported file types:</div>}
                        dataSource={acceptedFileTypes}
                        renderItem={item => <List.Item>{item}</List.Item>}
                    />
                ),
                onOk() { },
            });

            return false;
        }
        if (this.props.selectedGroup === 0) {
            Modal.warning({
                title: `Warning: No group for Image to Upload.`,
                onOk() { },
            });

            return false;
        }
        if (this.state.sectionFileListMap && this.state.sectionFileListMap[this.props.selectedGroup] &&
            this.state.sectionFileListMap[this.props.selectedGroup].find(x => x.name === file.name)) {
            Modal.info({
                title: "Image with this name already exists in this group.",
                content: '',
            });
            return false;
        }
        if (this.state.sectionFileListMap && this.state.sectionFileListMap[this.props.selectedGroup] && this.state.sectionFileListMap[this.props.selectedGroup].length > 9) {
            Modal.info({
                title: "Max 10 images can be uploaded.",
                content: '',
            });
            return false;
        }
    }

    onUploadImageStatusChange = (info) => {
        const fileList = info.fileList.reduce((acc, file) => {
            const { thumbUrl, ...rest } = file;

            if (info.file.status === 'done' && info.file.uid === file.uid) {
                rest.thumbUrl = info.file.response.accessUrl;
                rest.url = info.file.response.accessUrl;
            }

            acc.push(rest);
            return acc;
        }, []);

        if (info.file.status) {
            this.setState({ sectionFileListMap: { [this.props.selectedGroup]: fileList } });
        }
    }

    onImageRemove = (file) => {

        //TODO INH-61: This check avoids exception but does not delete the image if it is removed while being uploaded
        // from the storage and repository because it is trying to delete the image while it has not been uploaded yet. 
        if (!file || !file.response || !file.response.imageId) {
            return;
        }

        axios
            .delete(`${Strings.URL.GcLocalizerServiceBaseUrl}Admin/DeleteImage?groupId=${this.props.selectedGroup}&imageId=${file.response.imageId}&fileName=${file.name}`, { headers: this.headers })
            .then()
            .catch(reason => {
                this.props.errorHandler(reason);
            });

    }

    onImagePreview(thumbUrl) {
        let url = window.location.origin + "/Preview?prurl=" + thumbUrl;
        window.open(url);
    }

    showImageOption = () => {

        let UploadButton = <Button className="links" type="primary" icon="upload" size="large">Upload Image</Button>
        if (this.props.userInfo && this.props.userInfo.role === Roles.Developer && this.props.isGroupView && !this.props.selectedApp.IsLocked) {

            return (
                <Row className="buttons">
                    <Upload
                        multiple={true}
                        action={`${Strings.URL.GcLocalizerServiceBaseUrl}Admin/Upload`}
                        headers={this.headers}
                        fileList={this.state.sectionFileListMap && this.state.sectionFileListMap[this.props.selectedGroup]}
                        className='upload-list-inline'
                        listType='picture'
                        beforeUpload={this.beforeImageUpload}
                        onChange={this.onUploadImageStatusChange}
                        data={{
                            groupId: this.props.selectedGroup,
                            applicationId: this.props.selectedApplicationId
                        }}
                        onRemove={this.onImageRemove}
                        onPreview={(e) => { this.onImagePreview(e.url) }}
                    >
                        {UploadButton}
                    </Upload>
                </Row>
            );
        }
        else if (this.props.isGroupView && this.state.sectionFileListMap &&
            this.state.sectionFileListMap[this.props.selectedGroup]) {
            let thumnailSection = this.state.sectionFileListMap[this.props.selectedGroup].map(item => {
                return <Row className="buttons" style={{ top: '20px', position: 'relative' }}>
                    <a onClick={() => { this.onImagePreview(item.thumbUrl) }} target="_blank">
                        <img src={item.thumbUrl} width="100px" height="100px" />
                    </a>
                    <div>
                        <a onClick={() => { this.onImagePreview(item.thumbUrl) }} target="_blank">
                            {item.name}
                        </a>
                    </div>
                </Row>
            })
            return (
                <React.Fragment>
                    {thumnailSection}
                </React.Fragment>
            );
        }
    }

    showPublishOption = () => {
        if (!this.props.isGroupView && (this.props.userInfo.role === Roles.Developer) && !this.props.selectedApp.IsLocked) {
            return (
                <Row className="buttons">
                    <Button className="links" type="primary" size="large" onClick={this.publishTranslations}>Publish</Button>
                </Row>
            );
        }
    }

    showPublishToEnvOption = () => {
        if ((this.props.userInfo.role === Roles.Developer && !this.props.isGroupView) || (this.props.userInfo.role === Roles.Translator && this.props.isGroupView)) {
            const show = this.props.selectedApp ? this.props.selectedApp.AllowPublish && !this.props.selectedApp.IsLocked : false;

            const environment = Strings.Functions.getEnvironment();
            let globalWebsiteId = 0;

            if (environment === Env.Local || environment === Env.Dev) {
                globalWebsiteId = 81;
            } else if (environment === Env.Prod) {
                globalWebsiteId = 56;
            }

            const showPublishToLive = globalWebsiteId !== 0 && this.props && this.props.selectedApplicationId === globalWebsiteId;

            return show ?
                <div>
                    <Row className="buttons">
                        <Button className="links" type="primary" size="large" onClick={() => this.publishToEnvTranslations(PublishToEnv.Test)}>Publish to Test</Button>
                    </Row>
                    {
                        showPublishToLive &&
                        <Row className="buttons">
                            <Button className="links" type="primary" size="large" onClick={() => this.publishToEnvTranslations(PublishToEnv.Prod)}>Publish to Live</Button>
                        </Row>
                    }
                </div>
                : "";
        }
    }

    showLockOption = () => {
        if (this.props.userInfo && !this.props.isGroupView) {
            const btnText = this.props.selectedApp.IsLocked ? 'Unlock' : 'Lock';

            return (
                <Row className="buttons">
                    <Button className="links" type="primary" size="large" onClick={this.props.toggleAppLock}>{btnText}</Button>
                </Row>
            )
        }
    }

    saveTranslationChanges = (obj) => {
        this.setState({ loaded: false });
        return axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/SaveTranslation', obj, { headers: this.headers })
            .then(() => {
                this.getTranslations(null, true, true);
                this.showToast(Strings.Texts.Success, Strings.Texts.SavedSuccess);
                return true; // on success;
            }).catch(error => {
                this.props.errorHandler(error);
                this.setState({ loaded: true });
                return false; // on failure;
            });
    }

    deleteTranslation = (event) => {
        const target = event.currentTarget;
        const id = parseInt(target.getAttribute("data-id"));

        Modal.confirm({
            title: Strings.Texts.TranslationDeleteConfirmation,
            content: '',
            okText: Strings.Texts.Yes,
            cancelText: Strings.Texts.No,
            onOk: () => {
                axios.get(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/DeleteTranslation?textId=${id}`, { headers: this.headers })
                    .then(res => {
                        this.showToast(Strings.Texts.Success, Strings.Texts.TranslationDeleteSucess);
                        this.getTranslations(null, true);
                    }).catch(error => {
                        this.props.errorHandler(error);
                    });
            },
            onCancel() {

            }
        });
    }

    handleSaveClick(event, modalTranslation) {
        let target = event.currentTarget;
        let id = parseInt(target.getAttribute("data-id"));
        let temp = _.cloneDeep(this.state.translations);
        const translation = temp.find(o => o.textId === id);

        if (this.props.selectedLanguage.id !== 1 && translation && !isTranslationChangeValid(translation.englishTranslation, translation.translation)) {

            if (this.state.showTranslationModal) {
                this.setState({ showTranslationModal: false });
            }

            this.showToast(Strings.Texts.Danger, 'Curly brackets do not match!');
            event.preventDefault();
            event.stopPropagation();
            return false;
        }

        translation.shouldRenderSaveIcon = false;

        var obj = {
            RoleId: this.props.userInfo.role,
            TextId: translation.textId,
            TranslationId: translation.translationId,
            LanguageId: this.props.selectedLanguage.id,
            SectionId: this.props.selectedSection,
            Key: translation.key,
            StatusId: translation.statusId,
            Translation: translation.translation,
            ReleaseId: translation.releaseId,
        }

        if (this.props.selectedLanguage.id == 1) {
            obj.Translation = translation.englishTranslation;
        }
        if (translation.key.trim().length != 0) {
            var proceedWithSave = true;
            if (obj.Translation.trim().length === 0 && obj.LanguageId === Languages.English) {

                Modal.confirm({
                    title: 'The translation for the key is empty. Do you still want to proceed?',
                    content: '',
                    cancelText: 'No',
                    onOk: () => {
                        this.saveTranslationChanges(obj)
                            .then(isSuccess => {
                                if (!isSuccess) {
                                    if (this.props.selectedLanguage.id == 1) {
                                        translation.translation = translation.originalEnglishTranslation;
                                    } else {
                                        translation.translation = translation.originalTranslation;
                                    }

                                    this.setState({ translations: temp });
                                } else if (this.state.showTranslationModal) {
                                    this.setState({ showTranslationModal: false });
                                }

                            });
                    },
                    onCancel: () => {
                        if (this.props.selectedLanguage.id == 1) {
                            if (this.state.showTranslationModal) {
                                modalTranslation.englishTranslation = translation.originalEnglishTranslation;
                            } else {
                                translation.englishTranslation = translation.originalEnglishTranslation;
                            }
                        } else {
                            if (this.state.showTranslationModal) {
                                modalTranslation.translation = translation.originalTranslation;
                            } else {
                                translation.translation = translation.originalTranslation;
                            }
                        }

                        this.setState({ translations: temp });
                        this.forceUpdate();
                    },
                });
            }
            else if (obj.Translation.trim().length === 0 && obj.LanguageId !== Languages.English && obj.ReleaseId !== translation.originalRelease) {
                this.saveTranslationChanges(obj)
                    .then(isSuccess => {
                        if (!isSuccess) {
                            if (this.props.selectedLanguage.id == 1) {
                                translation.translation = translation.originalEnglishTranslation;
                            } else {
                                translation.translation = translation.originalTranslation;
                            }

                            this.setState({ translations: temp });
                        }
                    });
                if (this.state.showTranslationModal) {
                    this.setState({ showTranslationModal: false });
                }
            }
            else if (obj.Translation.trim().length === 0 && obj.LanguageId !== Languages.English) {
                this.deleteTextTranslation(obj.TranslationId);
            }
            else {
                this.saveTranslationChanges(obj)
                    .then(isSuccess => {
                        if (!isSuccess) {
                            if (this.props.selectedLanguage.id == 1) {
                                translation.translation = translation.originalEnglishTranslation;
                            } else {
                                translation.translation = translation.originalTranslation;
                            }

                            this.setState({ translations: temp });
                        }
                    });
                if (this.state.showTranslationModal) {
                    this.setState({ showTranslationModal: false });
                }
            }
        }
        else {
            this.showToast(Strings.Texts.Danger, "Error ! Key cannot be empty");
        }

        this.setState({ translations: temp });
    }

    deleteTextTranslation = (translationId) => {
        axios.delete(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/DeleteTextTranslation?translationId=' + translationId, { headers: this.headers })
            .then(() => {
                this.getTranslations(null, true);
                this.setState({ showTranslationModal: false });
            }).catch(error => {
                this.props.errorHandler(error);
            });
    }

    showToast = (type, message) => {
        let color = "";
        if (type === "danger") {
            color = { background: '#ee3647', text: "#FFFFFF" };
            notify.show(message, "custom", 2500, color);
        }
        else if (type === "info") {
            color = { background: '#0E1717', text: "#FFFFFF" };
            notify.show(message, "custom", 2000, color);
        }
        else if (type === "success") {
            color = { background: '#008000', text: "#FFFFFF" };
            notify.show(message, "custom", 5000, color);
        }
    }

    addTranslation = () => {
        if (this.state.newKey.trim().length != 0 && this.state.newTranslation.trim().length != 0 && this.state.isUnique == AppStates.True && this.state.newTranslationRelease !== 0) {
            const obj = {
                TranslationId: -1,
                SectionId: this.props.selectedSection,
                LanguageId: Languages.English,
                Key: this.state.newKey,
                StatusId: 2,
                Translation: this.state.newTranslation,
                ReleaseId: this.state.newTranslationRelease
            };
            this.setState({
                loaded: false
            });
            axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/AddTranslation', obj, { headers: this.headers })
                .then(() => {
                    this.showToast(Strings.Texts.Success, Strings.Texts.TranslationAddSucess);
                    this.getTranslations(this.state.languages);

                }).catch(error => {
                    this.props.errorHandler(error);
                });
        }
        else {
            let message = "";
            if (this.state.newKey.trim().length == 0) {
                message = Strings.Texts.EmptyNewKey;
            }
            else if (this.state.newTranslation.trim().length == 0) {
                message = Strings.Texts.EmptyNewTranslation;
            }
            else if (this.state.isUnique != AppStates.True) {
                message = Strings.Texts.NotUniqueKey;
            }
            else {
                message = Strings.Texts.ReleaseNotSelected;
            }

            this.showToast(Strings.Texts.Danger, message);
        }
    }

    showAddTranslation = () => {
        this.setState({
            addTranslateOptionVisible: true
        });
    }

    hideAddTranslation = () => {
        this.setState({
            addTranslateOptionVisible: false,
            newKey: "",
            newTranslation: "",
            newTranslationRelease: 0,
            isUnique: AppStates.None
        });
    }

    toggleTranslationModal = (event) => {
        const textId = event.target.getAttribute('data-id');

        if (textId) {
            this.selectedTranslation = _.cloneDeep(this.state.translations.find(translation => translation.textId == textId)); // deep clone to avoid mutation of state.
        } else {
            this.selectedTranslation = null;
        }

        this.setState(prevState => ({ showTranslationModal: !prevState.showTranslationModal }));
    }

    renderTranslationModal = () => {
        if (!this.selectedTranslation) {
            return;
        }

        return this.selectedTranslation.renderModal();
    }

    getTranslationWidth(showKeys, isEnglish) {
        if (isEnglish) {
            //admin role is also treated as devloper role in translation.
            if (showKeys) {
                return 15;
            }
            else {
                return 19;
            }
        } else {
            return 7;
        }
    }

    onTranslationModalCancel(translation) {
        const translations = _.cloneDeep(this.state.translations); // deep clone to avoid mutation of state.
        translation.translation = translation.originalTranslation;
        translation.englishTranslation = translation.originalEnglishTranslation;
        translation.shouldRenderSaveIcon = false;
        const currentTranslation = translations.find(t => t.textId === translation.textId);
        currentTranslation.translation = currentTranslation.originalTranslation;
        currentTranslation.englishTranslation = currentTranslation.originalEnglishTranslation;
        currentTranslation.shouldRenderSaveIcon = false;
        currentTranslation.releaseId = translation.originalRelease;

        this.setState(prevState => ({
            showTranslationModal: !prevState.showTranslationModal,
            translations: translations
        }));
    }

    onModalCopyClick(translation, event) {
        translation.translation = translation.englishTranslation;
        const id = event.target.getAttribute('data-id');
        const translations = _.cloneDeep(this.state.translations); // deep clone to avoid direct mutation of state.
        const currentTranslation = translations.find(translation => translation.textId == id);
        currentTranslation.translation = translation.translation;
        currentTranslation.shouldRenderSaveIcon = currentTranslation.translation !== currentTranslation.originalTranslation;
        translation.shouldRenderSaveIcon = currentTranslation.shouldRenderSaveIcon;
        this.setState({ translations });
    }

    onTranslationCopyClick = (event) => {
        const textId = event.target.getAttribute('data-id');
        const translations = _.cloneDeep(this.state.translations); // deep clone to avoid direct mutation of state.
        const translation = translations.find(trans => trans.textId == textId);
        translation.translation = translation.englishTranslation
        translation.shouldRenderSaveIcon = !!translation.translation && translation.translation !== translation.originalTranslation;
        this.setState({ translations });
    }

    handleIgnoreClick = (translation) => {
        this.setTranslationState([SET_COMMANDS.SET_LOADED])
        translation.isVisible = !translation.isVisible;
        const transferObject = {
            languageId: this.props.selectedLanguage.id,
            textTranslations: [translation]
        };

        axios.post(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/ToggleIgnore`, transferObject, { headers: this.headers })
            .then(() => {
                this.setState(prevState => {
                    const prevTranslation = prevState.translations.find(t => t.textId === translation.textId);
                    prevTranslation.isVisible = translation.isVisible;

                    if (prevTranslation.isVisible) {
                        this.props.updateApplicationIgnore({ type: 'translation' });
                    }

                    prevState.loaded = true;
                    return prevState;
                }, () => {
                    this.props.getNotifications();
                });
            })
            .catch(reason => {
                this.props.errorHandler(reason);
                this.resetTranslationState([RESET_COMMANDS.RESET_LOADED])
            });
    }

    renderTranslations = () => {
        const keyDisabled = this.props.userInfo && this.props.userInfo.role === Roles.Developer ? 'disabled' : 'disabled';
        let englishDisabled = false;
        const translations = this.props.userInfo && this.props.userInfo.role === Roles.Translator && !this.props.includeIgnored ? this.state.translations.filter(translation => translation.isVisible) : this.state.translations;
        const versionTranslations = this.props.versionId === 0 ? translations : translations.filter(trans => trans.releaseId === this.props.versionId);

        if (this.props.userInfo && this.props.userInfo.role === Roles.Translator) {
            englishDisabled = true;
            if (this.props.selectedLanguage && this.props.selectedLanguage.id == 1 && this.props.userLanguages.findIndex(x => x.languageId == 1) > -1) {
                englishDisabled = false;
            }
        } else if (this.props.selectedLanguage && this.props.selectedLanguage.id !== 1) {
            englishDisabled = true;
        }
        if (versionTranslations.length === 0) {
            return <div className="no-result-section" ><Icon type="warning" />There are no strings to translate for now!</div>
        }
        return versionTranslations.map(t => {
            let statusClass = '';

            switch (t.statusId) {
                case 1:
                    statusClass = 'new-translation';
                    break;

                case 2:
                    statusClass = 'updated-translation';
                    break;

                case 3:
                    statusClass = 'published-translation';
                    break;

                default:
                    break;
            }

            if (!t.isVisible) {
                statusClass = 'ignored-translation';
            }

            const saveIcon = (<Col span={1}>
                <div className="">
                    <Icon type="save" title="Update translation" onClick={this.handleSaveClick} data-id={t.textId} className="translation-row-icon" data-translationid={t.translationId} data-statusid={t.statusId} theme="twoTone" />
                </div>
            </Col>);
            const translatedValue = this.props.selectedLanguage.id != 1 ? (
                <Col span={this.props.userInfo.role === Roles.Developer ? 7 : 11}>
                    <div className="">
                        <Input
                            title="Double click to open"
                            size="default"
                            className={statusClass}
                            data-id={t.textId}
                            onChange={this.handleTranslationInputChange}
                            data-name="translation" placeholder="Translation" value={t.translation}
                            onDoubleClick={this.toggleTranslationModal}
                            onPressEnter={t.shouldRenderSaveIcon && this.handleSaveClick}
                        />
                    </div>
                </Col>
            ) : '';

            let historyIcon = '';
            let historyIconContent;
            let popoverTitle = '';

            if (t.previousTranslation && this.props.selectedLanguage && this.props.selectedLanguage.id !== 1) {
                historyIconContent = (
                    <Tabs defaultActiveKey="1">
                        <Tabs.TabPane tab="Previous Translation" key="1">
                            {t.previousTranslation}
                        </Tabs.TabPane>
                        <Tabs.TabPane tab="Previous English" key="2">
                            {t.previousEnglish}
                        </Tabs.TabPane>
                    </Tabs>
                );
            } else {
                popoverTitle = 'Previous Translation'
                historyIconContent = t.previousTranslation;
            }

            const copyIcon = (
                <Tooltip placement="top" title="Copy same as English">
                    <Button icon="arrow-right" type="primary" data-id={t.textId} onClick={this.onTranslationCopyClick} />
                </Tooltip>
            );

            if (t.previousTranslation) {
                historyIcon = (<Col span={1}>
                    <div className="">
                        <Popover overlayStyle={{ maxWidth: '500px', maxHeight: '150px' }} title={popoverTitle} content={historyIconContent} autoAdjustOverflow={false}>
                            <Icon type="clock-circle" theme="twoTone" className="translation-row-icon" />
                        </Popover>
                    </div>
                </Col>);
            }

            const deleteIcon = this.props.userInfo.role === Roles.Developer && !this.props.selectedApp.IsLocked ? (
                <Col span={1} title="Delete translation">
                    <Icon type="delete" theme="twoTone" onClick={this.deleteTranslation} data-id={t.textId} className="translation-row-icon" />
                </Col>
            ) : '';

            const ignoreIcon = this.props.userInfo.role === Roles.Translator && !this.props.selectedApp.IsLocked ? (
                <Col span={1} title={t.isVisible ? 'Ignore translation' : 'Un-ignore translation'}>
                    <Icon type={t.isVisible ? "eye-invisible" : "eye"} theme="twoTone" data-id={t.textId} onClick={() => this.handleIgnoreClick(t)} className="translation-row-icon" />
                </Col>
            ) : '';

            const self = this;
            const englishPlaceHolder = t.originalEnglishTranslation ? "English translation" : Strings.Texts.TranslationPlaceholder;

            t.renderModal = function () {

                const handleModalSaveClick = self.handleSaveClick.bind(this);
                return <Modal
                    title="Translation"
                    visible={self.state.showTranslationModal}
                    onCancel={() => { self.onTranslationModalCancel(this) }}
                    destroyOnClose={true}
                    footer={[
                        <Button key="cancel" type="danger" onClick={() => { self.onTranslationModalCancel(this) }} >Cancel</Button>,
                        <Button
                            key="submit" type="primary" onClick={(e) => handleModalSaveClick(e, this)}
                            data-id={this.textId} data-translationid={this.translationId}
                            data-statusid={this.statusId} disabled={!this.shouldRenderSaveIcon}>Save</Button>,
                        historyIcon
                    ]}
                >
                    <div>
                        {self.props.userInfo.role === Roles.Developer ?
                            <Select
                                style={{ marginBottom: "20px", width: "50%" }}
                                defaultValue={this.releaseId}
                                autosize={{ minRows: 3 }}
                                onChange={(e) => self.handleReleaseChange(this.textId, e)}
                            >
                                {self.renderReleaseOPtions()}
                            </Select>
                            : null}
                        {
                            self.props.selectedLanguage.id !== 1 ?
                                (<React.Fragment>
                                    <div id="modal-english-translation">
                                        <Input.TextArea
                                            autosize={{ minRows: 3 }}
                                            size="default"
                                            className={statusClass}
                                            placeholder={Strings.Texts.TranslationPlaceholder}
                                            onChange={self.handleTranslationInputChange}
                                            disabled={englishDisabled}
                                            data-id={this.textId}
                                            data-name="englishTranslation"
                                            value={this.englishTranslation} />
                                    </div>
                                    <div className="copy-modal">
                                        <Tooltip placement="right" title="Copy same as English">
                                            <Button icon="arrow-down" type="primary" data-id={this.textId} onClick={(e) => { self.onModalCopyClick(this, e) }} />
                                        </Tooltip>
                                    </div>
                                </React.Fragment>)
                                : ''
                        }
                        <div id="modal-translation">
                            <Input.TextArea
                                autosize={{ minRows: 3 }}
                                autoFocus
                                className={statusClass}
                                onFocus={(e) => {
                                    if (self.props.selectedLanguage.id === 1) {
                                        e.target.setSelectionRange(this.englishTranslation.length, this.englishTranslation.length);
                                    } else {
                                        e.target.setSelectionRange(this.translation.length, this.translation.length);
                                    }

                                }
                                }
                                data-id={this.textId}
                                onChange={self.handleTranslationInputChange}
                                data-name={self.props.selectedLanguage.id === 1 ? "englishTranslation" : "translation"}
                                placeholder={self.props.selectedLanguage.id === 1 ? englishPlaceHolder : "Translation"}
                                value={self.props.selectedLanguage.id === 1 ? this.englishTranslation : this.translation}
                            />
                        </div>
                    </div>
                </Modal>
            }

            return (
                <div>
                    <Row key={t.id}>
                        <Col span={this.props.isGroupView ? 2 : 1} className="sno-pos">
                            {this.props.isGroupView ? t.translationId : t.sno}
                        </Col>
                        {
                            this.props.userInfo.role === Roles.Developer ?
                                <Col span={4} title={t.key}>
                                    <div className="">
                                        <Input size="default" className={statusClass} placeholder="large size" disabled={keyDisabled} data-id={t.textId} data-name="key" value={t.key} />
                                    </div>
                                </Col> : ''

                        }
                        <Col span={this.getTranslationWidth(this.props.userInfo.role === Roles.Developer, this.props.selectedLanguage.id === Languages.English)}>
                            <div className="">
                                <Input
                                    title="Double click to open"
                                    size="default" className={statusClass} placeholder={englishPlaceHolder}
                                    onChange={this.handleTranslationInputChange}
                                    disabled={englishDisabled} data-id={t.textId}
                                    data-name="englishTranslation" value={t.englishTranslation}
                                    onDoubleClick={this.toggleTranslationModal}
                                    onPressEnter={t.shouldRenderSaveIcon && this.handleSaveClick}
                                />
                            </div>
                        </Col>
                        {
                            this.props.selectedLanguage.id !== 1 ?
                                <Col span={1}>
                                    <div className="copy-translation">
                                        {copyIcon}
                                    </div>
                                </Col> : ''
                        }
                        {translatedValue}
                        {
                            t.previousTranslation ? historyIcon : ''
                        }
                        {deleteIcon}
                        {ignoreIcon}
                        {t.shouldRenderSaveIcon ? saveIcon : ''}
                    </Row>
                </div>
            );
        });
    }

    renderMaskContent = () => {
        if (!this.props.applications || !this.props.selectedApplicationId) {
            return;
        }

        if (!this.props.selectedApp || !this.props.selectedApp.IsLocked || !this.state.translations.length) {
            return;
        }

        const translations = this.props.userInfo && this.props.userInfo.role === Roles.Translator && !this.props.includeIgnored ? this.state.translations.filter(translation => translation.isVisible) : this.state.translations;
        const versionTranslations = this.props.versionId === 0 ? translations : translations.filter(trans => trans.releaseId === this.props.versionId);
        if (!versionTranslations.length) {
            return;
        }
        const width = '79.4';
        let marginLeft = 0;
        if (this.props.userInfo.role === Roles.Translator || this.props.isGroupView) {
            marginLeft = 8.2;
        } else {
            marginLeft = 4;
        }

        return (
            <Tooltip placement="right" title="Application locked for publish">
                <div style={{ width: `${width}%`, left: `${marginLeft}%` }} className="mask-section"></div>
            </Tooltip>
        );
    }

    showAddNotesOption = (note) => {
        if (this.props.userInfo && this.props.userInfo.role === Roles.Developer && this.props.isGroupView && !this.props.selectedApp.IsLocked) {
            return (
                <Row className="buttons">
                    {!note && <Button className="links" onClick={this.props.addNote} type="primary" icon="plus" size="large">
                        Add Note
                    </Button>}
                </Row>
            )
        }
    }

    renderReleaseVersion = () => {
        const versions = this.props.version;
        return versions.map(ver => {
            return (
                <Option value={ver.id} key={ver.id + ver.versionName + "1"} > {ver.versionName}</Option >
            )
        });
    }

    selectedRelease = (value) => {
        this.setState({ newTranslationRelease: value })
    }

    render() {
        let buttonSection = null;
        let loader = null;
        let isUniqueICon = '';
        const selectedSection = this.props.selectedApp
            && this.props.selectedApp.Section
            && this.props.selectedApp.Section.find(section => section.Id == this.props.selectedSection);
        const note = selectedSection && selectedSection.Note;

        // Notes will now be shown on Group view.
        const selectedGroup = this.props.selectedGroup
            && this.props.selectedApp.Group
            && this.props.selectedApp.Group.find(group => group.Id == this.props.selectedGroup);
        const groupNote = selectedGroup && selectedGroup.Note;

        if (this.state.isUnique == AppStates.Loading) {
            isUniqueICon = <Button shape="circle" loading />;
        }
        else if (this.state.isUnique == AppStates.True) {
            isUniqueICon = <Icon type="check-circle" className="key-status" theme="twoTone" />;
        }
        else if (this.state.isUnique == AppStates.False) {
            isUniqueICon = <Icon type="close-circle" className="key-status" theme="twoTone" />;
        }
        if (this.state.loaded && this.props.applications.length > 0) {
            buttonSection = <Col style={{ marginTop: '10px' }} span={6}>
                <div className="buttons-block">
                    {this.showAddTranslationOption()}
                    {this.showDownloadOption()}
                    {this.showPublishOption()}
                    {this.showLockOption()}
                    {this.props.ignoreButtons}
                    {this.showImageOption()}
                    {this.showAddNotesOption(groupNote)}
                    {this.showPublishToEnvOption()}
                </div>
            </Col>
        }
        if (this.state.loaded == false) {
            loader = <div className="pdf-page-loader">
                <Spin size="large" />
            </div>
        }

        return (
            <React.Fragment>
                {loader}
                <Row>
                    <Col span={18}>
                        {this.props.renderNote()}
                        <div id="text-area">
                            <Row>
                                {this.renderMaskContent()}
                                {this.renderTranslations()}
                            </Row>

                        </div>
                    </Col>
                    {buttonSection}
                </Row>
                <Drawer
                    title="Add New Key"
                    width={500}
                    placement="right"
                    onClose={this.hideAddTranslation}
                    maskClosable={false}
                    visible={this.state.addTranslateOptionVisible}
                    style={{
                        height: 'calc(100% - 55px)',
                        overflow: 'auto',
                        paddingBottom: 53,
                    }}
                    destroyOnClose={true}
                >
                    <Form layout="vertical" hideRequiredMark>
                        <Row gutter={6}>
                            <Col span={24} className='field'>
                                <Row>
                                    <Col span={20}>
                                        <Input maxLength="100" onChange={this.handleKeyChange} data-name="newKey" value={this.state.newKey} placeholder="Key" />
                                    </Col>
                                    <Col span={3} offset={1}>
                                        {isUniqueICon}
                                    </Col>
                                </Row>
                            </Col>
                            <Col span={20} className='field'>
                                <Input
                                    data-name="newTranslation"
                                    onChange={(e) => { this.setState({ newTranslation: e.target.value }) }}
                                    value={this.state.newTranslation}
                                    placeholder="English" />
                            </Col>
                            <Col span={20} className='feild'>
                                <Select
                                    placeholder="Release"
                                    onChange={this.selectedRelease}
                                    optionFilterProp="children"
                                    filterOption={(input, option) =>
                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                >
                                    {this.renderReleaseVersion()}
                                </Select>
                            </Col>
                        </Row>
                    </Form>
                    <div
                        style={{
                            position: 'absolute',
                            bottom: 0,
                            width: '100%',
                            borderTop: '1px solid #e8e8e8',
                            padding: '10px 16px',
                            textAlign: 'right',
                            left: 0,
                            background: '#fff',
                            borderRadius: '0 0 4px 4px',
                        }}
                    >
                        <Button
                            style={{
                                marginRight: 8,
                            }}
                            onClick={this.hideAddTranslation}
                        >
                            Cancel
                        </Button>
                        <Button onClick={this.addTranslation} type="primary">Add</Button>
                    </div>
                </Drawer>
                {this.renderTranslationModal()}
            </React.Fragment>
        );
    }

    resetTranslationState = (resetCommands) => {
        const partialState = {};

        if (resetCommands.indexOf(RESET_COMMANDS.RESET_LOADED) >= 0) {
            partialState.loaded = true;
        }

        this.setState(partialState);
    }

    setTranslationState = (setCommands) => {
        const partialState = {};

        if (setCommands.indexOf(SET_COMMANDS.SET_LOADED) >= 0) {
            partialState.loaded = false;
        }

        this.setState(partialState);
    }
}
