import React, { Component } from 'react';
import { Row, Col, Button, Icon, Radio, Select, Badge, Card, Tabs, Switch, Tooltip, Spin, Modal, Input, Alert } from 'antd';
import axios from 'axios';
import Notifications, { notify } from 'react-notify-toast';
import jQuery from 'jquery';
import { Link } from 'react-router-dom'
import Roles, { TranslationType, PageRoutes, STATUS_CODES } from '../constants/Constants';
import _ from 'lodash';
import Strings from '../constants/String';
import TextTranslations from './TextTranslations';
import PdfTranslations from './PdfTranslations';
import Header from './Header';
import SearchBox from './SearchBox';
import SearchResultText from './SearchResultText';
import SearchResultFile from './SearchResultFile';
import queryString from 'query-string';

const Status = { 'New': 1, 'Updated': 2, 'Published': 3 };
const Languages = { 'English': 1 };
const States = { 'Loading': 1, 'True': 2, 'False': 3, 'None': 4 };
const TabPane = Tabs.TabPane;
Object.freeze(Status);
Object.freeze(Languages);

const getSelectedFromLeftRemaining = (sourceToChooseFrom, index) => {
    const leftRemaining = sourceToChooseFrom.slice(0, index);

    if (leftRemaining.length) {
        return _.findLast(leftRemaining, { IsVisible: true });
    }
}

const validateNote = (note) => {
    return !!(note && note.trim());
}

const RESET_COMMANDS = {
    RESET_LOADED: 'RESET_LOADED',
}

const SET_COMMANDS = {
    SET_LOADED: 'SET_LOADED',
}

class Translations extends Component {
    headers = {
        authorization: localStorage.getItem("gsuser") ? `Bearer ${JSON.parse(localStorage.getItem("gsuser")).token}` : null
    }
    userRole = 0;
    user = null;;
    sections = [];
    groups = [];
    selectedSection = 0;
    selectedGroup = 0;
    applications = [];
    userInfo = {};
    userLangList = [];
    allLanguages = [];
    newTranslationCounter = -1;
    userLanguages = [];
    isGroupView = false;
    includeIgnored = false;
    searchBoxRef = React.createRef();

    constructor() {
        super();
        this.state = {
            loaded: false,
            size: 'large',
            applications: [],
            selectedApplication: 0,
            sections: [],
            groups: [],
            languages: [],
            selectedLanguage: 0,
            addTranslateOptionVisible: false,
            isUnique: States.None,
            sectionFileListMap: null, // contains fileList per sectionId
            imageUrl: '',
            applicationNotification: [],
            sectionNotification: [],
            groupNotification: [],
            languageNotification: [],
            showTranslationModal: false,
            selectedSection: 0,
            selectedGroup: 0,
            userInfo: null,
            userLanguages: [],
            showSearchResultFlag: false,
            searchResultFile: [],
            searchResultText: [],
            translationTypeSearchOption: null,
            imageList: null,
            includeIgnored: false,
            isCompleteSectionOrAppIgnored: false, // false if only one translation was ignored, true, if section or application was ignored.
            languageNotificationLoading: false,
            note: '',
            groupNote: '',
            noteFieldValue: '',
            notesModalVisible: false,
            isGroupView: false,
            selectedVersion: 0,
            version: [],
        };
        localStorage.setItem("previousPage", "Translations");
    }

    componentWillMount() {
        this.user = JSON.parse(localStorage.getItem("gsuser"));
        const values = this.generateQueryParams(this.props.location.search);
        if (!this.user) {
            this.props.history.push('/');
            return;
        }
        else if (!this.user.isPasswordVerified) {
            this.props.history.push('/ChangePassword');
            return;
        }

        axios.get(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/GetUserInfo?id=' + this.user.id, { headers: this.headers })
            .then(res => {
                this.userRole = res.data.userinfo.role;
                this.userInfo = res.data.userinfo;
                if (res.data.userinfo.role === 2 && values.sectionId !== undefined) {
                    this.props.history.push("/");
                }
                this.applications = JSON.parse(res.data.application);
                if (this.applications && this.applications[0]) {
                    this.selectedApplication = values.applicationId !== undefined ? parseInt(values.applicationId) : this.applications[0].Id;
                    this.sections = this.applications.find(app => app.Id === this.selectedApplication).Section;
                    if (values.applicationId === undefined) {
                        this.groups = this.applications[0].Group.sort((a, b) => a.Sno - b.Sno);
                    }
                    else {
                        this.groups = this.applications.find(app => app.Id === this.selectedApplication).Group.sort((a, b) => a.Sno - b.Sno);
                    }
                    if (this.applications[0].IsLocked && this.userRole == Roles.Translator) {
                        this.showToast(Strings.Texts.Danger, Strings.Texts.AppLockMessage);
                    }
                }

                if (this.sections && this.sections[0]) {
                    this.selectedSection = values.sectionId !== undefined ? parseInt(values.sectionId) : this.sections[0].Id;
                }

                if (this.groups && this.groups[0]) {
                    this.selectedGroup = values.groupId !== undefined ? parseInt(values.groupId) : this.groups[0].Id;
                }

                // set the view to groupview if the user logged in is Translator
                this.isGroupView = this.userInfo.role === Roles.Translator ? true : false;
                if (values.groupId !== undefined) {
                    this.isGroupView = true;
                }

                // this.getLanguages is called in empty setState beacuse changes made to class variables were not being reflected.
                this.setState({}, this.getLanguages);


            }).catch(error => {
                this.errorHandler(error);
            });

        axios.get(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/GetVersionDetails', { headers: this.headers })
            .then(res => {
                this.setState({ version: res.data })
            }).catch(error => {
                this.errorHandler(error);
            });

    }

    generateQueryParams = (queryString) => {
        if (!queryString) {
            return {};
        }
        if (queryString.startsWith('?')) {
            queryString = queryString.substr(1);
        }
        const params = queryString.split('&');
        const queryParams = {};
        for (var index in params) {
            var keyValue = params[index].split('='); // index 0 -> key, 1 -> value.
            if (keyValue && keyValue[0]) {
                queryParams[keyValue[0]] = keyValue[1];
            }
        }
        return queryParams;
    }


    componentWillReceiveProps(props) {
        const values = this.generateQueryParams(props.location.search);
        let applicationId = parseInt(values.applicationId);
        this.setState({ loading: false });

        if (!values.applicationId) {
            let applications = this.state.applications;
            if (applications) {
                if (this.userInfo.role == Roles.Translator && !this.state.includeIgnored) {
                    const visibleApplications = applications.filter(app => app.IsVisible);
                    if (visibleApplications) {
                        applicationId = visibleApplications[0].Id;
                    }
                } else {
                    applicationId = this.state.applications[0].Id;
                }
            }
        }
        axios.get(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/IsApplicationLocked?appId=' + applicationId, { headers: this.headers })
            .then(res => {
                let applications = this.state.applications;
                let isLocked = res.data.isLocked;
                if (this.userInfo.role == Roles.Translator || this.state.isGroupView) {
                    let groups = [];
                    for (var i = 0; i < applications.length; i++) {
                        if (applications[i].Id == applicationId) {
                            groups = applications[i].Group.sort((a, b) => a.Sno - b.Sno);;
                            applications[i].IsLocked = isLocked;
                            break;
                        }
                    }

                    if (isLocked) {
                        this.showToast(Strings.Texts.Danger, Strings.Texts.AppLockMessage);
                    }

                    if (groups.length) {
                        const visibleGroups = groups.filter(grp => grp.IsVisible);
                        if (visibleGroups) {
                            this.selectedGroup = values.groupId !== undefined ? parseInt(values.groupId) : visibleGroups[0].Id;
                        } else {
                            this.selectedGroup = values.groupId !== undefined ? parseInt(values.groupId) : groups[0].Id;
                        }
                    } else {
                        this.selectedGroup = 0;
                    }

                    const selectedGroup = groups.length ? groups.find(group => group.Id === this.selectedGroup) : 0;
                    const groupNote = (selectedGroup && selectedGroup.Note) || '';
                    this.selectedApplication = applicationId;
                    this.setState({ translationTypeSearchOption: null, loading: false, applications, groups, selectedApplication: this.selectedApplication, selectedGroup: this.selectedGroup, groupNote, showSearchResultFlag: false });
                } else {
                    let sections = [];
                    let isLocked = res.data.isLocked;

                    for (var i = 0; i < applications.length; i++) {
                        if (applications[i].Id == applicationId) {
                            sections = applications[i].Section;
                            applications[i].IsLocked = isLocked;
                            break;
                        }
                    }

                    if (!sections[0].Id.IsVisible) {
                        const visibleSection = sections.find(s => s.IsVisible);

                        if (visibleSection) {
                            this.selectedSection = visibleSection.Id;
                        }
                    } else {
                        this.selectedSection = sections[0].Id;
                    }
                    this.selectedSection = values.sectionId !== undefined ? parseInt(values.sectionId) : this.selectedSection;
                    const selectedSection = sections.find(section => section.Id == this.selectedSection);
                    const note = (selectedSection && selectedSection.Note) || '';
                    this.selectedApplication = applicationId;
                    this.setState({ translationTypeSearchOption: null, loading: false, applications, sections, selectedApplication: this.selectedApplication, selectedSection: this.selectedSection, note, noteFieldValue: note, showSearchResultFlag: false, isGroupView: false });
                }
            }).catch(error => {
                this.setState({ loading: false });
                this.errorHandler(error);
            });
    }

    errorHandler = (error) => {
        if (error && error.response) {

            if (error.response.data) {
                if (error.response.data.error_code === STATUS_CODES.ApplicationIsLocked) {
                    this.showToast(Strings.Texts.Danger, Strings.Texts.AppLockMessage);
                    const applications = _.cloneDeep(this.state.applications);
                    const selectedApp = applications.find(app => app.Id === this.state.selectedApplication);
                    selectedApp.IsLocked = true;
                    this.setState({ applications });
                }

                return;
            }

            if (error.response.status) {
                if (error.response.status == 401) {
                    localStorage.removeItem("gsuser");
                    this.props.history.push('/');
                }
            }
            else {
                this.showToast(Strings.Texts.Danger, Strings.Texts.Error);
            }
        }

    }

    generateDefaultSectionId = (applicationId) => {
        let sections = [];
        const applications = this.state.applications;
        for (var i = 0; i < applications.length; i++) {
            if (applications[i].Id == applicationId) {
                sections = applications[i].Section;
                break;
            }
        }
        if (sections.length) {

            if (!sections[0].Id.IsVisible) {
                const visibleSection = sections.find(s => s.IsVisible);

                if (visibleSection) {
                    return visibleSection.Id;
                }
            } else {
                return sections[0].Id;
            }
        } else {
            return 0;
        }
    }
    generateDefaultGroupId = (applicationId) => {
        let groups = [];
        const applications = this.state.applications;
        for (var i = 0; i < applications.length; i++) {
            if (applications[i].Id == applicationId) {
                groups = applications[i].Group.sort((a, b) => a.Sno - b.Sno);
                break;
            }
        }

        if (groups.length) {
            const visibleGroups = groups.filter(grp => grp.IsVisible == true);
            if (this.state.userInfo.role === Roles.Translator || this.state.isGroupView == true) {
                if (this.state.includeIgnored) {
                    if (!visibleGroups.length && groups.length) {
                        return groups[0].Id;
                    }
                }
                if (visibleGroups.length) {
                    return visibleGroups[0].Id;
                } else {
                    return 0;
                }
            } else {
                if (visibleGroups.length) {
                    return visibleGroups[0].Id;
                } else {
                    return 0;
                }
            }

        } else {
            return 0;
        }
    }
    getLanguages = () => {
        axios.get(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/GetAllLanguages', { headers: this.headers })
            .then(res => {
                this.allLanguages = res.data.languages;
                this.userLanguages = this.userInfo.userLanguage;

                for (var i = 0; i < this.allLanguages.length; i++) {
                    if (this.userLanguages.find(o => o.languageId === this.allLanguages[i].id)) {
                        this.userLangList.push(this.allLanguages[i]);
                    }
                }

                /* If English is allowed for the user, then select english
                 * otherwise, select the first language in the alphabetical order*/
                const english = this.userLangList.find(lang => lang.name === 'English' || lang.id === 1);

                if (english) {
                    this.selectedLanguage = english;
                } else {
                    this.selectedLanguage = this.userLangList[0];
                }

                this.applications.map(application => {
                    const index = application.LanguageApplicationIgnore.findIndex(item => item.LanguageId === this.selectedLanguage.id);
                    application.IsVisible = index < 0;

                    application.Section.map(section => {
                        const index = section.LanguageSectionIgnore.findIndex(item => item.LanguageId === this.selectedLanguage.id);
                        section.IsVisible = index < 0;
                        return section;
                    });

                    application.Group.map(group => {
                        const index = group.LanguageGroupIgnore.findIndex(item => item.LanguageId === this.selectedLanguage.id);
                        group.IsVisible = index < 0;
                        return group;
                    });

                    return application;
                });


                if (!this.state.includeIgnored && this.selectedApplication) {
                    let selectedApp = this.applications.find(app => app.Id == this.selectedApplication);
                    if (this.userInfo.role === Roles.Translator) {
                        this.includeIgnored = !selectedApp.IsVisible;
                    }
                    if (this.selectedGroup !== 0) {
                        let selectedGroup = this.groups.find(grp => grp.Id == this.selectedGroup);
                        this.includeIgnored = !selectedGroup.IsVisible;
                    }


                    // The operations are being done inside empty setState beacuase the class variable are changed but the changes were not being
                    // reflected in the variables untill setState was performed.
                    this.setState(({}), () => {
                        const selectedSection = selectedApp.Section.find(section => section.Id == this.selectedSection);
                    });
                }

                const selectedSection = this.sections.find(section => section.Id === this.selectedSection)
                const note = (selectedSection && selectedSection.Note) || '';


                const selectedGroup = this.groups.find(group => group.Id === this.selectedGroup);
                const groupNote = (selectedGroup && selectedGroup.Note) || '';
                this.setState({
                    loaded: true,
                    languages: this.userLangList,
                    applications: this.applications,
                    sections: this.sections,
                    groups: this.groups,
                    selectedSection: this.selectedSection,
                    selectedGroup: this.selectedGroup,
                    selectedLanguage: this.selectedLanguage,
                    userInfo: this.userInfo,
                    selectedApplication: this.selectedApplication,
                    userLanguages: this.userLanguages,
                    note,
                    groupNote,
                    noteFieldValue: note,
                    isGroupView: this.isGroupView,
                    includeIgnored: this.includeIgnored
                }, () => { this.getNotifications() });
            }).catch(error => {
                this.errorHandler(error);
            });
        jQuery(document).ready(function () {
            // jQuery('#text-area').scrollbar();
        });
    }

    handleKeyChange = (event) => {
        var target = event.target,
            value = target.value.trim();
        this.setState({ [target.getAttribute("data-name")]: value, isUnique: States.None });

        if (target.getAttribute("data-name") == "newKey" && value.length > 2) {
            this.checkUniqueness(value);
        }
        else if (value.length > 0 && value.length < 3) {
            this.setState({ isUnique: States.Loading });
        }
    }

    onVersionChange = (e) => {
        const versionId = e.target.value;
        this.setState({ loading: false });
        this.setState({ selectedVersion: versionId });

    }

    languageChanged = (value) => {
        for (var i = 0; i < this.userLangList.length; i++) {
            if (this.userLangList[i].name === value) {
                this.selectedLanguage = this.userLangList[i];
            }
        }

        this.setState(prevState => {
            prevState.selectedLanguage = this.selectedLanguage;
            prevState.applications.map(application => {
                const index = application.LanguageApplicationIgnore.findIndex(item => item.LanguageId === this.selectedLanguage.id);
                application.IsVisible = index < 0;

                application.Section.map(section => {
                    const index = section.LanguageSectionIgnore.findIndex(item => item.LanguageId === this.selectedLanguage.id);
                    section.IsVisible = index < 0;
                });

                //changes for group 
                application.Group.map(group => {
                    const index = group.LanguageGroupIgnore.findIndex(item => item.LanguageId === this.selectedLanguage.id);
                    group.IsVisible = index < 0;
                });
            });

            if (prevState.userInfo.role === Roles.Translator && !this.state.includeIgnored) {
                const index = prevState.applications.findIndex(application => application.Id == prevState.selectedApplication);
                let selectedApp;

                if (!prevState.applications[index].IsVisible) {
                    const remainingApps = prevState.applications.slice(index + 1);

                    if (remainingApps.length) {
                        selectedApp = remainingApps.find(app => app.IsVisible);

                        if (selectedApp) {
                            prevState.selectedApplication = selectedApp.Id;
                            prevState.selectedSection = selectedApp.Section[0].Id;
                            prevState.sections = selectedApp.Section;

                            prevState.groups = selectedApp.Group;
                            prevState.selectedGroup = selectedApp.Group.length ? selectedApp.Group[0].Id : 0;
                        }
                    } else {
                        const leftRemainingApps = prevState.applications.slice(0, index);

                        if (leftRemainingApps) {
                            selectedApp = _.findLast(leftRemainingApps, { IsVisible: true });
                            prevState.selectedApplication = selectedApp.Id;
                            prevState.selectedSection = selectedApp.Section[0].Id;
                            prevState.sections = selectedApp.Section;

                            prevState.groups = selectedApp.Group;
                            prevState.selectedGroup = selectedApp.Group.length ? selectedApp.Group[0].Id : 0;
                        }
                    }
                }

                const groupIndex = prevState.groups.length && prevState.groups.findIndex(s => s.Id == prevState.selectedGroup);
                let selectedGroup = prevState.groups[groupIndex];

                if (selectedGroup && !selectedGroup.IsVisible) {
                    const remainingGroups = prevState.groups.slice(groupIndex + 1);

                    if (remainingGroups.length) {
                        selectedGroup = remainingGroups.find(g => g.IsVisible);

                        if (selectedGroup) {
                            prevState.selectedGroup = selectedGroup.Id;
                        }
                    } else {
                        const leftRemainingGroups = prevState.groups.slice(0, groupIndex);

                        if (leftRemainingGroups) {
                            selectedGroup = _.findLast(leftRemainingGroups, { IsVisible: true });

                            if (selectedGroup) {
                                prevState.selectedGroup = selectedGroup.Id;
                            }
                        }
                    }
                }

                prevState.groupNote = selectedGroup && selectedGroup.Note;
                prevState.noteFieldValue = prevState.note;
                return prevState;
            }
        }, () => {
            this.selectedApplication = this.state.selectedApplication;
            this.selectedSection = this.state.selectedSection;
            this.selectedGroup = this.state.selectedGroup;
            this.getNotifications();
        });
    }

    getNotifications = (isForAllLanguages = false, isForCurrentApplication = false) => {
        const notificationReducer = (groupByParam, data) => {
            return Object.values(_.groupBy(data, groupByParam)).reduce((acc, item) => {
                const obj = {
                    [groupByParam]: item[0][groupByParam],
                    notificationCount: item.reduce((accCount, res) => {
                        accCount += res.notificationCount;
                        return accCount;
                    }, 0)
                };
                // save the applicationId if reducing for section or group
                // it will be used to delete the section's/group's notification count.
                if (groupByParam === 'sectionId' || groupByParam === 'groupId') {
                    obj.applicationId = item[0].applicationId;
                }

                acc.push(obj);
                return acc;
            }, []);
        }

        const self = this;
        const selectedLanguageId = isForAllLanguages ? 0 : this.state.selectedLanguage.id;
        const selectedApplicationId = isForCurrentApplication ? this.state.selectedApplication : 0;
        axios
            .get(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/GetNotifications?applicationId=${selectedApplicationId}&userId=${this.userInfo.id}&roleId=${this.userInfo.role}&languageId=${selectedLanguageId}&isGroupView=${this.state.isGroupView}`, { headers: this.headers })
            .then(res => {
                const groupedByLanguages = _.groupBy(res.data.notifications, 'languageId');
                let notifications;
                let applicationNotification;
                let sectionNotification;
                let groupNotification;
                const isGroupView = this.state.isGroupView;

                if (!isForAllLanguages) {
                    notifications = groupedByLanguages[self.state.selectedLanguage.id];
                    applicationNotification = notificationReducer('applicationId', notifications);
                    if (isGroupView) { // if it is group view then run the notificationReducer for groups
                        groupNotification = notificationReducer('groupId', notifications);
                    } else {
                        sectionNotification = notificationReducer('sectionId', notifications);
                    }
                }

                if (!isForCurrentApplication) {
                    var languageNotification = Object.values(groupedByLanguages).reduce((acc, item) => {
                        const obj = {
                            languageId: item[0].languageId,
                            notificationCount: item.reduce((acc, item) => {
                                acc += item.notificationCount;
                                return acc;
                            }, 0)
                        };
                        acc.push(obj);
                        return acc;
                    }, []);
                }
                else {
                    let appNotificationcount = 0;
                    // update application notification
                    let newApplicationNotification = [...this.state.applicationNotification];
                    let newGroupNotification = [];
                    let newSectionNotification = [];

                    if (isGroupView) {
                        newGroupNotification = [...this.state.groupNotification];
                    } else {
                        newSectionNotification = [...this.state.sectionNotification];
                    }
                    let appIndex = newApplicationNotification.findIndex(app => app.applicationId === this.state.selectedApplication);

                    if (groupedByLanguages[this.state.selectedLanguage.id] !== undefined) {
                        if (isGroupView) {
                            groupedByLanguages[this.state.selectedLanguage.id].map(appGroup => {
                                appNotificationcount += appGroup.notificationCount;
                                // update the specific section too
                                let groupIndex = newGroupNotification.findIndex(group => group.groupId === appGroup.groupId);
                                if (groupIndex > -1) {
                                    // update the existing entry
                                    newGroupNotification[groupIndex].notificationCount = appGroup.notificationCount;
                                }
                                else {
                                    // add new entry
                                    newGroupNotification.push({ groupId: appGroup.groupId, notificationCount: appGroup.notificationCount, applicationId: this.state.selectedApplication });
                                }
                            });
                        } else {
                            groupedByLanguages[this.state.selectedLanguage.id].map(appSection => {
                                appNotificationcount += appSection.notificationCount;
                                // update the specific section too
                                let sectionIndex = newSectionNotification.findIndex(section => section.sectionId === appSection.sectionId);
                                if (sectionIndex > -1) {
                                    // update the existing entry
                                    newSectionNotification[sectionIndex].notificationCount = appSection.notificationCount;
                                }
                                else {
                                    // add new entry
                                    newSectionNotification.push({ sectionId: appSection.sectionId, notificationCount: appSection.notificationCount, applicationId: this.state.selectedApplication });
                                }
                            });
                        }
                    }
                    else {
                        if (isGroupView) {
                            // remove all the groups of selected application as there is no notification.
                            newGroupNotification = newGroupNotification.filter(group => group.applicationId !== this.state.selectedApplication);
                        } else {
                            // remove all the sections of selected application as there is no notification
                            newSectionNotification = newSectionNotification.filter(sec => sec.applicationId !== this.state.selectedApplication);
                        }
                    }
                    let newGroupedByLanguages = groupedByLanguages;
                    if (!newGroupedByLanguages) {
                        // means the notifications were empty
                        newGroupedByLanguages = {};
                        newGroupedByLanguages[this.state.selectedLanguage.id] = [];
                    }

                    if (isGroupView) {
                        // find the group in the reseponse from API
                        // if not found in response then remove the group
                        newGroupNotification = newGroupNotification.filter((group, index) => {
                            if (group.applicationId === this.state.selectedApplication) {
                                // check if language does not have any group
                                if (!newGroupedByLanguages[this.state.selectedLanguage.id]) {
                                    return false;
                                }
                                var groupIndex = newGroupedByLanguages[this.state.selectedLanguage.id].findIndex(gp => gp.groupId === group.groupId);
                                if (groupIndex > -1) {
                                    return true;
                                }
                                return false;
                            }
                            return true;
                        });
                    } else {
                        // find the section in the reseponse from API
                        // if not found in response then remove the section
                        newSectionNotification = newSectionNotification.filter((sec, index) => {
                            if (sec.applicationId === this.state.selectedApplication) {
                                // check if language does not have any section
                                if (!newGroupedByLanguages[this.state.selectedLanguage.id]) {
                                    return false;
                                }
                                var sectionIn = newGroupedByLanguages[this.state.selectedLanguage.id].findIndex(secc => secc.sectionId === sec.sectionId);
                                if (sectionIn > -1) {
                                    return true;
                                }
                                return false;
                            }
                            return true;
                        });
                    }


                    // if appIndex is -1 and appNotificationcount > 0 then add application to notification count and
                    // else save the new count

                    if (appIndex === -1 && appNotificationcount > 0) {
                        newApplicationNotification.push({ applicationId: this.state.selectedApplication, notificationCount: appNotificationcount });
                    }
                    else if (appIndex === -1 && appNotificationcount === 0) {
                        // remove empty entry from newNotificationCount

                        // This code made the array's element as empty instead of removing it therefore filter is used 
                        //delete newApplicationNotification[this.state.selectedApplication];

                        newApplicationNotification = newApplicationNotification.filter(x => x.applicationId !== this.state.selectedApplication)
                    }
                    else {
                        newApplicationNotification[appIndex].notificationCount = appNotificationcount;
                    }
                    // update total count for language
                    let newLanguageNotification = [...this.state.languageNotification];
                    let languageIndex = this.state.languageNotification.findIndex(l => l.languageId === this.state.selectedLanguage.id);
                    let oldLanguageNotificationCount = this.state.languageNotification[languageIndex].notificationCount;
                    let previousApplicationNotificationCount = 0;
                    if (appIndex > -1) {
                        // get the application previous count
                        previousApplicationNotificationCount = this.state.applicationNotification[appIndex].notificationCount;
                    }
                    newLanguageNotification[languageIndex].notificationCount = (oldLanguageNotificationCount - previousApplicationNotificationCount) + appNotificationcount;
                    // update the state
                    if (isGroupView) {
                        this.setState({ groupNotification: newGroupNotification, applicationNotification: newApplicationNotification, languageNotification: newLanguageNotification, languageNotificationLoading: false })
                    } else {
                        this.setState({ sectionNotification: newSectionNotification, applicationNotification: newApplicationNotification, languageNotification: newLanguageNotification, languageNotificationLoading: false })
                    }
                }
                if (isForAllLanguages) {
                    this.setState({ languageNotification, languageNotificationLoading: false });
                } else if (!isForCurrentApplication) {
                    if (isGroupView) {
                        this.setState(prevState => ({
                            applicationNotification,
                            groupNotification,
                            languageNotification: prevState.languageNotification.length ? prevState.languageNotification.reduce((acc, item) => {
                                if (languageNotification
                                    && languageNotification[0]
                                    && languageNotification[0].languageId === item.languageId) {
                                    return acc.concat(languageNotification);
                                } else {
                                    acc.push(item);
                                    return acc;
                                }
                            }, []) : languageNotification
                        }));
                    } else {
                        this.setState(prevState => ({
                            applicationNotification,
                            sectionNotification,
                            languageNotification: prevState.languageNotification.length ? prevState.languageNotification.reduce((acc, item) => {
                                if (languageNotification
                                    && languageNotification[0]
                                    && languageNotification[0].languageId === item.languageId) {
                                    return acc.concat(languageNotification);
                                } else {
                                    acc.push(item);
                                    return acc;
                                }
                            }, []) : languageNotification
                        }));
                    }
                }
            })
            .catch(reason => {
                this.setState({ languageNotificationLoading: false });
                this.errorHandler(reason);
            });
    }

    renderLanguages = () => {
        const Option = Select.Option;
        return this.state.languages.map((lang) => {
            if (this.state.languageNotification && this.state.languageNotification.find(n => n.languageId === lang.id)) {
                const notificationCount = this.state.languageNotification.find(n => n.languageId === lang.id).notificationCount;

                return (
                    <Option value={lang.name} key={lang.Id}>
                        <div className="lz-select-name" title={lang.name}>{lang.name}</div>
                        <div className="lz-select-badge">
                            <Badge count={notificationCount} overflowCount={999} />
                        </div>
                    </Option>
                );
            } else {
                return (
                    <Option value={lang.name} key={lang.Id + lang.name}>
                        <div className="lz-select-name" title={lang.name}>{lang.name}</div>
                        <div className="lz-select-badge">
                            {this.state.languageNotificationLoading ? <Icon type="loading"></Icon> : null}
                        </div>
                    </Option>
                );
            }
        });
    }

    renderSections = () => {
        const sections = this.state.sections;

        return sections.map(section => {
            let notificationCount = 0;

            if (this.state.sectionNotification) {
                const index = this.state.sectionNotification.findIndex(n => n.sectionId === section.Id);

                if (index >= 0) {
                    notificationCount = this.state.sectionNotification[index].notificationCount
                }
            }

            return <TabPane tab={<Link to={`/Translations/?applicationId=${this.selectedApplication}&sectionId=${section.Id}`}>{section.DisplayName || section.Name} <Badge count={notificationCount} overflowCount={999} /></Link>} key={section.Id}></TabPane>
        });
    }

    renderGroups = () => {
        const groups = this.state.isGroupView && !this.state.includeIgnored ?
            this.state.groups.filter(group => group.IsVisible) : this.state.groups;

        if (groups.length) {
            return groups.map(group => {
                let notificationCount = 0;

                if (this.state.groupNotification) {
                    const index = this.state.groupNotification.findIndex(n => n.groupId === group.Id);

                    if (index >= 0) {
                        notificationCount = this.state.groupNotification[index].notificationCount;
                    }
                }
                return <TabPane tab={<Link to={`/Translations?applicationId=${this.selectedApplication}&groupId=${group.Id}`}>{group.Name} <Badge count={notificationCount} overflowCount={999} /></Link>} key={group.Id}></TabPane>
            });
        } else {
            return <TabPane tab={<span>No Groups</span>} disabled></TabPane>
        }
    }

    renderVersions = () => {
        const versions = this.state.version;
        return versions.map(ver => {
            return (
                <Radio.Button value={ver.id} key={ver.id + ver.versionName} >{ver.versionName}</Radio.Button >);
        });
    }


    renderApplications = () => {
        let url = "";
        const applications = this.userInfo.role === Roles.Translator && !this.state.includeIgnored ?
            this.state.applications.filter(application => application.IsVisible) : this.state.applications;

        return applications.map(application => {

            if (this.state.isGroupView) {
                const groupId = this.generateDefaultGroupId(application.Id);
                url = `/Translations?applicationId=${application.Id}&groupId=${groupId}`;
            }
            else {
                const sectionId = this.generateDefaultSectionId(application.Id);
                url = `/Translations?applicationId=${application.Id}&sectionId=${sectionId}`;
            }
            if (this.state.applicationNotification && this.state.applicationNotification.find(n => n.applicationId === application.Id)) {
                const notificationCount = this.state.applicationNotification.find(n => n.applicationId === application.Id).notificationCount;
                const notificationBadge = notificationCount ? (<span className="relative-badge">{notificationCount}</span>) : '';

                if (application.Id != this.state.selectedApplication) {
                    return (
                        <Link to={url}>
                            <Radio.Button value={application.Id} key={application.Id + application.Name}>
                                {application.Name}
                                {notificationBadge}
                            </Radio.Button>
                        </Link>
                    );
                }
                else {
                    return (
                        <Link to={url}>
                            <Radio.Button value={application.Id} key={application.Id + application.Name}>
                                {application.Name}
                                {notificationBadge}
                            </Radio.Button>
                        </Link>
                    );
                }
            } else {
                if (application.Id != this.state.selectedApplication)
                    return (
                        <Link to={url}>
                            <Radio.Button value={application.Id} key={application.Id + application.Name}>
                                {application.Name}
                            </Radio.Button>
                        </Link>
                    )
                else
                    return (
                        <Link to={url}>
                            <Radio.Button value={application.Id} key={application.Id + application.Name}>
                                {application.Name}
                            </Radio.Button>
                        </Link>
                    )
            }
        });
    }

    showToast = (type, message) => {
        let color = "";

        if (type == "danger") {
            color = { background: '#ee3647', text: "#FFFFFF" };
            notify.show(message, "custom", 2500, color);
        }
        else if (type == "info") {
            color = { background: '#0E1717', text: "#FFFFFF" };
            notify.show(message, "custom", 2000, color);
        }
        else if (type == "success") {
            color = { background: '#008000', text: "#FFFFFF" };
            notify.show(message, "custom", 5000, color);
        }
    }

    handleLogoClick = () => {
        window.location.reload();
    }

    toggleAppLock = () => {
        const applications = _.cloneDeep(this.state.applications);
        let selectedApp = applications.find(app => app.Id == this.selectedApplication);

        if (!selectedApp) {
            return;
        }

        this.setTranslationState([SET_COMMANDS.SET_LOADED]);
        axios.post(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/ToggleLock/`, { isLocked: !selectedApp.IsLocked, Id: selectedApp.Id }, { headers: this.headers })
            .then(res => {

                if (res.data == STATUS_CODES.ApplicationLocked) {
                    selectedApp.IsLocked = true;
                } else {
                    selectedApp.IsLocked = false;
                }
                this.setState({ applications, loaded: true });
            })
            .catch(reason => {
                this.errorHandler(reason);
            });
    }

    updateNotesInSection = (note) => {
        this.setState(prevState => {
            const applications = prevState.applications;
            const sections = applications.find(app => app.Section.find(section => section.Id == prevState.selectedSection)).Section;
            const sectionFromAppState = sections.find(section => section.Id == prevState.selectedSection);
            const sectionFromSectionState = prevState.sections.find(section => section.Id == prevState.selectedSection);
            sectionFromAppState.Note = note;
            sectionFromSectionState.Note = note;
            return prevState;
        });
    }

    // Notes for group
    updateNotesInGroup = (note) => {
        this.setState(prevState => {
            const applications = prevState.applications;
            const groups = applications.find(app => app.Group.find(group => group.Id == prevState.selectedGroup)).Group;
            const groupFromAppState = groups.find(group => group.Id == prevState.selectedGroup);
            const groupFromGroupState = prevState.groups.find(group => group.Id == prevState.selectedGroup);
            groupFromAppState.Note = note;
            groupFromGroupState.Note = note;
            return prevState;
        });
    }

    renderNoApplicationMessage = () => {
        let shouldRenderNoAppMessage = this.state.applications.length === 0;

        if (!this.state.includeIgnored && this.state.userInfo && this.state.userInfo.role === Roles.Translator) {
            shouldRenderNoAppMessage = this.state.applications.filter(app => app.IsVisible).length === 0;
        }

        shouldRenderNoAppMessage = shouldRenderNoAppMessage && this.state.loaded;

        return shouldRenderNoAppMessage ? (
            <div className="centered">
                <Card
                    title={<div><Icon type="info-circle" theme="twoTone" /> No applications</div>}
                    style={{ width: 300 }} hoverable={true}
                >
                    <p>{Strings.Texts.NoAppMessage}</p>
                </Card>
            </div>
        ) : '';
    }

    generateIgnoreUrl = () => {
        let appid = 0;
        let groupId = 0

        const applications = _.cloneDeep(this.state.applications);
        const selectedApplication = applications.find(app => app.Id == this.state.selectedApplication);
        if (selectedApplication && selectedApplication.IsVisible) {
            let groupId = this.generateDefaultGroupId(this.state.selectedApplication);
            if (groupId === 0 && selectedApplication.Group.length) {
                groupId = selectedApplication.Group[0].Id;
            }
            return `/Translations/?applicationId=${this.state.selectedApplication}&groupId=${groupId}`;
        }
        const visibleApplication = this.state.applications.filter(app => app.IsVisible);

        if (visibleApplication && visibleApplication[0]) {
            appid = visibleApplication[0].Id;

            if (visibleApplication[0].Group && visibleApplication[0].Group[0]) {
                const visibleGroups = visibleApplication[0].Group.filter(grp => grp.IsVisible);
                if (visibleGroups.length) {
                    groupId = visibleGroups[0].Id;
                } else {
                    groupId = visibleApplication[0].Group[0].Id;
                }
            }
        }
        return `/Translations/?applicationId=${appid}&groupId=${groupId}`;
    }

    renderIgnoreSwitch = () => {
        if (this.state.showSearchResultFlag && this.userInfo.role === Roles.Translator) {
            return this.userInfo.role === Roles.Translator ? (<div className="switch">
                <Tooltip placement="right" title={this.state.includeIgnored ? 'Hide Ignored' : 'View Ignored'}>
                    <Switch size="large" checkedChildren={<Icon type="eye"></Icon>} unCheckedChildren={<Icon type="eye-invisible"></Icon>} checked={this.state.includeIgnored} onChange={this.handleIgnoreSwitchToggle} />
                </Tooltip>
            </div>) : ''
        }
        return this.userInfo.role === Roles.Translator ? (<div className="switch">
            <Tooltip placement="right" title={this.state.includeIgnored ? 'Hide Ignored' : 'View Ignored'}>
                <Link to={this.generateIgnoreUrl()} ><Switch size="large" checkedChildren={<Icon type="eye"></Icon>} unCheckedChildren={<Icon type="eye-invisible"></Icon>} checked={this.state.includeIgnored} onChange={this.handleIgnoreSwitchToggle} /></Link>
            </Tooltip>
        </div>) : ''
    }

    renderToggleGroupButton = () => {
        let url = "";
        if (this.state.isGroupView) {
            const sectionId = this.generateDefaultSectionId(this.state.selectedApplication);
            url = `/Translations/?applicationId=${this.state.selectedApplication}&sectionId=${sectionId}`;
        } else {
            let groupId = 0;
            const applications = this.state.applications;
            const application = applications.find(app => app.Id == this.selectedApplication);
            const groups = application.Group;
            if (groups.length) {
                const visibleGroups = groups.filter(grp => grp.IsVisible);
                if (visibleGroups.length) {
                    groupId = visibleGroups[0].Id;
                }
            }
            url = `/Translations/?applicationId=${this.state.selectedApplication}&groupId=${groupId}`;
        }
        return this.userInfo.role === Roles.Developer ? (<div className="buttons-block button-togglegroup">
            <Tooltip placement="right" title={this.state.isGroupView ? 'View Sections' : 'View Groups'}>
                View Groups&nbsp;<Link to={url}><Switch size="large" checkedChildren={<Icon type="check"></Icon>} unCheckedChildren={<Icon type="close"></Icon>} onChange={this.handleToggleGroupView} checked={this.state.isGroupView} /></Link>
            </Tooltip>
        </div>) : ''
    }

    renderContent = (searchResultSection, languageSection, translationSection) => {
        if (!this.state.loaded) {
            return <div className="pdf-page-loader"><Spin size="large"></Spin></div>
        }

        let shouldRenderContent = this.state.applications.length > 0;

        if (!this.state.includeIgnored && this.state.userInfo && this.state.userInfo.role === Roles.Translator) {
            shouldRenderContent = this.state.applications.filter(app => app.IsVisible).length > 0;
        }

        return shouldRenderContent ?
            (<div className="content">
                {this.state.showSearchResultFlag && searchResultSection}
                {!this.state.showSearchResultFlag && (
                    <Row>
                        <Col span={18}>
                            <Radio.Group className="lz-application-list" value={this.state.selectedApplication} onChange={this.onChangeApplication}>
                                {this.renderApplications()}
                            </Radio.Group>
                            <Radio.Group className="lz-application-list" value={this.state.selectedVersion} onChange={this.onVersionChange}>
                                <Radio.Button value={0} key={0 + "all"}>ALL</Radio.Button>
                                {this.renderVersions()}
                            </Radio.Group>
                            {
                                this.state.isGroupView ?
                                    <div className="lz-translations-categories">
                                        <Tabs activeKey={this.state.selectedGroup.toString()} defaultActiveKey="1">
                                            {this.renderGroups()}
                                        </Tabs>
                                    </div>
                                    :
                                    <div className="lz-translations-categories">
                                        <Tabs activeKey={this.state.selectedSection.toString()} defaultActiveKey="1" >
                                            {this.renderSections()}
                                        </Tabs>
                                    </div>
                            }
                        </Col>
                        <Col span={6}>
                            {languageSection}
                            {this.renderToggleGroupButton()}
                        </Col>
                    </Row>
                )
                }
                {!this.state.showSearchResultFlag && translationSection}
            </div>) : ''
    }

    onSearch = (searchResult, imageList, translationType) => {
        if (translationType == TranslationType.Text) {
            this.setState({ showSearchResultFlag: true, searchResultText: searchResult, translationTypeSearchOption: translationType, imageList: imageList });
        }
        else {
            this.setState({ showSearchResultFlag: true, searchResultFile: searchResult, translationTypeSearchOption: translationType });
        }
    }

    backToTranslations = () => {
        this.setState(prevState => ({ showSearchResultFlag: false, isGroupView: prevState.isGroupView }));
        this.searchBoxRef.resetSearchBox();
    }

    refreshSearchResult = (searchResultText) => {
        this.setState({ searchResultText: searchResultText });
    }

    handleIgnoreClick = (application, group) => {
        this.setTranslationState([SET_COMMANDS.SET_LOADED]);
        let transferObject = {};
        let isVisible = true;

        if (application) {
            isVisible = !application.IsVisible;

            transferObject = {
                applicationId: application.Id,
                languageId: this.state.selectedLanguage.id,
                isVisible
            };
        } else if (group) {
            isVisible = !group.IsVisible;
            const ignoredGroup = _.cloneDeep(group); // Avoid direct mutation of state.
            ignoredGroup.IsVisible = isVisible;

            transferObject = {
                applicationId: group.ApplicationId,
                languageId: this.state.selectedLanguage.id,
                groups: [ignoredGroup],
                isVisible
            };
        }

        axios.post(`${Strings.URL.GcLocalizerServiceBaseUrl}Localization/ToggleIgnore`, transferObject, { headers: this.headers })
            .then(() => {
                this.setState(prevState => {
                    prevState.isCompleteSectionOrAppIgnored = true;

                    if (application) {
                        let visibleApplications = prevState.applications.filter(s => s.IsVisible);
                        const index = visibleApplications.findIndex(app => app.Id === application.Id);
                        const app = index >= 0 ? visibleApplications[index] : prevState.applications.find(app => app.Id === application.Id);
                        app.IsVisible = isVisible;

                        if (isVisible) {
                            _.remove(app.LanguageApplicationIgnore, { LanguageId: prevState.selectedLanguage.id, ApplicationId: app.Id });
                        } else {
                            app.LanguageApplicationIgnore.push({ LanguageId: prevState.selectedLanguage.id, ApplicationId: app.Id });
                        }

                        app.Group.forEach(g => {
                            g.IsVisible = isVisible;

                            if (isVisible) {
                                _.remove(g.LanguageGroupIgnore, { LanguageId: prevState.selectedLanguage.id, GroupId: g.Id });
                            } else {
                                g.LanguageGroupIgnore.push({ LanguageId: prevState.selectedLanguage.id, GroupId: g.Id });
                            }
                        });

                        if (!this.state.includeIgnored) {
                            const remainingVisible = visibleApplications.slice(index + 1);
                            let selectedApp;

                            if (remainingVisible.length) {
                                prevState.selectedApplication = remainingVisible[0].Id;
                                selectedApp = remainingVisible[0];
                            } else {
                                if (index > 0) {
                                    prevState.selectedApplication = visibleApplications[index - 1].Id;
                                    selectedApp = visibleApplications[index - 1];
                                }
                            }

                            if (selectedApp) {
                                prevState.groups = selectedApp.Group;
                                const visibleGroups = selectedApp.Group.filter(g => g.IsVisible);

                                if (visibleGroups.length) {
                                    prevState.selectedGroup = visibleGroups[0].Id;
                                }
                            }
                        }
                    } else if (group) {
                        // visibleGroups1 is used beacuase of shadow naming issue.
                        const visibleGroups1 = prevState.groups.filter(group1 => group1.IsVisible);
                        const groupIndex = visibleGroups1.findIndex(g => g.Id === group.Id);
                        let selectedGroup;

                        if (groupIndex >= 0) {
                            selectedGroup = visibleGroups1[groupIndex];
                        } else {
                            selectedGroup = prevState.groups.find(g => g.Id === group.Id);
                        }

                        if (selectedGroup) {
                            selectedGroup.IsVisible = isVisible;

                            if (selectedGroup.IsVisible) {
                                _.remove(selectedGroup.LanguageGroupIgnore, { LanguageId: prevState.selectedLanguage.id, GroupId: selectedGroup.Id });
                                const relatedApp = prevState.applications.find(app => app.Id === selectedGroup.ApplicationId);

                                if (!relatedApp.IsVisible) {
                                    relatedApp.IsVisible = true;
                                    _.remove(relatedApp.LanguageApplicationIgnore, { LanguageId: prevState.selectedLanguage.id, ApplicationId: relatedApp.Id });
                                }

                            } else {
                                selectedGroup.LanguageGroupIgnore.push({ LanguageId: prevState.selectedLanguage.id, GroupId: selectedGroup.Id });
                            }
                        }

                        if (!this.state.includeIgnored) {
                            const remainingVisibleGroups = visibleGroups1.slice(groupIndex + 1);

                            if (remainingVisibleGroups.length) {
                                prevState.selectedGroup = remainingVisibleGroups[0].Id;
                            } else if (groupIndex > 0) {
                                prevState.selectedGroup = visibleGroups1[groupIndex - 1].Id;
                            }
                        }

                        // If all groups have been ignored then application also needs to be ignored.
                        if (!visibleGroups1.filter(s => s.IsVisible).length) {
                            let visibleApplications = prevState.applications.filter(s => s.IsVisible);
                            const index = visibleApplications.findIndex(app => app.Id === group.ApplicationId);
                            const app = index >= 0 ? visibleApplications[index] : prevState.applications.find(app => app.Id === application.Id);
                            app.IsVisible = isVisible;
                            app.LanguageApplicationIgnore.push({ LanguageId: prevState.selectedLanguage.id, ApplicationId: app.Id });

                            if (!this.state.includeIgnored) {
                                const remainingVisible = visibleApplications.slice(index + 1);
                                let selectedApp;

                                if (remainingVisible.length) {
                                    prevState.selectedApplication = remainingVisible[0].Id;
                                    selectedApp = remainingVisible[0];
                                } else {
                                    if (index > 0) {
                                        prevState.selectedApplication = visibleApplications[index - 1].Id;
                                        selectedApp = visibleApplications[index - 1];
                                    }
                                }

                                if (selectedApp) {
                                    prevState.groups = selectedApp.Group;
                                    const visibleGroups1 = selectedApp.Group.filter(g => g.IsVisible);

                                    if (visibleGroups1.length) {
                                        prevState.selectedGroup = visibleGroups1[0].Id;
                                    }
                                }
                            }
                        }
                    }

                    prevState.loaded = true;
                    return prevState;
                }, () => {
                    this.selectedApplication = this.state.selectedApplication;
                    this.selectedGroup = this.state.selectedGroup;
                    this.getNotifications();
                });
            })
            .catch(error => {
                this.errorHandler(error);
                this.resetTranslationState([RESET_COMMANDS.RESET_LOADED]);
            })
    }

    handleIgnoreSwitchToggle = () => {
        this.setState(prevState => {
            prevState.includeIgnored = !prevState.includeIgnored;
            return prevState;
        });
    }

    updateApplicationIgnore = (source) => {
        if (source.type === 'translation') {
            this.setState(prevState => {
                prevState.isCompleteSectionOrAppIgnored = false;
                const prevSection = prevState.sections.find(s => s.Id == prevState.selectedSection);
                const sectionIndex = prevSection.LanguageSectionIgnore.findIndex(item => item.SectionId === prevSection.Id && item.LanguageId === prevState.selectedLanguage.id);

                if (sectionIndex >= 0) {
                    prevSection.IsVisible = true;
                    prevSection.LanguageSectionIgnore.splice(sectionIndex, 1);
                }

                const prevGroup = prevState.groups.find(g => g.Id == prevState.selectedGroup);
                const groupIndex = prevGroup.LanguageGroupIgnore.findIndex(item => item.GroupId === prevGroup.Id && item.LanguageId === prevState.selectedLanguage.id)

                if (groupIndex >= 0) {
                    prevGroup.IsVisible = true;
                    prevGroup.LanguageSectionIgnore.splice(groupIndex, 1);
                }

                const prevApplication = prevState.applications.find(app => app.Id == prevState.selectedApplication);
                const appIndex = prevApplication.LanguageApplicationIgnore.findIndex(item => item.ApplicationId === prevApplication.Id && item.LanguageId === prevState.selectedLanguage.id);

                if (appIndex >= 0) {
                    prevApplication.IsVisible = true;
                    prevApplication.LanguageApplicationIgnore.splice(appIndex, 1);
                }

                return prevState;
            });
        }
    }

    onLanguageDropdownFocus = () => {
        if (this.state.languageNotification.length <= 1) {
            this.setState({ languageNotificationLoading: true });
            this.getNotifications(true);
        }
    }

    onNoteChange = (e) => {
        const value = e.target.value;
        this.setState({ noteFieldValue: value });
    }

    deleteNote = () => {
        const obj = {
            groupId: this.state.selectedGroup,
            note: ''
        }
        this.setTranslationState(SET_COMMANDS.SET_LOADED);
        axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/AddNoteToGroup', obj, { headers: this.headers })
            .then(() => {
                this.resetTranslationState(RESET_COMMANDS.RESET_LOADED);
                this.updateNotesInGroup(obj.note);
                this.setState({ groupNote: obj.note, noteFieldValue: obj.note });
            });
    }

    saveNote = () => {
        const isValid = validateNote(this.state.noteFieldValue);

        if (!isValid) {
            this.showToast(Strings.Texts.Danger, 'Invalid Note!');
            this.setState({ notesModalVisible: false, noteFieldValue: this.state.note });
            return;
        }

        const obj = {
            groupId: this.state.selectedGroup,
            note: this.state.noteFieldValue
        }

        this.setTranslationState(SET_COMMANDS.SET_LOADED);
        if (this.state.note != this.state.noteFieldValue) {
            axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'Localization/AddNoteToGroup', obj, { headers: this.headers })
                .then(() => {
                    this.resetTranslationState(RESET_COMMANDS.RESET_LOADED);
                    this.updateNotesInGroup(this.state.noteFieldValue);
                    this.setState({ notesModalVisible: false, groupNote: this.state.noteFieldValue });
                });
        }
    }

    onCancelNote = () => {
        this.setState({ notesModalVisible: false, noteFieldValue: this.state.groupNote });
    }

    toggleNotesModalVisibility = (e) => {
        this.setState(prevState => ({ notesModalVisible: !prevState.notesModalVisible }));
    }

    displayNoteModal = () => {
        const { notesModalVisible, noteFieldValue, groupNote } = this.state;

        return (<Modal
            title="Note"
            visible={notesModalVisible}
            onCancel={this.onCancelNote}
            footer={[
                <Button
                    key="submit" type="primary" onClick={this.saveNote}
                    disabled={noteFieldValue && noteFieldValue.trim() === groupNote}>Save</Button>
            ]}
            destroyOnClose
        >
            <Input.TextArea autosize={{ minRows: 3 }}
                size="default" value={noteFieldValue}
                onChange={this.onNoteChange} />

        </Modal>);
    }

    renderNote = () => {
        let description;
        const selectedGroup = this.state.groups.find(group => group.Id == this.state.selectedGroup);
        const groupNote = selectedGroup && selectedGroup.Note;
        const selectedApplication = this.state.applications.find(app => app.Id === this.state.selectedApplication);

        if (selectedApplication && !selectedApplication.IsLocked && this.state.isGroupView && this.state.userInfo.role === Roles.Developer) {
            description = (< div >
                <Icon title="delete" onClick={this.deleteNote} className="note-action-item" type="delete"></Icon>
                <Icon title="edit" onClick={this.toggleNotesModalVisibility} className="note-action-item" type="edit"></Icon>
            </div >
            );
        }

        return this.state.isGroupView ? (groupNote && <Alert message={groupNote} type="info" description={description} />) : '';
    }

    handleToggleGroupView = () => {
        const groups = this.state.applications && this.state.applications.find(app => app.Id == this.state.selectedApplication).Group;
        const sections = this.state.applications && this.state.applications.find(app => app.Id == this.state.selectedApplication).Section;

        const selectedGroup = groups.length && groups[0].Id;
        const selectedSection = sections.length && sections[0].Id;

        this.setState(prevState =>
        ({
            isGroupView: !prevState.isGroupView,
            groups: groups,
            sections: sections,
        }), () => {
            this.getNotifications();
            if (this.state.isGroupView) {
                this.renderGroups();
            } else {
                this.renderSections();
            }
        });

    }

    render() {
        let languageSection = null;
        let translationSection = null;
        const selectedApplication = this.state.applications && this.state.applications.find(x => x.Id == this.state.selectedApplication);
        const selectedSection = selectedApplication && selectedApplication.Section ? selectedApplication.Section.find(section => section.Id == this.state.selectedSection) : null;
        const selectedGroup = selectedApplication && selectedApplication.Group ? selectedApplication.Group.find(group => group.Id == this.state.selectedGroup) : null;
        let ignoreButtons = [];

        if (selectedApplication && this.state.isGroupView) {
            const ignoreAppBtnText = selectedApplication.IsVisible ? 'Ignore App' : 'Un-Ignore App';
            // ignoreSection will now become obsolete since now the translator can only view groups. 
            // but the text will still be as Ignore Section as the Transalator has no information of gorups.          
            const ignoreGroupBtnText = selectedGroup && selectedGroup.IsVisible ? 'Ignore Section' : 'Un-Ignore Section';

            if ((this.state.applications.filter(s => s.IsVisible).length || this.state.includeIgnored)
                && selectedApplication && !selectedApplication.IsLocked && (this.state.userInfo.role === Roles.Translator)) {
                ignoreButtons.push((
                    <Row className="buttons">
                        <Button className="links" type="primary" onClick={() => this.handleIgnoreClick(selectedApplication, null)} size="large">
                            {ignoreAppBtnText}
                        </Button>
                    </Row>
                ));
            }

            if ((this.state.groups.filter(group => group.IsVisible).length || this.state.includeIgnored)
                && selectedApplication && !selectedApplication.IsLocked && (this.state.userInfo.role === Roles.Translator) && selectedGroup) {
                ignoreButtons.push((
                    <Row className="buttons">
                        <Button className="links" type="primary" onClick={() => this.handleIgnoreClick(null, selectedGroup)} size="large">
                            {ignoreGroupBtnText}
                        </Button>
                    </Row>
                ));
            }
        }


        if (selectedApplication && selectedApplication.TranslationTypeId == TranslationType.Text) {

            translationSection = (<div id="text-translation">
                <TextTranslations
                    errorHandler={this.errorHandler}
                    applications={this.state.applications}
                    userInfo={this.state.userInfo}
                    selectedSection={this.state.selectedSection}
                    selectedGroup={this.state.selectedGroup}
                    selectedApplicationId={this.state.selectedApplication}
                    selectedApp={selectedApplication}
                    selectedLanguage={this.state.selectedLanguage}
                    userLanguages={this.state.userLanguages}
                    toggleAppLock={this.toggleAppLock}
                    getNotifications={this.getNotifications}
                    ignoreButtons={ignoreButtons}
                    versionId={this.state.selectedVersion}
                    version={this.state.version}
                    includeIgnored={this.state.includeIgnored}
                    isVisible={selectedSection ? selectedSection.IsVisible : false}
                    updateApplicationIgnore={this.updateApplicationIgnore}
                    isAllIgnored={this.state.isCompleteSectionOrAppIgnored}
                    showToast={this.showToast}
                    renderNote={this.renderNote.bind(this)}
                    addNote={this.toggleNotesModalVisibility}
                    notesModalVisible={this.state.notesModalVisible}
                    isGroupView={this.state.isGroupView}
                />
            </div>)
        }
        else if (selectedApplication && selectedApplication.TranslationTypeId == TranslationType.Pdf) {
            translationSection = (<Row id="pdf-translation">
                <PdfTranslations
                    errorHandler={this.errorHandler}
                    applications={this.state.applications}
                    userInfo={this.state.userInfo}
                    selectedSection={this.state.selectedSection}
                    selectedGroup={this.state.selectedGroup}
                    selectedApplicationId={this.state.selectedApplication}
                    selectedApp={selectedApplication}
                    selectedLanguage={this.state.selectedLanguage}
                    userLanguages={this.state.userLanguages}
                    toggleAppLock={this.toggleAppLock}
                    getNotifications={this.getNotifications}
                    ignoreButtons={ignoreButtons}
                    versionId={this.state.selectedVersion}
                    version={this.state.version}
                    includeIgnored={this.state.includeIgnored}
                    isVisible={selectedSection ? selectedSection.IsVisible : false}
                    updateApplicationIgnore={this.updateApplicationIgnore}
                    isAllIgnored={this.state.isCompleteSectionOrAppIgnored}
                    showToast={this.showToast}
                    renderNote={this.renderNote.bind(this)}
                    addNote={this.toggleNotesModalVisibility}
                    notesModalVisible={this.state.notesModalVisible}
                    isGroupView={this.state.isGroupView}
                />
            </Row>)
        }

        if (this.state.applications && this.state.applications.length > 0) {
            languageSection =
                <div className="buttons-block">
                    <Row className="buttons">
                        <Select
                            className="lz-language-select"
                            dropdownClassName="lz-language-select-dropdown"
                            onFocus={this.onLanguageDropdownFocus}
                            defaultValue={this.state.selectedLanguage.name}
                            style={{ width: 150 }} onChange={this.languageChanged}
                        >
                            {this.renderLanguages()}
                        </Select>
                    </Row>
                </div>
        }

        let translationTypeId = this.state.translationTypeSearchOption ? this.state.translationTypeSearchOption : selectedApplication && selectedApplication.TranslationTypeId;

        let searchResultSection = selectedApplication && translationTypeId == TranslationType.Text ?
            <SearchResultText
                userInfo={this.state.userInfo}
                applications={this.state.applications}
                selectedLanguage={this.state.selectedLanguage}
                searchResult={this.state.searchResultText}
                userLanguages={this.state.userLanguages}
                selectedSection={this.state.selectedSection}
                selectedGroup={this.state.selectedGroup}
                selectedApp={selectedApplication}
                errorHandler={this.errorHandler}
                imageList={this.state.imageList}
                updateSearchResult={this.searchBoxRef.updateSearchResult}
                includeIgnored={this.state.includeIgnored}
                isGroupView={this.state.isGroupView}
            />
            : <SearchResultFile
                userInfo={this.state.userInfo}
                applications={this.state.applications}
                selectedLanguage={this.state.selectedLanguage}
                searchResult={this.state.searchResultFile}
                userLanguages={this.state.userLanguages}
                selectedSection={this.state.selectedSection}
                selectedApp={selectedApplication}
                errorHandler={this.errorHandler}
                updateSearchResult={this.searchBoxRef.updateSearchResult}
                includeIgnored={this.state.includeIgnored}
                isGroupView={this.state.isGroupView}
            />
        return (
            <React.Fragment>
                <Notifications />
                <Header history={this.props.history} pageRoute={PageRoutes.Translations} />
                <div className="container app-pad-50">
                    <SearchBox
                        ref={r => this.searchBoxRef = r}
                        userInfo={this.state.userInfo}
                        applications={this.state.applications}
                        selectedLanguage={this.state.selectedLanguage}
                        includeIgnored={this.state.includeIgnored}
                        selectedSection={this.state.selectedSection}
                        selectedGroup={this.state.selectedGroup}
                        selectedApplicationId={this.state.selectedApplication}
                        userLanguages={this.state.userLanguages}
                        errorHandler={this.errorHandler}
                        onSearch={this.onSearch}
                        backToTranslations={this.backToTranslations}
                        backButtonShow={this.state.showSearchResultFlag}
                        isGroupView={this.state.isGroupView}
                    />
                    {this.renderIgnoreSwitch()}
                    {this.renderContent(searchResultSection, languageSection, translationSection)}
                </div>
                {this.displayNoteModal()}
                {this.renderNoApplicationMessage()}
            </React.Fragment>
        );
    }

    resetTranslationState = (resetCommands) => {
        const partialState = {};

        if (resetCommands.indexOf(RESET_COMMANDS.RESET_LOADED) >= 0) {
            partialState.loaded = true;
        }

        this.setState(partialState);
    }

    setTranslationState = (setCommands) => {
        const partialState = {};

        if (setCommands.indexOf(SET_COMMANDS.SET_LOADED) >= 0) {
            partialState.loaded = false;
        }

        this.setState(partialState);
    }
}

export default Translations;
