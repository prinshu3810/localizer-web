import React, { Component } from 'react';
import { Row, Col, Input, Button, Icon, Popover, Tabs, Tooltip } from 'antd';
import Roles from '../constants/Constants';
import _ from 'lodash';
import Strings from '../constants/String';

export default class TranslationsBySection extends Component {
    constructor() {
        super();
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (!_.isEqual(this.props.translationsofSection, nextProps.translationsofSection)) {
            return true;
        }
        return false;
    }
    renderTranslations = () => {
        const keyDisabled = this.props.userInfo && this.props.userInfo.role === Roles.Developer ? 'disabled' : 'disabled';
        let englishDisabled = false;
        let translations = _.cloneDeep(this.props.translationsofSection);
        if (this.props.userInfo && this.props.userInfo.role === Roles.Translator) {
            englishDisabled = true;
            if (this.props.selectedLanguage && this.props.selectedLanguage.id == 1 && this.props.userLanguages && this.props.userLanguages.findIndex(x => x.languageId == 1) > -1) {
                englishDisabled = false;
            }
        } else if (this.props.selectedLanguage && this.props.selectedLanguage.id !== 1) {
            englishDisabled = true;
        }

        return translations.map(t => {
            let statusClass = '';

            switch (t.statusId) {
                case 1:
                    statusClass = 'new-translation';
                    break;

                case 2:
                    statusClass = 'updated-translation';
                    break;

                case 3:
                    statusClass = 'published-translation';
                    break;

                default:
                    break;
            }

            if (!t.isVisible) {
                statusClass = 'ignored-translation';
            }

            const saveIcon = (<Col span={1}>
                <div>
                    <Icon type="save" title="Update translation" onClick={this.props.handleSaveClick} data-id={t.textId} className="translation-row-icon" data-translationid={t.translationId} data-statusid={t.statusId} theme="twoTone" />
                </div>
            </Col>);
            const translatedValue = this.props.selectedLanguage.id != 1 ? (
                <Col span={7} key={t.translationId}>
                    <div>
                        <Input
                            title="Double click to open"
                            size="default"
                            className={statusClass}
                            data-id={t.textId}
                            onChange={this.props.handleTranslationInputChange}
                            data-name="translation" placeholder="Translation" value={t.translation}
                            onDoubleClick={this.props.toggleTranslationModal}
                            onPressEnter={t.shouldRenderSaveIcon && this.props.handleSaveClick}
                        />
                    </div>
                </Col>
            ) : '';

            let historyIcon = '';
            let historyIconContent;
            let popoverTitle = '';

            if (t.previousTranslation && this.props.selectedLanguage && this.props.selectedLanguage.id !== 1) {
                historyIconContent = (
                    <Tabs defaultActiveKey="1">
                        <Tabs.TabPane tab="Previous Translation" key="1">
                            {t.previousTranslation}
                        </Tabs.TabPane>
                        <Tabs.TabPane tab="Previous English" key="2">
                            {t.previousEnglish}
                        </Tabs.TabPane>
                    </Tabs>
                );
            } else {
                popoverTitle = 'Previous Translation'
                historyIconContent = t.previousTranslation;
            }

            const copyIcon = (
                <Tooltip placement="top" title="Copy same as English">
                    <Button icon="arrow-right" type="primary" data-id={t.textId} onClick={this.props.onTranslationCopyClick} />
                </Tooltip>
            );

            if (t.previousTranslation) {
                historyIcon = (<Col span={1}>
                    <div className="">
                        <Popover overlayStyle={{ maxWidth: '500px', maxHeight: '150px' }} title={popoverTitle} content={historyIconContent} autoAdjustOverflow={false}>
                            <Icon type="clock-circle" theme="twoTone" className="translation-row-icon" />
                        </Popover>
                    </div>
                </Col>);
            }

            const deleteIcon = this.props.userInfo.role === Roles.Developer && !t.isLocked ? (
                <Col span={1} title="Delete translation">
                    <Icon type="delete" theme="twoTone" onClick={this.props.deleteTranslation} data-id={t.textId} className="translation-row-icon" />
                </Col>
            ) : '';

            var self = this;
            const englishPlaceHolder = t.originalEnglishTranslation ? "English translation" : Strings.Texts.TranslationPlaceholder;
            const handleTranslationInputChange = this.props.handleTranslationInputChange.bind(this);
            const handleModalSaveClick = self.props.handleSaveClick.bind(this);
            const toggleTranslationModal = this.props.toggleTranslationModal.bind(this);

            return (

                <Row key={t.id}>
                    <Col span={1}>
                        {t.sno}
                    </Col>
                    {
                        this.props.userInfo.role === Roles.Developer ?
                            <Col span={4} title="Key">
                                <div className="">
                                    <Input size="default" className={statusClass} placeholder="large size" disabled={keyDisabled} data-id={t.textId} data-name="key" value={t.key} />
                                </div>
                            </Col> : ''

                    }
                    <Col span={7}>
                        <div className="">
                            <Input
                                title="Double click to open"
                                size="default" className={statusClass} placeholder={englishPlaceHolder}
                                onChange={handleTranslationInputChange}
                                disabled={englishDisabled} data-id={t.textId}
                                data-name="englishTranslation" value={t.englishTranslation}
                                onDoubleClick={toggleTranslationModal}
                                onPressEnter={t.shouldRenderSaveIcon && this.props.handleSaveClick}
                            />
                        </div>
                    </Col>
                    {
                        this.props.selectedLanguage.id !== 1 ?
                            <Col span={1}>
                                <div className="copy-translation">
                                    {copyIcon}
                                </div>
                            </Col> : ''
                    }
                    {translatedValue}
                    {
                        t.previousTranslation ? historyIcon : ''
                    }
                    {deleteIcon}
                    {t.shouldRenderSaveIcon ? saveIcon : ''}
                </Row>
            );
        });
    }

    renderMaskContent = () => {

        let width = '';

        if (this.props.selectedLanguage.id === 1) {
            if (this.props.userInfo.role === Roles.Translator) {
                width = '29.5%';
            } else {
                width = '46%'
            }
        } else {
            if (this.props.userInfo.role === Roles.Translator) {
                width = '62.8%';
            } else {
                width = '79.5%';
            }
        }
        return (
            <Tooltip placement="right" title="Application locked for publish">
                <div style={{ width }} className="mask-section"></div>
            </Tooltip>
        );
    }

    render() {
        return <Row>
            {this.props.translationsofSection[0].isLocked && this.renderMaskContent()}
            {this.renderTranslations()}
        </Row>
    }
}

