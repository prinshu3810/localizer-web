import React from 'react';
import { Upload, Button, Icon, Modal, Select, Row, Col, Checkbox } from 'antd';
import axios from 'axios';
import Strings from '../constants/String';
import { ImportModes, Languages } from '../constants/Constants';

const splitJsonFile = (jsonParam) => {
    var sectionList = [];

    Object.entries(jsonParam).forEach(([key, value]) => {
        if (key.indexOf('.') > -1) {
            let sectionName = key.substring(0, key.indexOf('.'));
            if (!sectionList[sectionName]) {
                sectionList[sectionName] = {};
            }
            sectionList[sectionName][key] = value;
        }
        else {
            if (!sectionList["others"]) {
                sectionList["others"] = {};
            }
            sectionList["others"][key] = value;
        }

    });

    return sectionList;
}

const getSectionContent = (sectionContent, sections, languageId, removeEnglishForOtherLang, releaseId) => {
    for (const section in sectionContent) {
        let texts = [];
        const existingSection = sections.find(s => s.name === section);

        if (existingSection) {
            texts = existingSection.text;
        } else {
            sections.push({
                name: section,
                isVisible: true,
                text: texts
            });
        }

        const textContent = sectionContent[section];
        let order = 1;

        for (const key in textContent) {
            const existingText = texts.find(t => t.key === key);

            if (existingText) {
                const englishTranslation = existingText.textTranslation.find(t => t.languageId === 1);
                let translation = textContent[key];

                if (englishTranslation
                    && removeEnglishForOtherLang
                    && englishTranslation.translation === translation) {
                    continue;
                }

                existingText.textTranslation.push({
                    statusId: 3,
                    languageId: languageId,
                    translation: textContent[key],
                    isVisible: true
                });
            } else {
                texts.push({
                    key,
                    versionId: releaseId,
                    textTranslation: [{
                        statusId: 3,
                        languageId: languageId,
                        translation: textContent[key],
                        isVisible: true,
                    }],
                    sno: order++
                });
            }
        }
    }
}

export default class UploadForBulkImport extends React.Component {
    headers = {
        authorization: `Bearer ${JSON.parse(localStorage.getItem("gsuser")).token}`,
    };
    importedFileContent = [];
    state = {
        fileList: [],
        uploading: false,
        fileListWithLanguage: [],
        removeEnglishForOtherLang: true
    }
    fileChangedHandler = (event) => {
        const file = event.target.files[0];
    }
    validateUpload() {
        const { fileListWithLanguage, fileList } = this.state;
        if (fileListWithLanguage.length !== fileList.length) {
            Modal.warning({
                title: 'Warning',
                content: (
                    <div>
                        <p>Please select language.</p>
                    </div>
                ),
                onOk() { },
            });
            return false;
        }
        if (!fileListWithLanguage.find(x => x.languageName.toLowerCase() === "english")) {
            Modal.warning({
                title: 'Warning',
                content: (
                    <div>
                        <p>Please select atleast one English file.</p>
                    </div>
                ),
                onOk() { },
            });
            return false;
        }
        let seen = new Set();
        var hasDuplicates = fileListWithLanguage.some(function (currentObject) {
            return seen.size === seen.add(currentObject.languageName).size;
        });
        if (hasDuplicates) {
            Modal.warning({
                title: 'Warning',
                content: (
                    <div>
                        <p>Please select unique languages.</p>
                    </div>
                ),
                onOk() { },
            });
            return false;
        }
        return true;
    }
    handleUpload = () => {
        if (!this.validateUpload()) {
            return;
        }
        const { fileListWithLanguage } = this.state;
        fileListWithLanguage.forEach((fileDetails) => {
            let reader = new FileReader();
            let self = this;
            reader.onload = (function (theFile) {
                return function (e) {
                    try {
                        let languageName = fileDetails.languageName.toLowerCase();
                        var jsonList = JSON.parse(e.target.result);
                        self.importedFileContent[languageName] = splitJsonFile(jsonList);

                        if (Object.keys(self.importedFileContent).length === fileListWithLanguage.length) {
                            const sections = [];
                            const englishSectionContent = self.importedFileContent["english"];
                            getSectionContent(englishSectionContent, sections, 1, false, self.props.releaseId);

                            Object.entries(self.importedFileContent).forEach(langContent => {
                                const languageId = Languages[langContent[0]];
                                const sectionContent = langContent[1];

                                if (languageId !== 1) {
                                    getSectionContent(sectionContent, sections, languageId, self.state.removeEnglishForOtherLang, self.props.releaseId);
                                }
                            });

                            self.createApp(sections);
                        }
                    } catch (ex) {
                    }
                }
            })(fileDetails);
            reader.readAsText(fileDetails.file);
        });

        this.setState({
            uploading: true,
        });

    }

    createApp(sectionList) {
        const obj = {
            name: this.props.newAppName,
            isVisible: true,
            sections: sectionList,
            isLocked: false,
            translationTypeId: 1,
            versionId: this.props.releaseId,
        };

        axios.post(Strings.URL.GcLocalizerServiceBaseUrl + 'Admin/AddApplicationAndSections', obj, { headers: this.headers })
            .then(() => {
                Modal.success({
                    title: 'Success',
                    content: (
                        <div>
                            <p>Files successfully imported.</p>
                        </div>
                    ),
                    onOk() { window.location.reload(); },
                });
            }).catch(function (error) {

            });
    }

    errorHandler = (error) => {
        if (error && error.response && error.response.status == 401) {
            localStorage.removeItem("gsuser");
            this.props.history.push('/');
        }
    }

    renderLanguages = () => {
        const Option = Select.Option;
        return this.props.allLanguages.map((lang, index) => {
            return <Option value={lang.name} key={lang.Id + index}>{lang.name}</Option>
        });
    }

    languageChanged = (value, file) => {
        let selectedLanguage = null;
        for (var i = 0; i < this.props.allLanguages.length; i++) {
            if (this.props.allLanguages[i].name === value) {
                selectedLanguage = this.props.allLanguages[i];
            }
        }
        let fileListWithLanguage = this.state.fileListWithLanguage;
        let obj = {
            fileName: file.name,
            file: file,
            languageName: selectedLanguage.name,
            languageId: selectedLanguage.id
        }
        let fileIndex = fileListWithLanguage.findIndex(x => x.fileName === file.name);
        if (fileIndex == -1) {
            fileListWithLanguage.push(obj)
        }
        else {
            fileListWithLanguage.splice(fileIndex, 1, obj);
        }
        this.setState({ fileListWithLanguage })

        if (this.props.mode === ImportModes.Edit) {
            this.props.updateImportFileList(fileListWithLanguage);
        }
    }
    uploadedFileDelete = (file) => {
        let fileListWithLanguage = this.state.fileListWithLanguage;
        let fileList = this.state.fileList;
        let fileIndex = fileListWithLanguage.findIndex(x => x.fileName === file.name);
        if (fileIndex > -1) {
            fileListWithLanguage.splice(fileIndex, 1);
        }
        fileIndex = fileList.findIndex(x => x.name === file.name);
        if (fileIndex > -1) {
            fileList.splice(fileIndex, 1);
        }
        this.setState({ fileListWithLanguage, fileList })

        if (this.props.mode === ImportModes.Edit) {
            this.props.updateImportFileList(fileListWithLanguage);
        }
    }
    onRemoveEnglishCheckboxChange = (e) => {
        this.setState({
            removeEnglishForOtherLang: e.target.checked,
        });
    }
    render() {
        let importedFileSection = null;
        const { uploading } = this.state;
        const props = {
            onRemove: (file) => {
                this.setState(({ fileList }) => {
                    const index = fileList.indexOf(file);
                    const newFileList = fileList.slice();
                    newFileList.splice(index, 1);
                    return {
                        fileList: newFileList,
                    };
                });
            },
            beforeUpload: (file) => {
                let fileIndex = this.state.fileList.findIndex(x => x.name === file.name);
                if (fileIndex > -1) {
                    Modal.warning({
                        title: 'Warning',
                        content: (
                            <div>
                                <p>Please select unique file Names.</p>
                            </div>
                        ),
                        onOk() { },
                    });
                    return false;
                }
                var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
                if (ext != "json") {
                    Modal.warning({
                        title: 'Warning',
                        content: (
                            <div>
                                <p>Please select only json files.</p>
                            </div>
                        ),
                        onOk() { },
                    });
                    return false;
                }
                this.setState(({ fileList }) => ({
                    fileList: [...fileList, file],
                }));
                return false;
            },
            fileList: [],
            accept: ".json",
            multiple: true,
        };
        if (this.state.fileList.length > 0) {
            importedFileSection = this.state.fileList.map((file) => {
                return <Row type="flex" gutter={20} key={file.name} style={{ marginTop: '20px' }}>
                    <Col span={8}>{file.name}</Col>
                    <Col span={12}>
                        <Select placeholder="Select a Language" style={{ width: 150, marginRight: '20px' }} onChange={(e) => this.languageChanged(e, file)}>
                            {this.renderLanguages()}
                        </Select>
                    </Col>
                    <Col span={4} style={{ textAlign: 'center', cursor: 'pointer' }}>
                        <span onClick={() => this.uploadedFileDelete(file)}><Icon type="delete" theme="outlined" /> </span>
                    </Col>
                </Row>
            })
        }

        return (
            <div>
                <Row>
                    {
                        this.props.mode === ImportModes.New ? (
                            <Row gutter={6}>
                                <Row span={24} className='field'>
                                    <Col span={24}>
                                        <Checkbox
                                            checked={this.state.removeEnglishForOtherLang}
                                            onChange={this.onRemoveEnglishCheckboxChange}>
                                            Remove English Translation For Non-English Languages
                                </Checkbox>
                                    </Col>
                                </Row>
                            </Row>
                        ) : ''
                    }

                    <Upload {...props}>
                        <Button>
                            <Icon type="upload" /> Select File
                   </Button>
                    </Upload>
                </Row>
                <React.Fragment >
                    {importedFileSection}
                </React.Fragment>
                {
                    this.props.mode === ImportModes.New ? (<Row style={{ marginTop: '20px' }}>
                        <Button
                            className="upload-demo-start"
                            type="primary"
                            onClick={this.handleUpload}
                            disabled={this.state.fileList.length === 0 || this.props.newAppName === "" || this.props.releaseId === 0}
                            loading={uploading}
                        >
                            {uploading ? 'Uploading' : 'Start Upload'}
                        </Button>
                    </Row>) : ''
                }
            </div>
        );
    }
}

export { splitJsonFile };