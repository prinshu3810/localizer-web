import React from 'react';
import { Radio } from 'antd';
import { ListStates } from '../constants/Constants';

export default function UserStateList({ allUsersCount, activeUsersCount, value, handleStateListChange }) {
    return (
        <div className="ad-container__list-states">
            <Radio.Group onChange={handleStateListChange} value={value}>
                <Radio className="all" value={ListStates.All}>All({allUsersCount})</Radio>
                <Radio className="active" value={ListStates.Active}>Active({activeUsersCount})</Radio>
                <Radio className="inactive" value={ListStates.Inavtive}>InActive({allUsersCount - activeUsersCount})</Radio>
            </Radio.Group>
        </div>
    )
}
