const checkForDuplicates = (source) => {
    let isDuplicateFound = false;

    for (let i = 0; i < source.length; i++) {
        const baseElement = source[i];

        if (i !== source - 1) {
            for (let j = i + 1; j < source.length; j++) {
                const element = source[j];

                if (baseElement.trim() === element.trim()) {
                    isDuplicateFound = true;
                    break;
                }

            }
        }

        if (isDuplicateFound) {
            break;
        }
    }

    return isDuplicateFound;
}

export { checkForDuplicates };