var Roles = { Developer: 1, Translator: 2, Admin: 3 };
const Env = { Local: "Local", Dev: "Dev", Prod: "Prod" };
const EnvApiUrl = {
  Local: "https://localhost:5001/api/",
  Dev: "https://apilocalizer-dev.azurewebsites.net/api/",
  Prod: "https://apilocalizer.azurewebsites.net/api/",
};
const TranslationType = {
  Text: 1,
  Pdf: 2,
};

const STATUS_CODES = {
  ApplicationLocked: 200214,
  ApplicationUnlocked: 200215,
  SuccessfullyNotified: 200216,
  NoPermission: 500403,
  EntityDoesNotExist: 500404,
  Unknown: 500505,
  ArgumentNull: 500506,
  ArgumentInvalid: 500507,
  AzureStorage: 500508,
  DatabaseOp: 500509,
  NotificationUnsuccessfull: 500510,
  ApplicationIsLocked: 500515,
  NotificationDeferred: 600600,
};

const ForgotPasswordStatus = {
  Success: 1,
  EmailNotExist: -1,
  DisabledAccount: 2
};

const UpdatePasswordStatus = {
  Success: 1,
  CurrentPasswordWrong: -1,
  InvalidParameters: -2,
};

const PageRoutes = {
  ChangePassword: "ChangePassword",
  Translations: "Translations",
  Admin: "Admin",
  Profile: "profile",
  Developer: "Developer",
  Login: "/",
  ManageGroup: "/ManageGroup"
};

const ImportModes = {
  New: "New",
  Edit: "Edit",
};

const TranslationStatus = { New: 1, Updated: 2, Published: 3, Ignored: 4 };
const Languages = {
  English: 1,
  english: 1,
  Spanish: 2,
  spanish: 2,
  Japanese: 3,
  japanese: 3,
  Chinese: 4,
  chinese: 4,
  Mongolian: 5,
  mongolian: 5,
  Malaysian: 6,
  malaysian: 6,
  Russian: 7,
  russian: 7,
  Arabic: 8,
  arabic: 8,
  Vietnamese: 9,
  vietnamese: 9,
  Korean: 10,
  korean: 10,
  Thai: 11,
  thai: 11,
};
const AppStates = { Loading: 1, True: 2, False: 3, None: 4 };

const PageRouteTexts = {
  Logout: "logout",
  Admin: "admin",
  Developer: "developer",
  Translations: "translations",
  ChangePassword: "changePassword",
  Profile: "profile",
  Login: "login",
};

// Empty translations are to be considered valid even if the english translation contains curly braces.
const VALID_TRANSLATIONS_CURLY_BRACE = [""];
Object.freeze(STATUS_CODES);
Object.freeze(Roles);
Object.freeze(Env);
Object.freeze(EnvApiUrl);
Object.freeze(TranslationType);
Object.freeze(ForgotPasswordStatus);
Object.freeze(UpdatePasswordStatus);
Object.freeze(PageRoutes);
Object.freeze(ImportModes);
Object.freeze(TranslationStatus);
Object.freeze(Languages);
Object.freeze(AppStates);
Object.freeze(PageRouteTexts);

const ListStates = {
  All: 1,
  Active: 2,
  Inavtive: 3,
};

const PublishToEnv = { Dev: 1, Test: 2, Prod: 3}

export default Roles;

export {
  Env,
  EnvApiUrl,
  TranslationType,
  STATUS_CODES,
  ForgotPasswordStatus,
  UpdatePasswordStatus,
  PageRoutes,
  ImportModes,
  TranslationStatus,
  Languages,
  AppStates,
  VALID_TRANSLATIONS_CURLY_BRACE,
  PageRouteTexts,
  ListStates,
  PublishToEnv
};
