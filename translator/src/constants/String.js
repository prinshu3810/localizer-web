import { Env, EnvApiUrl } from './Constants';

/**
 * Gets the current environment, the app is running on.
 * returns one of 'Local', 'Dev' or 'Prod'.
 */
const getEnvironment = () => {
  switch (window.location.hostname) {
    case 'localhost':
    case '127.0.0.1':
      return Env.Local;

    case 'gclocalizationweb.azurewebsites.net':
    case 'localizer-dev.azurewebsites.net':
      return Env.Dev;


    case 'localizer.azurewebsites.net':
    case 'localizer.grapeseed.com':
      return Env.Prod;

    default:
      return Env.Dev;
  }
}

/**
 * Gets the current api base url based on environment.
 */
const getApiBaseUrl = () => {
  const env = getEnvironment();

  switch (env) {
    case Env.Local:
      return EnvApiUrl.Local;

    case Env.Dev:
      return EnvApiUrl.Dev;

    case Env.Prod:
      return EnvApiUrl.Prod;

    default:
      return EnvApiUrl.Local;
  }
}

const apiUrl = getApiBaseUrl();

var Strings = {
  Texts: {
    DeleteAppConfirmation: "On deleting this app, all the translations will be lost and cannot be recovered. Are you sure you want to delete this app ?",
    AppDeleteSucess: "App has been deleted successfully.",
    TranslationDeleteSucess: "Translation has been deleted successfully.",
    TranslationAddSucess: "Translation has been added successfully.",
    Success: "success",
    Danger: "danger",
    Info: "info",
    SavedSuccess: "Changes have been saved successfully",
    TranslationDeleteConfirmation: "Deleting this translation will effect all the languages. Are you sure you want to delete this ?",
    Yes: "Yes",
    No: "No",
    ReleaseNotSelected: "Please select a value for release",
    NotUniqueKey: "Please select a unique value for key",
    EmptyNewKey: "Please enter value for Key",
    EmptyNewTranslation: "Please enter valid values",
    Error: "Error occured while performing the action. Please try to login once again",
    TranslationPlaceholder: "No English translation. See the screenshot for usage in other languages.",
    AppLockMessage: "Admin is updating the translations. Please try after some time.",
    NoAppMessage: "No applications available.",
    PasswordResetMessage: "New password has been sent to your registered email.",
    ConfirmDisable: "Are you sure you want to disable this user?",
    ConfirmDeleteGroup: "Are you sure you want to delete the group?",
    ConfirmEnable: "Are you sure you want to enable this user?",
    AllowPublishConfirmation: "This will change the 'Publish to test' button visibility for the app. Are you sure to continue?",
    DisabledAccount: "You have entered a disabled account email id.",
    NoData: "No Data"
  },
  URL: {
    Local: "https://localhost:5001/api/",
    Dev: "https://gclocalization.azurewebsites.net/api/",
    Prod: "https://apilocalizer.azurewebsites.net/api/",
    GcLocalizerServiceBaseUrl: apiUrl
  },
  AppMessage: {
    ErrorMessage: {
      FetchDataError: "Failed to fetch Data!",
      OperationFailed: "Operation Failed!",
      WrongPassword: "Current Password is wrong!",
      InvalidAccount: "Invalid Account!",
      InvalidEmail: "The input is not valid E-mail!",
      Required: "Required!",
      WeakPassword: "The password you provided must have at least 8 characters.",
      AccountExists: "A User account already exists with this email!",
      AdminRoleRequired: "Admin role is required!",
      InconsistentPassword: "Two passwords that you enter is inconsistent!",
      ConfirmPassword: "Please confirm your password!",
    },
    SuccessMessage: {
      DetailsUpdated: "Details updated successfully!",
      InviteLinkSent: "Invite link sent successfully!",
      UserDisabled: "User account disabled successfully!",
      UserEnabled: "User account enabled successfully!",
      AccountActivated: "Account Activation successful! Redirecting to login page"
    }
  },
  Functions: {
    getEnvironment: getEnvironment
  }
};

export default Strings;