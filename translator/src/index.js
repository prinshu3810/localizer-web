import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Login from './components/Login';
import Translations from './components/Translations';
import Admin from './Pages/Admin';
import Developer from './components/Developer';
import registerServiceWorker from './registerServiceWorker';
import 'antd/dist/antd.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ImagePreview from './components/ImagePreview';
import ForgotPassword from './components/ForgotPassword';
import ChangePassword from './components/ChangePassword';
import routeGuard from './routeGuard';
import Roles from './constants/Constants';
import Profile from './components/Profile';
import ActivateAccount from './Pages/ActivateAccount';
import Groups from './Pages/Groups';

ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route path="/Translations" component={Translations} />
            <Route path="/Admin" component={routeGuard(Admin, [Roles.Admin])} />
            <Route path="/Developer" component={routeGuard(Developer, [Roles.Developer])} />
            <Route path="/Preview" component={ImagePreview} />
            <Route path="/ForgotPassword" component={ForgotPassword} />
            <Route path="/ChangePassword" component={ChangePassword} />
            <Route path="/Profile" component={Profile}/>
            <Route path="/ActivateAccount/:invitationCode" component={ActivateAccount} />
            <Route path="/Groups/:applicationId" component={routeGuard(Groups, [Roles.Admin,Roles.Developer])} />
            <Route exact path="/" component={Login} />
        </Switch>
    </BrowserRouter>
    ,
    document.getElementById('root'));

registerServiceWorker();
