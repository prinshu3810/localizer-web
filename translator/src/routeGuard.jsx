import React from 'react';
import { Alert, Col, Button } from 'antd';
import Roles from './constants/Constants';

/**
 * @param {The component to be rendered} Component
 * @param {The user roles that the incoming component can be rendered for} allowedRoles
 */
const routeGuard = (Component, allowedRoles) =>
    class RouteGuardComponent extends React.Component {
        constructor() {
            super()
            this.state = {
                user: {}
            }
            this.goBack = this.goBack.bind(this);
        }

        componentDidMount() {
            const user = JSON.parse(localStorage.getItem("gsuser"));

            if (!user) {
                this.goToLogin();
                return;
            }

            this.setState({ user });
        }

        goToLogin() {
            this.props.history.push('/');
        }

        goBack() {
            this.props.history.goBack();
        }

        render() {
            const { roles } = this.state.user;

            if (!roles) {
                return null;
            }

            if (allowedRoles.some((role)=> roles.includes(role)) || roles.includes(Roles.Admin)) {
                return <Component {...this.props} />;
            } else {
                return <Col className="centered alert-unauth" span={12}><Alert type="error" message={
                    <div>
                        <p>
                            You are not authorized to view this page. Login with developer credentials to do so.
                        </p>
                        <Button type="primary" onClick={this.goBack}>Go Back</Button>
                    </div>
                }></Alert></Col>
            }
        }
    }

export default routeGuard;