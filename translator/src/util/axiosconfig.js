import axios from "axios";
import Strings from "../constants/String";

// Configure base url.
const Axios = axios.create({
  baseURL: Strings.URL.GcLocalizerServiceBaseUrl,
  headers : {
    'content-type' : 'application/json'
  }
});

// Intercept requests and add authorization token.
Axios.interceptors.request.use((config) => {
  const authToken = localStorage.getItem("gsuser")
    ? `Bearer ${JSON.parse(localStorage.getItem("gsuser")).token}`
    : null;
  if (authToken) {
    config.headers.authorization = authToken;
  }
  return config;
});

export default Axios;
